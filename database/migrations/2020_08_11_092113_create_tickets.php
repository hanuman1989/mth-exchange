<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('ticket_reply_id')->unsigned();
            $table->longText('content');
			$table->longText('file');
			$table->integer('ticket_status_id')->unsigned();
            $table->integer('ticket_prority_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
			$table->softDeletes();
			$table->timestamp('completed_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
