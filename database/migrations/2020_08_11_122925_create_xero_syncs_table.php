<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateXeroSyncsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xero_syncs', function (Blueprint $table) {
            $table->id();
            $table->integer('xero_action')->comment(' 1 = client added,1 = invoice generated');
            $table->tinyInteger('xero_status')->comment(' 1 = updated on xero,0 = need to re update on xero');
            $table->string('xero_response_id', 200)->nullable()->comment('This is uniqeue id those returned fron xero');
            $table->longText('xero_response');
            $table->mediumText('xero_error')->nullable();
            $table->integer('retries')->nullable();
            $table->integer('xerosyncable_id')  ;
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xero_syncs');
    }
}
