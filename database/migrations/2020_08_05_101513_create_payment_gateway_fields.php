<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentGatewayFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_gateway_fields', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('payment_gateway_name_id')->unsigned()->nullable();
            $table->string('field_name', 191);
            $table->string('field_value', 255);
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /** 
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_gateway_fields');
    }
}
