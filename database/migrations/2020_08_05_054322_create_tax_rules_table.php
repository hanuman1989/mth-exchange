<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaxRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_rules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tax_name',191);
            $table->integer('country_id')->unsigned()->nullable();
            $table->double('tax_rate', 10, 2);
            $table->tinyInteger('status')->default(1)->comment(' 0 = Inactive,1 =Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax_rules');
    }
}
