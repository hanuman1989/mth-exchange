<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsigned()->index('client_details_user_id_foreign');
            $table->string('company_name', 100)->nullable();
            $table->integer('security_question')->unsigned()->nullable();
            $table->string('security_answer', 255)->nullable();
            $table->integer('tax_id')->nullable();
            $table->string('address_1', 255)->nullable();
            $table->string('address_2', 255)->nullable();
            $table->string('city', 255)->nullable();
            $table->integer('state_id')->unsigned()->index('client_details_state_id_foreign')->nullable();
            $table->string('postcode', 20)->nullable();
            $table->integer('country_id')->unsigned()->index('client_details_country_id_foreign')->nullable();
            $table->string('phone', 20)->nullable();
            $table->enum('payment_method', ['Default','Stripe', 'BankTransfer']);
            $table->enum('billing_contact', ['Default']);
            $table->integer('language_id')->unsigned()->nullable();
            $table->enum('currency', ['GBP','USD']);
            $table->enum('client_group', ['NONE']);
            $table->double('credit_balance', 10, 2)->nullable();
            $table->text('admin_notes')->nullable();
            $table->tinyInteger('late_fees')->default(0)->comment(' 0 = no,1 =yes');
            $table->tinyInteger('overdue_notices')->default(0)->comment(' 0 = no,1 =yes');
            $table->tinyInteger('tax_exempt')->default(0)->comment(' 0 = no,1 =yes');
            $table->tinyInteger('seperate_invoice')->default(0)->comment(' 0 = no,1 =yes');
            $table->tinyInteger('disable_cc_processing')->default(0)->comment(' 0 = no,1 =yes');
            $table->tinyInteger('marketing_email_opt_in')->default(0)->comment(' 0 = no,1 =yes');
            $table->tinyInteger('status_update')->default(0)->comment(' 0 = no,1 =yes');
            $table->tinyInteger('allow_single_sign_on')->default(0)->comment(' 0 = no,1 =yes');
            $table->tinyInteger('send_account_info_message')->default(0)->comment(' 0 = no,1 =yes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_details');
    }
}
