<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PaymentGatewayProductGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('payment_gateway_product_group', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('payment_gateway_id')->unsigned()->index();
			$table->integer('product_group_id')->unsigned()->index();
			
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_gateway_product_group');
    }
}
