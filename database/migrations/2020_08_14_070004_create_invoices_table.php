<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->string('invoice_number', 255);
            $table->integer('user_id')->unsigned()->index('invoices_user_id_foreign');
            $table->text("line_item_description")->nullable();
            $table->enum('payment_method', ['Default','Stripe', 'BankTransfer']);
            $table->enum('status', ['Draft','Unpaid', 'Overdue', 'Paid', 'Cancelled', 'Refunded', 'Collections', 'Payment Pending']);
            $table->date('invoice_date');
            $table->date('due_date');
            $table->date('date_paid');
            $table->date('due_from');
            $table->date('due_to');
            $table->string("invoice_file", 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
