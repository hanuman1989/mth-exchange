<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateXeroInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xero_invoices', function (Blueprint $table) {
            $table->id();
            $table->integer('xero_action')->comment(' 1 = client added,1 = invoice generated');
            $table->tinyInteger('xero_status')->comment(' 1 = updated on xero,0 = need to re update on xero');
            $table->string('xero_response_id', 200)->nullable()->comment('This is unique id those returned fron xero');
            $table->longText('xero_response');
            $table->mediumText('xero_error')->nullable();
            $table->integer('retries')->nullable();
            $table->integer('xero_invoice_id')  ;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xero_invoices');
    }
}
