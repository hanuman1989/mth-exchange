@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.clients.management') . ' | ' . trans('labels.backend.clients.view'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.clients.management') }}
        <small>{{ trans('labels.backend.clients.view') }}</small>
    </h1>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.clients.view') }}</h3>

            <div class="box-tools pull-right">
                @include('backend.clients.partials.clients-header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        @include('backend.clients.partials.client-header-tabs')
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane mt-30 active" id="summary">
                <div role="tabpanel" class="tab-pane mt-30 active" id="summary">
                    @include('backend.clients.partials.tabs.profile')
                </div><!--tab overview profile-->
            </div>
        </div><!--tab content-->

    </div><!--box-->
@endsection