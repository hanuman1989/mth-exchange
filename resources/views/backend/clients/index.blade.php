@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.clients.management'))

@section('page-header')
    <h1>{{ trans('labels.backend.clients.management') }}</h1>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.clients.management') }}</h3>

            <div class="box-tools pull-right">
            <div class="btn-group">
                <button type="button" class="btn btn-warning btn-flat clear-search">Clear Search
                </button>
            </div>
            <div class="btn-group">
                <button type="button" class="btn btn-warning btn-flat advance-search">Advance Search +
                </button>
            </div>
                @include('backend.clients.partials.clients-header-buttons')
            </div>
            
        </div><!--box-header with-border-->
        <div class="box-body">
            <div class="box-tools">
                {{ Form::open(['route' => 'admin.clients.get', 'class' => 'form-horizontal search-client', 'role' => 'form', 'method' => 'post']) }}
                    <div class="col-md-12 row search-row">
                        <div class="col-md-4">
                            {{ Form::text('company_name', app('request')->query('company_name') , ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.clients.table.company')]) }}
                        </div>
                        <div class="col-md-4">
                            {{ Form::text('email', app('request')->query('email') , ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.clients.table.email')]) }}
                        </div>
                        <div class="col-md-4">
                            {{ Form::text('phone', app('request')->query('phone') , ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.clients.table.phone')]) }}
                        </div>
                    </div>
                    <div class="search-panel">
                        <div class="col-md-12 row search-row">
                            <div class="col-md-4">
                            {{ Form::select('client_group', config('constants.client_group'), app('request')->query('client_group') , ['class' => 'form-control status box-size', 'placeholder' => "Select Client Group"]) }}
                            </div>
                            <div class="col-md-4">
                                {{ Form::select('status', config('constants.status'), app('request')->query('status') , ['class' => 'form-control status box-size', 'placeholder' => "Select Status"]) }}
                            </div>
                            <div class="col-md-4">
                                {{ Form::text('address_1', app('request')->query('address_1') , ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.clients.table.address_1')]) }}
                            </div>
                        </div>
                        <div class="col-md-12 row search-row">
                            <div class="col-md-4">
                                {{ Form::text('address_2', app('request')->query('address_2') , ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.clients.table.address_2')]) }}
                            </div>
                            <div class="col-md-4">
                                {{ Form::text('city', app('request')->query('city') , ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.clients.table.city')]) }}
                            </div>
                            <div class="col-md-4">
                                {{ Form::select('state', $states, app('request')->query('state') , ['class' => 'form-control status box-size', 'placeholder' => "Select State"]) }}
                            </div>
                        </div>
                        <div class="col-md-12 row search-row">
                            <div class="col-md-4">
                                {{ Form::text('postcode', app('request')->query('postcode') , ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.clients.table.postcode')]) }}
                            </div>
                            <div class="col-md-4">
                                {{ Form::select('country', $countries, app('request')->query('country') , ['class' => 'form-control status box-size', 'placeholder' => "Select Country"]) }}
                            </div>
                            <div class="col-md-4">
                                {{ Form::select('payment_method', config('constants.payment_method'), app('request')->query('payment_method') , ['class' => 'form-control status box-size', 'placeholder' => 'Select Payment Method']) }}
                            </div>
                        </div>
                        <div class="col-md-12 row search-row">
                            <div class="col-md-4">
                                {{ Form::select('auto_credit_card_billing', config('constants.status') , app('request')->query('auto_credit_card_billing') , ['class' => 'form-control status box-size', 'placeholder' => "Select auto credit card option"]) }}
                            </div>
                            <div class="col-md-4">
                                {{ Form::text('credit_balance', app('request')->query('credit_balance') , ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.clients.table.credit_balance')]) }}
                            </div>
                            <div class="col-md-4">
                                {{ Form::select('currency', config('constants.currency') ,app('request')->query('currency') , ['class' => 'form-control status box-size', 'placeholder' => "Select Currency"]) }}
                            </div>
                        </div>
                        <div class="col-md-12 row search-row">
                            <div class="col-md-4">
                                {{ Form::select('language', $languages ,app('request')->query('language') , ['class' => 'form-control status box-size', 'placeholder' => "Select Language"]) }}
                            </div>
                            <div class="col-md-4">
                                {{ Form::select('marketing_emails_opt_in', config('constants.status') , app('request')->query('marketing_emails_opt_in') , ['class' => 'form-control status box-size', 'placeholder' => "Select marketing opt in option"]) }}
                            </div>
                            <div class="col-md-4">
                                {{ Form::select('email_verification_status', config('constants.status') , app('request')->query('email_verification_status') , ['class' => 'form-control status box-size', 'placeholder' => "Select email verification status"]) }}
                            </div>
                        </div>
                        <div class="col-md-12 row search-row">
                            <div class="col-md-4">
                                {{ Form::select('auto_status_update', config('constants.status') , app('request')->query('auto_status_update') , ['class' => 'form-control status box-size', 'placeholder' => "Select auto status update option"]) }}
                            </div>
                            <div class="col-md-4">
                                {{ Form::select('tax_exempt_status', config('constants.status') , app('request')->query('tax_exempt_status') , ['class' => 'form-control status box-size', 'placeholder' => "Select tax exempt status"]) }}
                            </div>
                            <div class="col-md-4">
                                {{ Form::select('late_fees', config('constants.status') , app('request')->query('late_fees') , ['class' => 'form-control status box-size', 'placeholder' => "Select late fees option"]) }}
                            </div>
                        </div>
                        <div class="col-md-12 row search-row">
                            <div class="col-md-4">
                                {{ Form::select('overdue_notices', config('constants.status') , app('request')->query('overdue_notices') , ['class' => 'form-control status box-size', 'placeholder' => "Select overdue notice option"]) }}
                            </div>
                            <div class="col-md-4">
                                {{ Form::select('seperate_invoices', config('constants.status') , app('request')->query('seperate_invoices') , ['class' => 'form-control status box-size', 'placeholder' => "Select seperate invoice option"]) }}
                            </div>
                        </div>
                    </div>
                    <div class="box-tools pull-right">
                        {{ Form::button(trans('Search'), ['class' => 'btn btn-primary btn-md search-client']) }}
                    </div>
                {{ Form::close() }}
            </div>
        </div>

        <div class="box-body">
            <div class="table-responsive data-table-wrapper">
                <table id="clients-table" class="table table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>{{ trans('labels.backend.clients.table.first_name') }}</th>
                            <th>{{ trans('labels.backend.clients.table.last_name') }}</th>
                            <th>{{ trans('labels.backend.clients.table.company') }}</th>
                            <th>{{ trans('labels.backend.clients.table.email') }}</th>
                            <th>{{ trans('labels.backend.clients.table.status') }}</th>
                            <th>{{ trans('labels.backend.clients.table.createdat') }}</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                    <thead class="transparent-bg">
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th>
                            {!! Form::select('status', [0 => "InActive", 1 => "Active"], null, ["class" => "search-input-select form-control", "data-column" => 4, "placeholder" => trans('labels.backend.clients.table.all')]) !!}
                            </th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection

@push('after-scripts')
    {{-- For DataTables --}}
    {{ Html::script(mix('js/dataTable.js')) }}

    <script>
        //Below written line is short form of writing $(document).ready(function() { })
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            redraw_datatable();
            
            var dataTable;
            function redraw_datatable() {
                    dataTable = $('#clients-table').dataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: '{{ route("admin.clients.get") }}',
                        type: 'post',
                        data : function(d) {
                            d.form = $(".search-client").serialize()
                        },
                    },
                    columns: [
                        {data: 'first_name', name: '{{config('access.users_table')}}.first_name'},
                        {data: 'last_name', name: '{{config('access.users_table')}}.last_name'},
                        {data: 'company_name', name: 'client_detail.company_name'},
                        {data: 'email', name: '{{config('access.users_table')}}.email'},
                        {data: 'status', name: '{{config('access.users_table')}}.status'},
                        {data: 'created_at', name: '{{config('access.users_table')}}.created_at'},
                        {data: 'actions', name: 'actions', searchable: false, sortable: false}
                    ],
                    order: [[5, "desc"]],
                    searchDelay: 500,
                    dom: 'lBfrtip',
                    buttons: {
                        buttons: [
                            { extend: 'copy', className: 'copyButton',  exportOptions: {columns: [ 0, 1, 2, 3, 4, 5 ]  }},
                            { extend: 'csv', className: 'csvButton',  exportOptions: {columns: [ 0, 1, 2, 3, 4, 5 ]  }},
                            { extend: 'excel', className: 'excelButton',  exportOptions: {columns: [ 0, 1, 2, 3, 4, 5 ]  }},
                            { extend: 'pdf', className: 'pdfButton',  exportOptions: {columns: [ 0, 1, 2, 3, 4, 5 ]  }},
                            { extend: 'print', className: 'printButton',  exportOptions: {columns: [ 0, 1, 2, 3, 4, 5 ]  }}
                        ]
                    }
                });
            }
           
            $(".advance-search").click(function() {
                $(".search-panel").toggle();
            });

            $(".clear-search").click(function() {
                $("input[type='text']").val("");
                $("input[type='select']").val("");
                dataTable.fnDestroy();
                redraw_datatable();
            });

            Backend.DataTableSearch.init(dataTable);

            $(".search-client").on("click", function() {
                $(".input-sm").trigger("keypress");
                dataTable.fnDestroy();
                redraw_datatable();
            });
        });
    </script>
@endpush
