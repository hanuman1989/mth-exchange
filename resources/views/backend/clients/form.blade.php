<div class="box-body client-form">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('first_name', trans('labels.backend.clients.table.first_name'), ['class' => 'control-label required']) }}

                <div>
                    {{ Form::text('first_name', null, ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.clients.table.first_name'), 'required', 'maxlength' => 100]) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('address_1', trans('labels.backend.clients.table.address_1'), ['class' => 'control-label']) }}
                <div>
                    {{ Form::text('address_1', isset($clientdetail->detail->address_1) ? $clientdetail->detail->address_1 : null , ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.clients.table.address_1'), 'maxlength' => 255]) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('last_name', trans('labels.backend.clients.table.last_name'), ['class' => 'control-label required']) }}
                <div>
                    {{ Form::text('last_name', null, ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.clients.table.last_name'), 'required', 'maxlength' => 100]) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>
        
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('address_2', trans('labels.backend.clients.table.address_2'), ['class' => 'control-label']) }}
                <div>
                    {{ Form::text('address_2', isset($clientdetail->detail->address_2) ? $clientdetail->detail->address_2 : null , ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.clients.table.address_2'), 'maxlength' => 255]) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('company_name', trans('labels.backend.clients.table.company'), ['class' => 'control-label']) }}
                <div>
                    {{ Form::text('company_name', isset($clientdetail->detail->company_name) ? $clientdetail->detail->company_name : null , ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.clients.table.company'), 'maxlength' => 100]) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('city', trans('labels.backend.clients.table.city'), ['class' => 'control-label']) }}
                <div>
                    {{ Form::text('city', isset($clientdetail->detail->city) ? $clientdetail->detail->city : null , ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.clients.table.city'), 'maxlength' => 100]) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('email', trans('labels.backend.clients.table.email'), ['class' => 'control-label required']) }}

                <div>
                    {{ Form::text('email', null, ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.clients.table.email'), 'required', 'maxlength' => 100]) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('state', trans('labels.backend.clients.table.state'), ['class' => 'control-label']) }}
                <div>
                    {!! Form::select('state_id',$states, isset($clientdetail->detail->state_id) ? $clientdetail->detail->state_id : null , ["class" => "form-control box-size", "placeholder" => "Select State/Region"]) !!}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>
    </div>


    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('password', trans('labels.backend.clients.table.password'), ['class' => 'control-label']) }}

                <div>
                    @if(!isset($clientdetail))
                        {{ Form::password('password', ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.clients.table.password')]) }}
                    @else 
                        {{ Form::password('password', ['class' => 'form-control box-size', 'placeholder' => trans('Reset & Send')]) }}
                    @endif
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('postcode', trans('labels.backend.clients.table.postcode'), ['class' => 'control-label']) }}
                <div>
                    {{ Form::text('postcode', isset($clientdetail->detail->postcode) ? $clientdetail->detail->postcode : null , ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.clients.table.postcode')]) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('security_question', trans('labels.backend.clients.table.security_question'), ['class' => 'control-label']) }}

                <div>
                    {!! Form::select('security_question',$securityQuestions, isset($clientdetail->detail->security_question) ? $clientdetail->detail->security_question : null , ["class" => "form-control box-size", "placeholder" => "Security question"]) !!}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('country', trans('labels.backend.clients.table.country'), ['class' => 'control-label']) }}
                <div>
                    {!! Form::select('country_id',$countries, isset($clientdetail->detail->country_id) ? $clientdetail->detail->country_id : null , ["class" => "form-control box-size", "placeholder" => "Select Country"]) !!}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('security_answer', trans('labels.backend.clients.table.security_answer'), ['class' => 'control-label']) }}

                <div>
                    {{ Form::text('security_answer', isset($clientdetail->detail->security_answer) ? $clientdetail->detail->security_answer : null , ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.clients.table.security_answer')]) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('phone', trans('labels.backend.clients.table.phone'), ['class' => 'control-label']) }}
                <div>
                    {{ Form::text('phone', isset($clientdetail->detail->phone) ? $clientdetail->detail->phone : null , ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.clients.table.phone')]) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('tax_id', trans('labels.backend.clients.table.tax_id'), ['class' => 'control-label']) }}

                <div>
                    {{ Form::text('tax_id', isset($clientdetail->detail->tax_id) ? $clientdetail->detail->tax_id : null , ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.clients.table.tax_id')]) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('payment_method', trans('labels.backend.clients.table.payment_method'), ['class' => 'control-label']) }}
                <div>
                   {{ Form::select('payment_method', config('constants.payment_method'), isset($clientdetail->detail->payment_method) ? $clientdetail->detail->payment_method : null , ['class' => 'form-control  status box-size']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('late_fees', trans('labels.backend.clients.table.late_fees'), ['class' => 'control-label']) }}

                <div>
                    <div class="control-group">
                        <label class="control control--checkbox">
                        @if(isset($clientdetail->detail->late_fees) && !empty ($clientdetail->detail->late_fees))
                            {{ Form::checkbox('late_fees', 1, true) }}
                        @else
                            {{ Form::checkbox('late_fees', 1, false) }}
                        @endif
                        <div class="control__indicator"></div>
                        Don't Apply Late Fees
                        </label>
                    </div>
                </div><!--col-lg-1-->
            </div><!--form control-->
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('billing_contact', trans('labels.backend.clients.table.billing_contact'), ['class' => 'control-label']) }}
                <div>
                   {{ Form::select('billing_contact', config('constants.billing_contact'), isset($clientdetail->detail->billing_contacts) ? $clientdetail->detail->billing_contacts : null , ['class' => 'form-control  status box-size']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('overdue_notices', trans('labels.backend.clients.table.overdue_notices'), ['class' => 'control-label']) }}

                <div>
                    <div class="control-group">
                        <label class="control control--checkbox">
                        @if(isset($clientdetail->detail->overdue_notices) && !empty ($clientdetail->detail->overdue_notices))
                            {{ Form::checkbox('overdue_notices', 1, true) }}
                        @else
                            {{ Form::checkbox('overdue_notices', 1, false) }}
                        @endif
                        <div class="control__indicator"></div>
                        Don't Send Overdue Emails
                        </label>
                    </div>
                </div><!--col-lg-1-->
            </div><!--form control-->
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('language', trans('labels.backend.clients.table.language'), ['class' => 'control-label']) }}
                <div>
                    {!! Form::select('language_id',$languages, isset($clientdetail->detail->language_id) ? $clientdetail->detail->language_id : null , ["class" => "form-control box-size", "placeholder" => "Select Language"]) !!}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('tax_exempt', trans('labels.backend.clients.table.tax_exempt'), ['class' => 'control-label']) }}

                <div>
                    <div class="control-group">
                        <label class="control control--checkbox">
                        @if(isset($clientdetail->detail->tax_exempt) && !empty ($clientdetail->detail->tax_exempt))
                            {{ Form::checkbox('tax_exempt', 1, true) }}
                        @else
                            {{ Form::checkbox('tax_exempt', 1, false) }}
                        @endif
                        <div class="control__indicator"></div>
                        Don't apply tax to invoices
                        </label>
                    </div>
                </div><!--col-lg-1-->
            </div><!--form control-->
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('status', trans('labels.backend.clients.table.status'), ['class' => 'control-label']) }}

                <div>
                    <div class="control-group">
                        <label class="control control--checkbox">
                        @if(isset($clientdetail->status) && !empty ($clientdetail->status))
                            {{ Form::checkbox('status', 1, true) }}
                        @else
                            {{ Form::checkbox('status', 1, false) }}
                        @endif
                        <div class="control__indicator"></div>
                        Please check to active client
                        </label>
                    </div>
                </div><!--col-lg-1-->
            </div><!--form control-->
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('seperate_invoice', trans('labels.backend.clients.table.seperate_invoice'), ['class' => 'control-label']) }}

                <div>
                    <div class="control-group">
                        <label class="control control--checkbox">
                        @if(isset($clientdetail->detail->seperate_invoice) && !empty ($clientdetail->detail->seperate_invoice))
                            {{ Form::checkbox('seperate_invoice', 1, true) }}
                        @else
                            {{ Form::checkbox('seperate_invoice', 1, false) }}
                        @endif
                        <div class="control__indicator"></div>
                        Seperate Invoices for Services
                        </label>
                    </div>
                </div><!--col-lg-1-->
            </div><!--form control-->
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('currency', trans('labels.backend.clients.table.currency'), ['class' => 'control-label']) }}
                <div>
                   {{ Form::select('currency', config('constants.currency'), isset($clientdetail->detail->currency) ? $clientdetail->detail->currency : null , ['class' => 'form-control  status box-size']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('disable_cc_processing', trans('labels.backend.clients.table.disable_cc_processing'), ['class' => 'control-label']) }}

                <div>
                    <div class="control-group">
                        <label class="control control--checkbox">
                        @if(isset($clientdetail->detail->disable_cc_processing) && !empty ($clientdetail->detail->disable_cc_processing))
                            {{ Form::checkbox('disable_cc_processing', 1, true) }}
                        @else
                            {{ Form::checkbox('disable_cc_processing', 1, false) }}
                        @endif
                        <div class="control__indicator"></div>
                        Disable Automatic CC Processing
                        </label>
                    </div>
                </div><!--col-lg-1-->
            </div><!--form control-->
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('client_group', trans('labels.backend.clients.table.client_group'), ['class' => 'control-label']) }}
                <div>
                   {{ Form::select('client_group', config('constants.client_group'), isset($clientdetail->detail->client_groups) ? $clientdetail->detail->client_groups : null , ['class' => 'form-control status box-size']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('marketing_email_opt_in', trans('labels.backend.clients.table.marketing_email_opt_in'), ['class' => 'control-label']) }}

                <div>
                    <div class="control-group">
                        <label class="control control--checkbox">
                        @if(isset($clientdetail->detail->marketing_email_opt_in) && !empty ($clientdetail->detail->marketing_email_opt_in))
                            {{ Form::checkbox('marketing_email_opt_in', 1, true) }}
                        @else
                            {{ Form::checkbox('marketing_email_opt_in', 1, false) }}
                        @endif
                        <div class="control__indicator"></div>
                        Send Client Marketing Emails
                        </label>
                    </div>
                </div><!--col-lg-1-->
            </div><!--form control-->
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('credit_balance', trans('labels.backend.clients.table.credit_balance'), ['class' => 'control-label']) }}
                <div>
                    {{ Form::text('credit_balance', isset($clientdetail->detail->credit_balance) ? $clientdetail->detail->credit_balance : null , ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.clients.table.credit_balance')]) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('status_update', trans('labels.backend.clients.table.status_update'), ['class' => 'control-label']) }}

                <div>
                    <div class="control-group">
                        <label class="control control--checkbox">
                        @if(isset($clientdetail->detail->status_update) && !empty ($clientdetail->detail->status_update))
                            {{ Form::checkbox('status_update', 1, true) }}
                        @else
                            {{ Form::checkbox('status_update', 1, false) }}
                        @endif
                        <div class="control__indicator"></div>
                        Disable Automatic Status Update
                        </label>
                    </div>
                </div><!--col-lg-1-->
            </div><!--form control-->
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('allow_single_sign_on', trans('labels.backend.clients.table.allow_single_sign_on'), ['class' => 'control-label']) }}

                <div>
                    <div class="control-group">
                        <label class="control control--checkbox">
                        @if(isset($clientdetail->detail->allow_single_sign_on) && !empty ($clientdetail->detail->allow_single_sign_on))
                            {{ Form::checkbox('allow_single_sign_on', 1, true) }}
                        @else
                            {{ Form::checkbox('allow_single_sign_on', 1, false) }}
                        @endif
                        <div class="control__indicator"></div>
                        Tick To Allow Single Sign-ON
                        </label>
                    </div>
                </div><!--col-lg-1-->
            </div><!--form control-->
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                {{ Form::label('admin_notes', trans('labels.backend.clients.table.admin_notes'), ['class' => 'control-label']) }}

                <div>
                    {{ Form::textarea('admin_notes', null, ['class' => 'form-control box-size', 'placeholder' => trans('admin_notes'), 'rows' => 5]) }}
                </div><!--col-lg-1-->
            </div><!--form control-->
        </div>
    </div>
    @if(!isset($clientdetail))
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('send_account_info_message', trans('labels.backend.clients.table.send_account_info_message'), ['class' => 'control-label']) }}

                <div class="col-lg-1">
                    <div class="control-group">
                        <label class="control control--checkbox">
                        @if(isset($clientdetail->detail->send_account_info_message) && !empty ($clientdetail->detail->send_account_info_message))
                            {{ Form::checkbox('status_update', 1, true) }}
                        @else
                            {{ Form::checkbox('status_update', 1, false) }}
                        @endif
                        <div class="control__indicator"></div>
                        </label>
                    </div>
                </div><!--col-lg-1-->
            </div><!--form control-->
        </div>
    </div>
    @endif
    
    {{-- Associated Roles --}}
    <div class="form-group hidden">
        {{ Form::label('status', trans('validation.attributes.backend.access.users.associated_roles'), ['class' => 'col-lg-2 control-label']) }}

        <div class="col-lg-8">
            @if (count($roles) > 0)
                @foreach($roles as $role)
                    <div>
                    <label for="role-{{$role->id}}" class="control control--radio">
                    <input type="radio" value="{{$role->id}}" name="assignees_roles[]" id="role-{{$role->id}}" class="get-role-for-permissions" {{ $role->id == 2 ? 'checked' : '' }} />  &nbsp;&nbsp;{!! $role->name !!}
                    <div class="control__indicator"></div>
                        <a href="#" data-role="role_{{ $role->id }}" class="show-permissions small">
                            (
                                <span class="show-text">{{ trans('labels.general.show') }}</span>
                                <span class="hide-text hidden">{{ trans('labels.general.hide') }}</span>
                                {{ trans('labels.backend.access.users.permissions') }}
                            )
                        </a>
                    </label>
                    </div>
                    <div class="permission-list" data-role="role_{{ $role->id }}">
                        @if ($role->all)
                            {{ trans('labels.backend.access.users.all_permissions') }}<br/><br/>
                        @else
                            @if (count($role->permissions) > 0)
                                <blockquote class="small">
                                    @foreach ($role->permissions as $perm){{--
                                --}}{{$perm->display_name}}<br/>
                                    @endforeach
                                </blockquote>
                            @else
                                {{ trans('labels.backend.access.users.no_permissions') }}<br/><br/>
                            @endif
                        @endif
                    </div><!--permission list-->
                @endforeach
            @else
                {{ trans('labels.backend.access.users.no_roles') }}
            @endif
        </div><!--col-lg-3-->
    </div><!--form control-->

    {{-- Associated Permissions --}}
    <div class="form-group hidden">
        {{ Form::label('associated-permissions', trans('validation.attributes.backend.access.roles.associated_permissions'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            <div id="available-permissions" style="width: 700px; height: 200px; overflow-x: hidden; overflow-y: scroll;">
                <div class="row">
                    <div class="col-xs-12 get-available-permissions">
                        @if ($permissions)

                            @foreach ($permissions as $id => $display_name)
                            <div class="control-group">
                                <label class="control control--checkbox" for="perm_{{ $id }}">
                                    <input type="checkbox" name="permissions[{{ $id }}]" value="{{ $id }}" id="perm_{{ $id }}" {{ isset($userPermissions) && in_array($id, $userPermissions) ? 'checked' : '' }} /> <label for="perm_{{ $id }}">{{ $display_name }}</label>
                                    <div class="control__indicator"></div>
                                </label>
                            </div>
                            @endforeach
                        @else
                            <p>There are no available permissions.</p>
                        @endif
                    </div><!--col-lg-6-->
                </div><!--row-->
            </div><!--available permissions-->
        </div><!--col-lg-3-->
    </div><!--form control-->
</div>