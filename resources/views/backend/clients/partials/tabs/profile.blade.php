<table class="table table-striped table-hover">
    <tr>
        <th>{{ trans('labels.backend.access.users.tabs.content.overview.name') }}</th>
        <td>{{ $client->first_name .' '. $client->last_name }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.access.users.tabs.content.overview.email') }}</th>
        <td>{{ $client->email }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.clients.table.phone') }}</th>
        <td>{{ $client->detail->phone ?? 'NA' }}</td>
    </tr>
    
    <tr>
        <th>{{ trans('labels.backend.clients.table.company') }}</th>
        <td>{{ $client->detail->company_name ?? 'NA' }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.clients.table.address_1') }}</th>
        <td>{{ $client->detail->address_1 ?? 'NA' }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.clients.table.address_2') }}</th>
        <td>{{ $client->detail->address_2 ?? 'NA' }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.clients.table.city') }}</th>
        <td>{{ $client->detail->city ?? 'NA' }}</td>
    </tr>

    <tr>
        <th>{{ trans('State/Region') }}</th>
        <td>{{ $client->detail->state ?? 'NA' }}</td>
    </tr>

    <tr>
        <th>{{ trans('Country') }}</th>
        <td>{{ $client->detail->country ?? 'NA' }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.clients.table.postcode') }}</th>
        <td>{{ $client->detail->postcode ?? 'NA' }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.access.users.tabs.content.overview.status') }}</th>
        <td>{!! $client->status_label !!}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.access.users.tabs.content.overview.confirmed') }}</th>
        <td>{!! $client->confirmed_label !!}</td>
    </tr>
    
</table>