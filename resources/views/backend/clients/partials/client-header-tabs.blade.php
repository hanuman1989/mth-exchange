<?php
use Illuminate\Support\Facades\Route;
?>
<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="{{ Route::is('admin.clients.*') ? 'active' : '' }}">
        <a href="{{ route('admin.clients.show', $client->id) }}" aria-controls="profile" role="tab">{{ trans('labels.backend.clients.tabs.titles.summary') }}</a>
    </li>
    <li role="presentation">
        <a href="#contacts" aria-controls="contacts" role="tab" data-toggle="tab">{{ trans('labels.backend.clients.tabs.titles.contacts') }}</a>
    </li>
    <li role="presentation">
        <a href="#product" aria-controls="product" role="tab" data-toggle="tab">{{ trans('labels.backend.clients.tabs.titles.product') }}</a>
    </li>
    <li role="presentation">
        <a href="#domains" aria-controls="domains" role="tab" data-toggle="tab">{{ trans('labels.backend.clients.tabs.titles.domains') }}</a>
    </li>
    <li role="presentation">
        <a href="#billable_items" aria-controls="billable_items" role="tab" data-toggle="tab">{{ trans('labels.backend.clients.tabs.titles.billable_items') }}</a>
    </li>
    <li class="{{ Route::is('admin.invoices.*') ? 'active' : '' }}" role="presentation">
        <a class="{{ Route::is('admin.invoices.*') ? 'active' : '' }}" href="{{ route('admin.invoices.index', ['client_id' => $client->id]) }}" aria-controls="invoices" role="tab">{{ trans('labels.backend.clients.tabs.titles.invoices') }}</a>
    </li>
    <li role="presentation">
        <a href="#quotes" aria-controls="quotes" role="tab" data-toggle="tab">{{ trans('labels.backend.clients.tabs.titles.quotes') }}</a>
    </li>
    <li role="presentation">
        <a href="#transactions" aria-controls="transactions" role="tab" data-toggle="tab">{{ trans('labels.backend.clients.tabs.titles.transactions') }}</a>
    </li>
    <li role="presentation">
        <a href="#tickets" aria-controls="tickets" role="tab" data-toggle="tab">{{ trans('labels.backend.clients.tabs.titles.tickets') }}</a>
    </li>
    <li class="{{ Route::is('admin.notes.*') ? 'active' : '' }}" role="presentation">
        <a class="{{ Route::is('admin.notes.*') ? 'active' : '' }}" href="{{ route('admin.notes.index', ['client_id' => $client->id]) }}" aria-controls="notes" role="tab">{{ trans('labels.backend.clients.tabs.titles.notes') }}</a>
    </li>
    <li rol e="presentation">
        <a href="#emails" aria-controls="emails" role="tab" data-toggle="tab">{{ trans('labels.backend.clients.tabs.titles.emails') }}</a>
    </li>
</ul>