
        {{ Form::label('name', trans('validation.attributes.backend.orderformtemplates.name'), ['class' => 'col-lg-2 control-label required']) }}
			
        <div class="col-lg-10">
            {{ Form::text('name', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.orderformtemplates.name'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
		
		
		
    </div><!--form control-->
	 <div class="form-group">
        {{ Form::label('image', trans('validation.attributes.backend.orderformtemplates.image'), ['class' => 'col-lg-2 control-label required']) }}
        @if(!empty($orderformtemplates->image))
            <div class="col-lg-1">
                <img src="{{ Storage::disk('public')->url('img/backend/order_form_template_images/' . $orderformtemplates->image) }}" height="80" width="80">
            </div>
            <div class="col-lg-5">
                <div class="custom-file-input">
                    <input type="file" name="image" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" />
                    <label for="file-1"><i class="fa fa-upload"></i><span>Choose a file</span></label>
                </div>
            </div>
        @else
            <div class="col-lg-5">
                <div class="custom-file-input">
                        <input type="file" name="image" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" />
                        <label for="file-1"><i class="fa fa-upload"></i><span>Choose a file</span></label>
                </div>
            </div>
        @endif
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('status', trans('validation.attributes.backend.orderformtemplates.is_active'), ['class' => 'col-lg-2 control-label']) }}

        <div class="col-lg-10">
            <div class="control-group">
                <label class="control control--checkbox">
				 {{ Form::checkbox('status', 1, null) }}
                       
                    <div class="control__indicator"></div>
                </label>
            </div>
        </div><!--col-lg-3-->
    </div>
