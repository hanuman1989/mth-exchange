@extends ('backend.layouts.app')

@section('breadcrumb')
<div class="content-top-sec">
    <nav aria-label="breadcrumb">
        {{ Breadcrumbs::generate('common',['append' => [['label'=> $getController,'action' => $getAction,'route'=> 'admin.securityquestions','id'=>isset($securityquestions->id)?$securityquestions->id:0]]]) }}
    </nav> 
    <h1>Security Question Manager</h1>
</div>
@endsection

@section ('title', trans('labels.backend.securityquestions.management'))

@section('page-header')
    <h1>{{ trans('labels.backend.securityquestions.management') }}</h1>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.securityquestions.management') }}</h3>

            <div class="box-tools pull-right">
                @include('backend.securityquestions.partials.securityquestions-header-buttons')
            </div>
        </div><!--box-header with-border-->

        <div class="box-body">
            <div class="table-responsive data-table-wrapper">
                <table id="securityquestions-table" class="table table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>{{ trans('labels.backend.securityquestions.table.question') }}</th>
                            <th>{{ trans('labels.backend.securityquestions.table.status') }}</th>
                            <th>{{ trans('labels.backend.securityquestions.table.createdat') }}</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                    <thead class="transparent-bg">
                        <tr>
                            <th></th>
                            <th>
                            {!! Form::select('status', [0 => "InActive", 1 => "Active"], null, ["class" => "search-input-select form-control", "data-column" => 1, "placeholder" => trans('labels.backend.master-data.countries.table.all')]) !!}
                            </th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection

@push('after-scripts')
    {{-- For DataTables --}}
    {{ Html::script('js/dataTable.js') }}

    <script>
        //Below written line is short form of writing $(document).ready(function() { })
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            var dataTable = $('#securityquestions-table').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.securityquestions.get") }}',
                    type: 'post'
                },
                columns: [
                    {data: 'question', name: '{{config('module.securityquestions.table')}}.question'},
                    {data: 'status', name: '{{config('module.securityquestions.table')}}.status'},
                    {data: 'created_at', name: '{{config('module.securityquestions.table')}}.created_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                dom: 'lBfrtip',
                buttons: {
                    buttons: [
                        { extend: 'copy', className: 'copyButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'csv', className: 'csvButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'excel', className: 'excelButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'pdf', className: 'pdfButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'print', className: 'printButton',  exportOptions: {columns: [ 0, 1 ]  }}
                    ]
                }
            });

            Backend.DataTableSearch.init(dataTable);
        });
    </script>
@endpush
