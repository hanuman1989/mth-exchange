<div class="box-body">
    <div class="form-group">
        {{ Form::label('question', trans('labels.backend.securityquestions.table.question'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            {{ Form::text('question', null, ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.securityquestions.table.question'), 'required', 'maxlength' => 255]) }}
        </div><!--col-lg-10-->
    </div><!--form control-->

    {{-- Status --}}
    <div class="form-group">
        {{ Form::label('status', trans('labels.backend.securityquestions.table.status'), ['class' => 'col-lg-2 control-label']) }}

        <div class="col-lg-1">
                    <div class="control-group">
                        <label class="control control--checkbox">
                        @if(isset($securityquestion->status) && !empty ($securityquestion->status))
                            {{ Form::checkbox('status', 1, true) }}
                        @else
                            {{ Form::checkbox('status', 1, false) }}
                        @endif
                        <div class="control__indicator"></div>
                        </label>
                    </div>
        </div><!--col-lg-1-->
    </div><!--form control-->
</div>