@extends ('backend.layouts.app')

@section('breadcrumb')
<div class="content-top-sec">
    <nav aria-label="breadcrumb">
        {{ Breadcrumbs::generate('common',['append' => [['label'=> $getController,'action' => $getAction,'route'=> 'admin.securityquestions','id'=>isset($securityquestions->id)?$securityquestions->id:0]]]) }}
    </nav> 
    <h1>Security Question Manager</h1>
</div>
@endsection

@section ('title', trans('labels.backend.securityquestions.management') . ' | ' . trans('labels.backend.securityquestions.create'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.securityquestions.management') }}
        <small>{{ trans('labels.backend.securityquestions.create') }}</small>
    </h1>
@endsection

@section('content')
    {{ Form::open(['route' => 'admin.securityquestions.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-securityquestion']) }}

        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('labels.backend.securityquestions.create') }}</h3>

                <div class="box-tools pull-right">
                    @include('backend.securityquestions.partials.securityquestions-header-buttons')
                </div><!--box-tools pull-right-->
            </div><!--box-header with-border-->

            <div class="box-body">
                <div class="form-group">
                    {{-- Including Form blade file --}}
                    @include("backend.securityquestions.form")
                    <div class="edit-form-btn">
                        {{ link_to_route('admin.securityquestions.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-md']) }}
                        {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-primary btn-md']) }}
                        <div class="clearfix"></div>
                    </div><!--edit-form-btn-->
                </div><!-- form-group -->
            </div><!--box-body-->
        </div><!--box box-success-->
    {{ Form::close() }}
@endsection
