<!--Action Button-->
<div class="btn-group">
  <button type="button" class="btn btn-primary btn-flat dropdown-toggle" data-toggle="dropdown">Action
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><a href="{{route('admin.email.index')}}"><i class="fa fa-list-ul"></i> {{trans('menus.backend.emailtemplates.all')}}</a></li>
    <li><a href="{{route('admin.email.create')}}"><i class="fa fa-plus"></i> {{trans('menus.backend.emailtemplates.create')}}</a></li>
  </ul>
</div>
<div class="clearfix"></div>
