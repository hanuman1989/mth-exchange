<div class="create-invoice-form">
    {{ Form::hidden('user_id', $client->id) }}

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('invoice_number', trans('labels.backend.invoices.table.invoice_number'), ['class' => ' control-label required']) }}
                <div>
                    {{ Form::text('invoice_number', null, ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.invoices.table.invoice_number'), 'required']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('invoice_date', trans('labels.backend.invoices.table.invoice_date'), ['class' => ' control-label required']) }}
                <div>
                    {{ Form::text('invoice_date', null, ['class' => 'form-control box-size date-picker', 'placeholder' => trans('labels.backend.invoices.table.invoice_date'), 'required']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('line_item_description', trans('labels.backend.invoices.table.line_item_description'), ['class' => ' control-label required']) }}
                <div>
                    {{ Form::text('line_item_description', null, ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.invoices.table.line_item_description'), 'required']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('due_date', trans('labels.backend.invoices.table.due_date'), ['class' => ' control-label required']) }}
                <div>
                    {{ Form::text('due_date', null, ['class' => 'form-control box-size date-picker', 'placeholder' => trans('labels.backend.invoices.table.due_date'), 'required']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('payment_method', trans('labels.backend.invoices.table.payment_method'), ['class' => ' control-label required']) }}
                <div>
                    {{ Form::select('payment_method', config('constants.payment_method'), null , ['class' => 'form-control  status box-size']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('date_paid', trans('labels.backend.invoices.table.date_paid'), ['class' => ' control-label required']) }}
                <div>
                    {{ Form::text('date_paid', null, ['class' => 'form-control box-size date-picker', 'placeholder' => trans('labels.backend.invoices.table.date_paid'), 'required']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('status', trans('labels.backend.invoices.table.status'), ['class' => ' control-label required']) }}
                <div>
                    {{ Form::select('status', config('constants.status'), null , ['class' => 'form-control  status box-size']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('due_from', trans('labels.backend.invoices.table.total_due'), ['class' => ' control-label required']) }}
                <div>
                    {{ Form::text('due_from', null, ['class' => 'form-control box-size col-lg-6  date-picker', 'placeholder' => trans('labels.backend.invoices.table.due_from'), 'required']) }}
                </div><!--col-lg-10-->
                <div>
                    {{ Form::text('due_to', null, ['class' => 'form-control box-size col-lg-6  date-picker', 'placeholder' => trans('labels.backend.invoices.table.due_to'), 'required']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div>
    </div>
</div>

@push("after-scripts")
    <script type="text/javascript">
        //Put your javascript needs in here.
        //Don't forget to put `@`parent exactly after `@`section("after-scripts"),
        //if your create or edit blade contains javascript of its own
        $( document ).ready( function() {
            //Everything in here would execute after the DOM is ready to manipulated.
        });
    </script>
@endpush
