@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.invoices.management'))

@section('page-header')
    <h1>{{ trans('labels.backend.invoices.management') }}</h1>
@endsection

@section('content')
<div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.clients.view') }}</h3>
        </div><!-- /.box-header -->

        <div class="box-body">

            <div role="tabpanel">

                @include('backend.clients.partials.client-header-tabs')
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane mt-30 active" id="summary">
                        <h3 class="box-title">{{ trans('labels.backend.invoices.create') }}</h3>
                        {{ Form::open(['route' => 'admin.invoices.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-invoice']) }}
                            <!-- include form to create invoice -->
                            @include('backend.invoices.form')
                            <div class="pull-right create-invoice-btn edit-form-btn">
                                {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-primary btn-md']) }}
                                <div class="clearfix"></div>
                            </div><!--edit-form-btn-->
                        {{ Form::close() }}
                        <div class="table-responsive data-table-wrapper">
                            <table id="invoices-table" class="table table-condensed table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>{{ trans('labels.backend.invoices.table.invoice_number') }}</th>
                                        <th>{{ trans('labels.backend.invoices.table.line_item_description') }}</th>
                                        <th>{{ trans('labels.backend.invoices.table.payment_method') }}</th>
                                        <th>{{ trans('labels.backend.invoices.table.status') }}</th>
                                        <th>{{ trans('labels.backend.invoices.table.invoice_date') }}</th>
                                        <th>{{ trans('labels.backend.invoices.table.due_date') }}</th>
                                        <th>{{ trans('labels.backend.invoices.table.date_paid') }}</th>
                                        <th>{{ trans('labels.backend.invoices.table.due_from') }}</th>
                                        <th>{{ trans('labels.backend.invoices.table.due_to') }}</th>
                                        <th>{{ trans('labels.backend.invoices.table.createdat') }}</th>
                                        <th>{{ trans('labels.general.actions') }}</th>
                                    </tr>
                                </thead>
                                <thead class="transparent-bg">
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div><!--table-responsive-->
                    </div>
                </div>
                    

            </div><!--tab panel-->

        </div><!-- /.box-body -->
    </div><!--box-->
@endsection

@push('after-scripts')
    {{-- For DataTables --}}
    {{ Html::script(mix('js/dataTable.js')) }}

    <script>
        //Below written line is short form of writing $(document).ready(function() { })
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            var dataTable = $('#invoices-table').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.invoices.get") }}',
                    type: 'post',
                    data : {
                        'client_id' : {{ $client->id }}
                    }
                },
                columns: [
                    {data: 'invoice_number', name: '{{config('module.invoices.table')}}.invoice_number'},
                    {data: 'line_item_description', name: '{{config('module.invoices.table')}}.line_item_description'},
                    {data: 'payment_method', name: '{{config('module.invoices.table')}}.payment_method'},
                    {data: 'status', name: '{{config('module.invoices.table')}}.status'},
                    {data: 'invoice_date', name: '{{config('module.invoices.table')}}.invoice_date'},
                    {data: 'due_date', name: '{{config('module.invoices.table')}}.due_date'},
                    {data: 'date_paid', name: '{{config('module.invoices.table')}}.date_paid'},
                    {data: 'due_from', name: '{{config('module.invoices.table')}}.due_from'},
                    {data: 'due_to', name: '{{config('module.invoices.table')}}.due_to'},
                    {data: 'created_at', name: '{{config('module.invoices.table')}}.created_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                dom: 'lBfrtip',
                buttons: {
                    buttons: [
                        { extend: 'copy', className: 'copyButton',  exportOptions: {columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ]  }},
                        { extend: 'csv', className: 'csvButton',  exportOptions: {columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ]  }},
                        { extend: 'excel', className: 'excelButton',  exportOptions: {columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ]  }},
                        { extend: 'pdf', className: 'pdfButton',  exportOptions: {columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ]  }},
                        { extend: 'print', className: 'printButton',  exportOptions: {columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ]  }}
                    ]
                }
            });

            Backend.DataTableSearch.init(dataTable);
        });
        $(".date-picker").datepicker({ 
            autoclose: true, 
            todayHighlight: true,
            format : 'yyyy-mm-dd'
        });
    </script>
@endpush
