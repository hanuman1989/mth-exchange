<div class="btn-group">
    <button type="button" class="btn btn-primary btn-flat dropdown-toggle" data-toggle="dropdown">Action
        <span class="sr-only">Toggle Dropdown</span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li>
            <a href="{{ route( 'admin.notes.index' , ['client_id' => $client->id]) }}">
                <i class="fa fa-list-ul"></i> {{ trans( 'menus.backend.notes.all' ) }}
            </a>
        </li>
        @permission( 'create-note' )
            <li>
                <a href="{{ route( 'admin.notes.create', ['client_id' => $client->id] ) }}">
                    <i class="fa fa-plus"></i> {{ trans( 'menus.backend.notes.create' ) }}
                </a>
            </li>
        @endauth
    </ul>
</div>
<div class="clearfix"></div>
