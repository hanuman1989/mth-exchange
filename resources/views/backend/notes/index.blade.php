@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.clients.management') . ' | ' . trans('labels.backend.clients.view'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.clients.management') }}
        <small>{{ trans('labels.backend.clients.view') }}</small>
    </h1>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.clients.view') }}</h3>

            <div class="box-tools pull-right">
                @include('backend.notes.partials.notes-header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">

            <div role="tabpanel">

                @include('backend.clients.partials.client-header-tabs')
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane mt-30 active" id="summary">
                        <div class="table-responsive data-table-wrapper">
                            <table id="notes-table" class="table table-condensed table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>{{ trans('labels.backend.notes.table.notes') }}</th>
                                        <th>{{ trans('labels.backend.notes.table.added_by') }}</th>
                                        <th>{{ trans('labels.backend.notes.table.createdat') }}</th>
                                        <th>{{ trans('labels.general.actions') }}</th>
                                    </tr>
                                </thead>
                                <thead class="transparent-bg">
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div><!--table-responsive-->
                    </div>
                </div>
                    

            </div><!--tab panel-->

        </div><!-- /.box-body -->
    </div><!--box-->
@endsection

@push('after-scripts')
    {{-- For DataTables --}}
    {{ Html::script(mix('js/dataTable.js')) }}

    <script>
        //Below written line is short form of writing $(document).ready(function() { })
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            var dataTable = $('#notes-table').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.notes.get") }}',
                    type: 'post'
                },
                columns: [
                    {data: 'notes', name: '{{config('module.notes.table')}}.notes'},
                    {data: 'added_by', name: '{{config('module.notes.table')}}.added_by'},
                    {data: 'created_at', name: '{{config('module.notes.table')}}.created_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                dom: 'lBfrtip',
                buttons: {
                    buttons: [
                        { extend: 'copy', className: 'copyButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'csv', className: 'csvButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'excel', className: 'excelButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'pdf', className: 'pdfButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'print', className: 'printButton',  exportOptions: {columns: [ 0, 1 ]  }}
                    ]
                }
            });

            Backend.DataTableSearch.init(dataTable);
        });
    </script>
@endpush
