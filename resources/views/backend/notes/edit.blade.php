@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.notes.management') . ' | ' . trans('labels.backend.notes.edit'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.notes.management') }}
        <small>{{ trans('labels.backend.notes.edit') }}</small>
    </h1>
@endsection

@section('content')
    {{ Form::model($notes, ['route' => ['admin.notes.update', $notes], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-note']) }}

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.clients.view') }}</h3>
            <div class="box-tools pull-right">
                @include('backend.notes.partials.notes-header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">

            <div role="tabpanel">

                @include('backend.clients.partials.client-header-tabs')
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane mt-30 active" id="summary">
                        <div class="form-group">
                            {{-- Including Form blade file --}}
                            @include("backend.notes.form")
                            <div class="edit-form-btn">
                                {{ link_to_route('admin.notes.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-md']) }}
                                {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-primary btn-md']) }}
                                <div class="clearfix"></div>
                            </div><!--edit-form-btn-->
                        </div><!-- form-group -->
                    </div>
                </div>
            </div><!--tab panel-->

        </div><!-- /.box-body -->
    </div><!--box-->
    {{ Form::close() }}
@endsection
