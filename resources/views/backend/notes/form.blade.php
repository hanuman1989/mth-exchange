<div class="box-body">
    <div class="form-group">
        {{ Form::label('notes', trans('labels.backend.notes.table.notes'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            {{ Form::textarea('notes', null, ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.notes.table.notes'), 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form control-->

    {{ Form::hidden('user_id', $client->id) }}
    {{ Form::hidden('added_by', auth()->user()->contact_name) }}

</div>