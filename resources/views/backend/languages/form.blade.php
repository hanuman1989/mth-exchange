<div class="box-body">
    <div class="form-group">
        {{ Form::label('name', trans('labels.backend.languages.table.name'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            {{ Form::text('name', null, ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.languages.table.name'), 'required', 'maxlength' => 255]) }}
        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('iso_639-1', trans('labels.backend.languages.table.iso_639-1'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            {{ Form::text('iso_639-1', null, ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.languages.table.iso_639-1'), 'required', 'maxlength' => 2]) }}
        </div><!--col-lg-10-->
    </div><!--form control-->

    {{-- Status --}}
    <div class="form-group">
        {{ Form::label('status', trans('labels.backend.languages.table.status'), ['class' => 'col-lg-2 control-label']) }}

        <div class="col-lg-1">
                    <div class="control-group">
                        <label class="control control--checkbox">
                        @if(isset($language->status) && !empty ($language->status))
                            {{ Form::checkbox('status', 1, true) }}
                        @else
                            {{ Form::checkbox('status', 1, false) }}
                        @endif
                        <div class="control__indicator"></div>
                        </label>
                    </div>
        </div><!--col-lg-1-->
    </div><!--form control-->
</div>