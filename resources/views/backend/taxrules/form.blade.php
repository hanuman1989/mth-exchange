<div class="box-body">
    <div class="form-group">
        {{ Form::label('tax_name', trans('labels.backend.taxrules.table.tax_name'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            {{ Form::text('tax_name', null, ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.taxrules.table.tax_name'), 'required', 'maxlength' => 100]) }}
        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('country_id', trans('Select Country'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            {!! Form::select('country_id',$countries, isset($taxrule->country->country_id) ? $clientdetail->detail->country_id : null , ["class" => "form-control box-size", "placeholder" => "Select Country"]) !!}
        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('tax_rate', trans('labels.backend.taxrules.table.tax_rate'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            {{ Form::text('tax_rate', null, ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.taxrules.table.tax_rate'), 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form control-->
    
    {{-- Status --}}
    <div class="form-group">
        {{ Form::label('status', trans('labels.backend.taxrules.table.status'), ['class' => 'col-lg-2 control-label']) }}

        <div class="col-lg-1">
                    <div class="control-group">
                        <label class="control control--checkbox">
                        @if(isset($taxrules->status) && !empty ($taxrules->status))
                            {{ Form::checkbox('status', 1, true) }}
                        @else
                            {{ Form::checkbox('status', 1, false) }}
                        @endif
                        <div class="control__indicator"></div>
                        </label>
                    </div>
        </div><!--col-lg-1-->
    </div><!--form control-->
</div>