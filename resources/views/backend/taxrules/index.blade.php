@extends ('backend.layouts.app')

@section('breadcrumb')
<div class="content-top-sec">
    <nav aria-label="breadcrumb">
        {{ Breadcrumbs::generate('common',['append' => [['label'=> $getController,'action' => $getAction,'route'=> 'admin.taxrules','id'=>isset($taxrules->id)?$taxrules->id:0]]]) }}
    </nav> 
    <h1>Tax Rule Manager</h1>
</div>
@endsection

@section ('title', trans('labels.backend.taxrules.management'))

@section('page-header')
    <h1>{{ trans('labels.backend.taxrules.management') }}</h1>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.taxrules.management') }}</h3>

            <div class="box-tools pull-right">
                @include('backend.taxrules.partials.taxrules-header-buttons')
            </div>
        </div><!--box-header with-border-->

        <div class="box-body">
            <div class="table-responsive data-table-wrapper">
                <table id="taxrules-table" class="table table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>{{ trans('labels.backend.taxrules.table.tax_name') }}</th>
                            <th>{{ trans('labels.backend.taxrules.table.tax_rate') }}</th>
                            <th>{{ trans('labels.backend.taxrules.table.country_id') }}</th>
                            <th>{{ trans('labels.backend.taxrules.table.status') }}</th>
                            <th>{{ trans('labels.backend.taxrules.table.createdat') }}</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                    <thead class="transparent-bg">
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th>
                            {!! Form::select('status', ['0' => 'Inactive', '1' => 'active'], null, ["class" => "search-input-select form-control", "data-column" => 3, "placeholder" => trans('labels.backend.master-data.countries.table.all')]) !!}
                            </th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection

@push('after-scripts')
    {{-- For DataTables --}}
    {{ Html::script('js/dataTable.js') }}

    <script>
        //Below written line is short form of writing $(document).ready(function() { })
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            var dataTable = $('#taxrules-table').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.taxrules.get") }}',
                    type: 'post'
                },
                columns: [
                    {data: 'tax_name', name: '{{config('module.taxrules.table')}}.tax_name'},
                    {data: 'tax_rate', name: '{{config('module.taxrules.table')}}.tax_rate'},
                    {data: 'country_id', name: 'country.name'},
                    {data: 'status', name: '{{config('module.taxrules.table')}}.status'},
                    {data: 'created_at', name: '{{config('module.taxrules.table')}}.created_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                dom: 'lBfrtip',
                buttons: {
                    buttons: [
                        { extend: 'copy', className: 'copyButton',  exportOptions: {columns: [ 0, 1, 2, 3, 4, 5 ]  }},
                        { extend: 'csv', className: 'csvButton',  exportOptions: {columns: [ 0, 1, 2, 3, 4, 5 ]  }},
                        { extend: 'excel', className: 'excelButton',  exportOptions: {columns: [ 0, 1, 2, 3, 4, 5 ]  }},
                        { extend: 'pdf', className: 'pdfButton',  exportOptions: {columns: [ 0, 1, 2, 3, 4, 5 ]  }},
                        { extend: 'print', className: 'printButton',  exportOptions: {columns: [ 0, 1, 2, 3, 4, 5 ]  }}
                    ]
                }
            });

            Backend.DataTableSearch.init(dataTable);
        });
    </script>
@endpush
