<div class="box-body">
    <div class="form-group">
        {{ Form::label('name', trans('labels.backend.master-data.states.table.name'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            {{ Form::text('name', null, ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.master-data.states.table.name'), 'required', 'maxlength' => 100]) }}
        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('code', trans('labels.backend.master-data.states.table.code'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            {{ Form::text('code', null, ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.master-data.states.table.code'), 'required', 'maxlength' => 2]) }}
        </div><!--col-lg-10-->
    </div><!--form control-->

    {{-- Status --}}
    <div class="form-group">
        {{ Form::label('status', trans('labels.backend.master-data.states.table.status'), ['class' => 'col-lg-2 control-label']) }}

        <div class="col-lg-1">
                    <div class="control-group">
                        <label class="control control--checkbox">
                        @if(isset($state->status) && !empty ($state->status))
                            {{ Form::checkbox('status', 1, true) }}
                        @else
                            {{ Form::checkbox('status', 1, false) }}
                        @endif
                        <div class="control__indicator"></div>
                        </label>
                    </div>
        </div><!--col-lg-1-->
    </div><!--form control-->
</div>