@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.master-data.countries.management') . ' | ' . trans('labels.backend.master-data.countries.create'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.master-data.countries.management') }}
        <small>{{ trans('labels.backend.master-data.countries.create') }}</small>
    </h1>
@endsection

@section('content')
    {{ Form::open(['route' => 'admin.master-data.countries.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-state']) }}

        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('labels.backend.master-data.countries.create') }}</h3>

                <div class="box-tools pull-right">
                @permission('create-state')
                <a class="btn btn-primary btn-flat" style='color:#fff;' href="{{ route( 'admin.master-data.countries.index' ) }}">
                    <i class="fa fa-list"></i> {{trans('List')}}
                </a>
                <div class="clearfix"></div>
                @endauth

                </div><!--box-tools pull-right-->
            </div><!-- /.box-header -->

            {{-- Including Form blade file --}}
            <div class="box-body">
                <div class="form-group">
                    @include("backend.master-data.countries.form")
                    <div class="edit-form-btn">
                    {{ link_to_route('admin.master-data.countries.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-md']) }}
                    {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-primary btn-md']) }}
                    <div class="clearfix"></div>
                </div>
            </div>
        </div><!--box-->
    </div>
    {{ Form::close() }}
@endsection
