@extends ('backend.layouts.app')

@section('breadcrumb')
<div class="content-top-sec">
    <nav aria-label="breadcrumb">
        {{ Breadcrumbs::generate('common',['append' => [['label'=> $getController,'action' => $getAction,'route'=> 'admin.backuplogs','id'=>isset($backuplogs->id)?$backuplogs->id:0]]]) }}
    </nav> 
    <h1>Backup Logs Manager</h1>
</div>
@endsection

@section ('title', trans('labels.backend.backuplogs.management'))

@section('page-header')
    <h1>{{ trans('labels.backend.backuplogs.management') }}</h1>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.backuplogs.management') }}</h3>

            <div class="box-tools pull-right">
                <div class="btn-group">
                    <button type="button" class="btn btn-warning btn-flat dropdown-toggle" data-toggle="dropdown">Export
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li id="copyButton"><a href="#"><i class="fa fa-clone"></i> Copy</a></li>
                        <li id="csvButton"><a href="#"><i class="fa fa-file-text-o"></i> CSV</a></li>
                        <li id="excelButton"><a href="#"><i class="fa fa-file-excel-o"></i> Excel</a></li>
                        <li id="pdfButton"><a href="#"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                        <li id="printButton"><a href="#"><i class="fa fa-print"></i> Print</a></li>
                    </ul>
                </div>
            </div>
        </div><!--box-header with-border-->

        <div class="box-body">
            <div class="table-responsive data-table-wrapper">
                <table id="activities-table" class="table table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>{!! trans('labels.backend.backuplogs.table.file') !!}</th>
                            <th>{{ trans('labels.backend.backuplogs.table.created_at') }}</th>
                        </tr>
                    </thead>
                    <thead class="transparent-bg">
                        <tr>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection

@push('after-scripts')
    {{-- For DataTables --}}
    {{ Html::script('js/dataTable.js') }}

    <script>
        //Below written line is short form of writing $(document).ready(function() { })
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            var dataTable = $('#activities-table').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.backuplogs.get") }}',
                    type: 'post'
                },
                columns: [
                    {data: 'file', name: '{{config('module.backuplogs.table')}}.file'},
                    {data: 'created_at', name: '{{config('module.backuplogs.table')}}.created_at'},
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                dom: 'lBfrtip',
                buttons: {
                    buttons: [
                        { extend: 'copy', className: 'copyButton',  exportOptions: {columns: [ 0, 1, 2, 3 ]  }},
                        { extend: 'csv', className: 'csvButton',  exportOptions: {columns: [ 0, 1, 2, 3 ]  }},
                        { extend: 'excel', className: 'excelButton',  exportOptions: {columns: [ 0, 1, 2, 3 ]  }},
                        { extend: 'pdf', className: 'pdfButton',  exportOptions: {columns: [ 0, 1, 2, 3 ]  }},
                        { extend: 'print', className: 'printButton',  exportOptions: {columns: [ 0, 1, 2, 3 ]  }}
                    ]
                }
            });

            Backend.DataTableSearch.init(dataTable);
        });
    </script>
@endpush
