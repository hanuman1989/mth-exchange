  {{ Form::label('name', trans('validation.attributes.backend.productgroups.name'), ['class' => 'col-lg-2 control-label required']) }}
  			
          <div class="col-lg-10">
              {{ Form::text('name', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.productgroups.name'), 'required' => 'required']) }}
          </div><!--col-lg-10-->
      </div><!--form control-->
  	
  	<div class="form-group">
          {{ Form::label('head_line', trans('validation.attributes.backend.productgroups.head_line'), ['class' => 'col-lg-2 control-label required']) }}
         <div class="col-lg-10">
              {{ Form::text('head_line', null, ['class' => 'form-control box-size', 'placeholder' =>trans('validation.attributes.backend.productgroups.head_line'), 'required' => 'required']) }}
          </div><!--col-lg-10-->
            
      </div><!--form control-->
  	<div class="form-group">
          {{ Form::label('tag_line', trans('validation.attributes.backend.productgroups.tag_line'), ['class' => 'col-lg-2 control-label required']) }}
         <div class="col-lg-10">
              {{ Form::text('tag_line', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.productgroups.tag_line'), 'required' => 'required']) }}
          </div><!--col-lg-10-->
            
      </div><!--form control-->
  	
  	<div class="form-group">
  	
       {{ Form::label('group_feature', trans('validation.attributes.backend.productgroups.group_features'), ['class' => 'col-lg-2 control-label ']) }}   
          
   @if(isset($productgroups->id) && !empty ($productgroups->id))
  	
    <div class="col-lg-8" id="saved-feature">
  @if($selectedGroupFeatures->count() != 0)

       @foreach($selectedGroupFeatures as $selectedGroupFeature)
  	 <div class="col-lg-8" id="feature{{$selectedGroupFeature->id}}">{{$selectedGroupFeature->name}}  <a href="javascript:void(0)"  onclick="deleteFeature({{$selectedGroupFeature->id}})" class="delete-feature" class="btn btn-primary btn-md">X</a></div>
  	 
  	 @endforeach
  	 @endif
  </div>
    
    
    <div class="col-lg-8"> 
     {{ Form::text('group_features', null, ['class' => 'form-control box-size','id'=>'group-features', 'placeholder' =>trans('validation.attributes.backend.productgroups.group_features')]) }}
     </div>
      <div class="col-lg-2"> 
    
     <a href="javascript:void(0)" onclick="groupFeature()" class="btn btn-primary btn-md">Add New </a>

    </div>

  @else	
  	<div class="col-lg-10">
          You must save the product group for the first time before you can add features
  	 </div>
  	@endif

          </div><!--col-lg-10-->
            
      </div>
  	
  	<div class="form-group">
          {{ Form::label('order_form_template', trans('validation.attributes.backend.productgroups.order_form_template'), ['class' => 'col-lg-2 control-label required']) }}
         <div class="col-lg-10"> 
  	   
      @if($orderForms->count() != 0)

       @foreach($orderForms as $orderForm)
  						
     <div class="col-lg-3"> 						
  	<img src="{{ Storage::disk('public')->url('img/backend/order_form_template_images/' . $orderForm->image) }}" height="100" width="100">

  	<p>{{$orderForm->name}}</p>
     {{Form::radio('order_form_template_id', $orderForm->id,null)}}
     </div>

  @endforeach

                                      @endif
          </div><!--col-lg-10-->
            
      </div>
  	
  	<div class="form-group">
          {{ Form::label('available_payment_gateway', trans('validation.attributes.backend.productgroups.available_payment_gateway'), ['class' => 'col-lg-2 control-label required']) }}
         <div class="col-lg-10"> 
  	   
   @if($paymentRecords->count() != 0)

                             @foreach($paymentRecords as $paymentRecord)
  						
  <div class="col-lg-3"> 						
  						

  	<p>{{$paymentRecord->name}}</p>

  	
  <?php

  $checked = false;

    if(isset($productgroups->paymentGateway) && !empty($productgroups->paymentGateway))
       
  		if(in_array($paymentRecord->id, $productgroups->paymentGateway->pluck('id')->toArray())) $checked = true;

  ?>

  {{Form::checkbox('payment_gateway_id[]', $paymentRecord->id,$checked)}}
  </div>

  @endforeach

                                      @endif
          </div><!--col-lg-10-->
            
      </div>
  	
      <div class="form-group">
          {{ Form::label('hidden', trans('validation.attributes.backend.productgroups.hidden'), ['class' => 'col-lg-2 control-label']) }}

          <div class="col-lg-10">
              <div class="control-group">
                  <label class="control control--checkbox">
				   {{ Form::checkbox('hidden', 1, null) }}
                         
                      <div class="control__indicator"></div>
                  </label>
              </div>
          </div><!--col-lg-3-->
      </div>
  	
