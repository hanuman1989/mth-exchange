@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.productgroups.management') . ' | ' . trans('labels.backend.productgroups.edit'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.productgroups.management') }}
        <small>{{ trans('labels.backend.productgroups.edit') }}</small>
    </h1>
@endsection

@section('content')
    {{ Form::model($productgroups, ['route' => ['admin.productgroups.update', $productgroups], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-productgroup']) }}

        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('labels.backend.productgroups.edit') }}</h3>

                <div class="box-tools pull-right">
                    @include('backend.productgroups.partials.productgroups-header-buttons')
                </div><!--box-tools pull-right-->
            </div><!--box-header with-border-->

            <div class="box-body">
                <div class="form-group">
                    {{-- Including Form blade file --}}
                    @include("backend.productgroups.form")
                    <div class="edit-form-btn">
                        {{ link_to_route('admin.productgroups.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-md']) }}
                        {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-primary btn-md']) }}
                        <div class="clearfix"></div>
                    </div><!--edit-form-btn-->
                </div><!--form-group-->
            </div><!--box-body-->
        </div><!--box box-success -->
    {{ Form::close() }}
@endsection

<script>
	function deleteFeature(deleteId){
		$.ajax({
            type: "post",
            url: '{{url("admin/delete_features")}}',
            data: {'id':deleteId},
            "headers": {
                "accept": "application/json",
            },
            
            success: function (response) {
                var data = JSON.parse(response);
                if (data['status'] == 'success') {
					$('#feature'+deleteId).hide();
                } else {
                   
                }
            }, error: function (error) {
               
            }, complete: function () {
               
            },
        });
	
	}
	
	
	
	function groupFeature(){
		var featureValue = $('#group-features').val();
		if(featureValue == ''){
			return false;
		}
		var productGroupId = {{$productgroups->id }}
		
		$.ajax({
            type: "post",
            url: '{{url("admin/group_features")}}',
            data: {'featureValue':featureValue,'product_group_id':productGroupId},
            "headers": {
                "accept": "application/json",
            },
            
            success: function (response) {
                var data = JSON.parse(response);
                if (data['status'] == 'success') {
					var id = data['id'];
                   $('#saved-feature').append('<div class="col-lg-8" id="feature'+id+'">'+featureValue+'  <a href="javascript:void(0)"  onclick="deleteFeature('+id+')" class="delete-feature" class="btn btn-primary btn-md">X</a></div>');
				   $('#group-features').val('');
                } else {
                   
                }
            }, error: function (error) {
               
            }, complete: function () {
               
            },
        });
	
	}
	</script>
