@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.productgroups.management'))

@section('page-header')
    <h1>{{ trans('labels.backend.productgroups.management') }}</h1>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.productgroups.management') }}</h3>

            <div class="box-tools pull-right">
                @include('backend.productgroups.partials.productgroups-header-buttons')
            </div>
        </div><!--box-header with-border-->

        <div class="box-body">
            <div class="table-responsive data-table-wrapper">
                <table id="productgroups-table" class="table table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>{{ trans('labels.backend.productgroups.table.name') }}</th>
							<th>{{ trans('labels.backend.productgroups.table.head_line') }}</th>
							<th>{{ trans('labels.backend.productgroups.table.tag_line') }}</th>
                            <th>{{ trans('labels.backend.productgroups.table.createdat') }}</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                    <thead class="transparent-bg">
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection

@push('after-scripts')

    <script>
        //Below written line is short form of writing $(document).ready(function() { })
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            var dataTable = $('#productgroups-table').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.productgroups.get") }}',
                    type: 'post'
                },
                columns: [
                    {data: 'name', name: '{{config('module.product_groups.table')}}.name'},
					 {data: 'head_line', name: '{{config('module.product_groups.table')}}.head_line'},
					  {data: 'tag_line', name: '{{config('module.product_groups.table')}}.tag_line'},
                    {data: 'created_at', name: '{{config('module.product_groups.table')}}.created_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                dom: 'lBfrtip',
                buttons: {
                    buttons: [
                        { extend: 'copy', className: 'copyButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'csv', className: 'csvButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'excel', className: 'excelButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'pdf', className: 'pdfButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'print', className: 'printButton',  exportOptions: {columns: [ 0, 1 ]  }}
                    ]
                }
            });

            Backend.DataTableSearch.init(dataTable);
        });
    </script>
@endpush
