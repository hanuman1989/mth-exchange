@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.ticketreplies.management'))

@section('page-header')
    <h1>{{ trans('labels.backend.ticketreplies.management') }}</h1>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.ticketreplies.management') }}</h3>

            <div class="box-tools pull-right">
                @include('backend.ticketreplies.partials.ticketreplies-header-buttons')
            </div>
        </div><!--box-header with-border-->

        <div class="box-body">
            <div class="table-responsive data-table-wrapper">
                <table id="ticketreplies-table" class="table table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>{{ trans('labels.backend.ticketreplies.table.id') }}</th>
							<th>{{ trans('labels.backend.ticketreplies.table.subject') }}</th>
							<th>{{ trans('labels.backend.ticketreplies.table.status') }}</th>
                            <th>{{ trans('labels.backend.ticketreplies.table.createdat') }}</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                    <thead class="transparent-bg">
                        <tr>
                            <th></th>
							<th> {!! Form::text('subject', null, ["class" => "search-input-text form-control", "data-column" => 0, "placeholder" => trans('labels.backend.ticketreplies.table.subject')]) !!}
                                <a class="reset-data" href="javascript:void(0)"><i class="fa fa-times"></i></a></th>
								<th>
                               {!! Form::select('status', [0 => "InActive", 1 => "Active"], null, ["class" => "search-input-select form-control", "data-column" => 1, "placeholder" => trans('labels.backend.ticketreplies.table.all')]) !!}
                            </th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection

@push('after-scripts')
    {{-- For DataTables --}}
    {{ Html::script(mix('js/dataTable.js')) }}

    <script>
        //Below written line is short form of writing $(document).ready(function() { })
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            var dataTable = $('#ticketreplies-table').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.ticketreplies.get") }}',
                    type: 'post'
                },
                columns: [
                    {data: 'id', name: '{{config('module.ticketreplies.table')}}.id'},
					 {data: 'subject', name: '{{config('module.ticketreplies.table')}}.subject'},
					  {data: 'status', name: '{{config('module.ticketreplies.table')}}.status'},
                    {data: 'created_at', name: '{{config('module.ticketreplies.table')}}.created_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                dom: 'lBfrtip',
                buttons: {
                    buttons: [
                        { extend: 'copy', className: 'copyButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'csv', className: 'csvButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'excel', className: 'excelButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'pdf', className: 'pdfButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'print', className: 'printButton',  exportOptions: {columns: [ 0, 1 ]  }}
                    ]
                }
            });

            Backend.DataTableSearch.init(dataTable);
        });
    </script>
@endpush
