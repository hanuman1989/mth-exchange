<div class="box-body">
    <div class="form-group">
        {{ Form::label('subject', trans('validation.attributes.backend.ticketreplies.subject'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            {{ Form::text('subject', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.ticketreplies.subject'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
    </div>
	
	<div class="form-group">
        {{ Form::label('content', trans('validation.attributes.backend.ticketreplies.content'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10 mce-box">
            {{ Form::textarea('content', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.ticketreplies.content')]) }}
        </div><!--col-lg-10-->
    </div><!--form control-->
	
	 <div class="form-group">
        {{ Form::label('status', trans('validation.attributes.backend.ticketreplies.is_active'), ['class' => 'col-lg-2 control-label']) }}

        <div class="col-lg-10">
            <div class="control-group">
                <label class="control control--checkbox">
                       
                            {{ Form::checkbox('status', 1, null) }}
                       
                    <div class="control__indicator"></div>
                </label>
            </div>
        </div><!--col-lg-3-->
    </div>
	
	</div>