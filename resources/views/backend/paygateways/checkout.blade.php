@extends('layouts.app')
@section('extra-style')
<style>
    .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    @media (min-width: 768px) {
        .bd-placeholder-img-lg {
            font-size: 3.5rem;
        }
    }
</style>
@endsection
@section('nav')
<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/checkout') }}">{{ __('Checkout') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>


                @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
                @endif
                @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>


                </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
@endsection

@section('content')
<div class="container">
    <div class="py-5 text-center">
        <h1>Checkout form</h1>
    </div>
    <div class="row">
        <div class="col-md-5  ">
            <h4 class="mb-3">Card information</h4>
            <form class="needs-validation" novalidate>
                @csrf
                <div class="mb-3">
                    <label for="cardholder-name">Name on card</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">@</span>
                        </div>
                        <input type="text" class="form-control" id="cardholder-name" value="Jenny Rosen" required>
                        <div class="invalid-feedback" style="width: 100%;">
                            Name on card is required
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="card-element">
                        Credit or debit card
                    </label>
                    <div id="card-element">
                        <!-- A Stripe Element will be inserted here. -->
                    </div>
                    <!-- Used to display form errors. -->

                </div>
                <hr class="mb-4">
                <button id="card-button" class="btn btn-primary btn-lg btn-block" type="submit">
                    Submit Payment Rs. 20.00
                </button>
                <hr class="mb-4">

            </form>
        </div>
        <div class="col-md-7">
            <div id="card-errors" class="alert text-danger" role="alert"></div>
            <!-- <div id="card-success" class="alert text-success" role="alert"></div> -->
            <pre id="card-message"></pre>
        </div>
    </div>

</div>

@endsection

@section('extra-js')

<script>
    // Create a Stripe client.
    var stripe = Stripe('{{config('
        get ')['
        STRIPE_KEY ']}}');

    // Pass the PaymentIntent’s client secret to the client
    var paymentIntent;
    fetch('payment_intents').then(function(r) {
        return r.json();
    }).then(function(response) {
        paymentIntent = response;
        console.log("Fetched PI: ", response);
    });

    // Collect payment method details on the client
    var elements = stripe.elements();
    var cardElement = elements.create('card');
    cardElement.mount('#card-element');

    // Submit the payment to Stripe from the client
    var cardholderName = document.getElementById('cardholder-name');
    var cardButton = document.getElementById('card-button');
    var cardMessage = document.getElementById('card-message'); // for testing (to remove)

    // var clientSecret = cardButton.dataset.secret;
    cardButton.addEventListener('click', function(ev) {
        ev.preventDefault();
        cardMessage.textContent = "Calling handleCardPayment..."; // for testing (to remove)
        stripe.handleCardPayment(
            paymentIntent.client_secret, cardElement, {
                payment_method_data: {
                    billing_details: {
                        name: cardholderName.value
                    }
                }
            }
        ).then(function(result) {
            cardMessage.textContent = JSON.stringify(result, null, 2); // for testing (to remove)
            if (result.error) {
                //     // Display error.message in your UI.
                //     // Inform the user if there was an error
                var errorElement = document.getElementById('card-errors');
                errorElement.textContent = result.error.message;
            } else {
                /*var errorElement = document.getElementById('card-success');
                 errorElement.textContent = result.paymentIntent.status;*/
            }
        });
    });
</script>
@endsection