<div class="box-body">
    <div class="form-group">
        {{ Form::label('Display Name', 'Display Name', ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-8">
            {{ Form::text('title', null, ['class' => 'form-control box-size', 'placeholder' =>'Display Name', 'required' => 'required']) }}
        </div>
    </div>
    <div class="form-group">
        {{ Form::label('status', '', ['class' => 'col-lg-2 control-label']) }}

        <div class="col-lg-10">
            <div class="control-group">
                <label class="control control--checkbox">
                    {{ Form::checkbox('status', 1, true) }}

                    <div class="control__indicator"></div>
                </label>
            </div>
        </div>
        <!--col-lg-3-->
    </div>
    <h3 class="box-title">&nbsp;&nbsp;Field List</h3>
    <div class="input_fields_wrap">
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-3">
                <div class="form-group">
                    <label>Field Name</label>
                    <input type="text" class="form-control box-size" required name="fields[0][field_name]">
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group">
                    <label>Field Value</label>
                    <input type="text" class="form-control box-size" required name="fields[0][field_value]">
                </div>
            </div>
            <div class="col-lg-3">
                <label>&nbsp;</label><br>
                <a href="javscript:void(0)" class="btn btn-success add_field_button">Add More</a>
            </div>
        </div>
    </div>
</div>
<!--box-body-->


@push("after-scripts")
<script type="text/javascript">
    $(document).ready(function() {
        var max_fields = 10; //maximum input boxes allowed
        var wrapper = $(".input_fields_wrap"); //Fields wrapper
        var add_button = $(".add_field_button"); //Add button ID
        var x = 1; //initlal text box count
        $(add_button).click(function(e) { //on add input button click
            e.preventDefault();
            if (x < max_fields) { //max input box allowed
                //text box increment
                $(wrapper).append('<div class="row"><div class="col-lg-2"></div><div class="col-lg-3"><div class="form-group"><label>Field Name</label><input type="text" class="form-control box-size"  name="fields[' + x + '][field_name]"></div></div><div class="col-lg-3"><div class="form-group"><label>Field  Value</label><input type="text" class="form-control box-size"  name="fields[' + x + '][field_value]"></div></div><div class="col-lg-3"><label>&nbsp;</label><br><a href="#" class=" btn btn-danger remove_field">Remove</a></div></div>'); //add input box
            }
            x++;
        });

        $(wrapper).on("click", ".remove_field", function(e) { //user click on remove text
            e.preventDefault();
            $(this).parent('div').parent('div').remove();
            x--;
        })
    });
</script>
@endpush