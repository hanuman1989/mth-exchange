@extends ('backend.layouts.app')
@section ('title', trans('labels.backend.paygateways.management') . ' | ' . trans('labels.backend.paygateways.edit'))

@section('page-header')
<h1>
    {{ trans('labels.backend.paygateways.management') }}
    <small>{{ trans('labels.backend.paygateways.edit') }}</small>
</h1>
@endsection
@section('breadcrumb')
<div class="content-top-sec">
    <nav aria-label="breadcrumb">
        {{ Breadcrumbs::generate('common',['append' => [['label'=> $getController,'action' => $getAction,'route'=> 'admin.paygateways','id'=>isset($paygateways->id)?$paygateways->id:0]]]) }}
    </nav>
    <h1>User Manager</h1>
</div>
@endsection
@section('content')
{{ Form::model($paygateways, ['route' => ['admin.paygateways.update', $paygateways], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-paygateway']) }}

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('labels.backend.paygateways.edit') }}- {{$paygateways->title}}</h3>

        <div class="box-tools pull-right">
            @include('backend.paygateways.partials.paygateways-header-buttons')
        </div>
        <!--box-tools pull-right-->
    </div>
    <!--box-header with-border-->

    <div class="box-body">
        <div class="form-group">

            <div class="box-body">
                <div class="form-group">
                    {{ Form::label('Display Name', 'Display Name', ['class' => 'col-lg-3 control-label required']) }}
                    <div class="col-lg-9">
                        {{ Form::text('title', null, ['class' => 'form-control box-size', 'placeholder' =>'Display Name', 'required' => 'required']) }}
                    </div>
                </div>
                @php
                $fields=$paygateways->fields()->get();
                @endphp

                @if(isset($fields))
                @foreach($fields as $field)
                <div class="form-group">

                    {{ Form::label($field->field_name, '', ['class' => 'col-lg-3 control-label required']) }}

                    <div class="col-lg-9">

                        {{ Form::text($field->id, $field->field_value, ['class' => 'form-control box-size', 'placeholder' =>$field->field_name, 'required' => 'required']) }}
                    </div>
                </div>
                @endforeach
                <div class="form-group">
                    {{ Form::label('status', '', ['class' => 'col-lg-3 control-label']) }}

                    <div class="col-lg-9">
                        <div class="control-group">
                            <label class="control control--checkbox">
                                <input type="hidden" name="pay_gateways_id" value="{{$paygateways->id}}">
                                {{ Form::checkbox('status', 1, ($paygateways->status == 1) ? true : false ) }}

                                <div class="control__indicator"></div>
                            </label>
                        </div>
                    </div>
                    <!--col-lg-3-->
                </div>
                <div class="edit-form-btn">
                    {{ link_to_route('admin.paygateways.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-md']) }}
                    {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-primary btn-md']) }}
                    <div class="clearfix"></div>
                </div>
                <!--edit-form-btn-->
            </div>
            <!--form-group-->
        </div>
        <!--box-body-->
    </div>
    <!--box box-success -->
    @endif
    {{ Form::close() }}
    @endsection