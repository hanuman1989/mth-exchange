@extends ('backend.layouts.app')
@section ('title', trans('labels.backend.paygateways.management') . ' | ' . trans('labels.backend.paygateways.create'))
@section('page-header')
<h1>
    {{ trans('labels.backend.paygateways.management') }}
    <small>{{ trans('labels.backend.paygateways.create') }}</small>
</h1>
@endsection
@section('breadcrumb')
<div class="content-top-sec">
    <nav aria-label="breadcrumb">
        {{ Breadcrumbs::generate('common',['append' => [['label'=> $getController,'action' => $getAction,'route'=> 'admin.paygateways','id'=>isset($paygateways->id)?$paygateways->id:0]]]) }}
    </nav>
    <h1>User Manager</h1>
</div>
@endsection
@section('content')
{{ Form::open(['route' => 'admin.paygateways.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-paygateway']) }}

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('labels.backend.paygateways.create') }}</h3>

        <div class="box-tools pull-right">
            @include('backend.paygateways.partials.paygateways-header-buttons')
        </div>
        <!--box-tools pull-right-->
    </div>
    <!--box-header with-border-->
    {{-- Including Form blade file --}}
    @include("backend.paygateways.form")
    <div class="edit-form-btn">
        {{ link_to_route('admin.paygateways.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-md']) }}
        {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-primary btn-md']) }}
        <div class="clearfix"></div>
    </div>
    <!--edit-form-btn-->
</div>
<!--box box-success-->
{{ Form::close() }}
@endsection