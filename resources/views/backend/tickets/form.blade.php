<div class="box-body">
   <div class="form-group">
        {{ Form::label('Subject', trans('validation.attributes.backend.tickets.subject'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-5">
        
            {{ Form::select('ticket_reply_id', $allTicketsSubjects, null, ['class' => 'form-control tags box-size', 'required' => 'required']) }}
        
        </div><!--col-lg-10-->
    </div>
	
	 <div class="form-group">
        {{ Form::label('content', trans('validation.attributes.backend.tickets.content'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10 mce-box">
            {{ Form::textarea('content', null,['class' => 'form-control ', 'required' => 'required','placeholder' => trans('validation.attributes.backend.tickets.content')]) }}
        </div><!--col-lg-3-->
    </div>
	 <div class="form-group">
        {{ Form::label('prority', trans('validation.attributes.backend.tickets.prority'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-5">
        
            {{ Form::select('ticket_prority_id', $allProrities, null, ['class' => 'form-control tags box-size', 'required' => 'required']) }}
        
        </div><!--col-lg-10-->
    </div>
	 <div class="form-group">
        {{ Form::label('file', trans('validation.attributes.backend.tickets.file'), ['class' => 'col-lg-2 control-label']) }}
        @if(!empty($tickets->file))
            <div class="col-lg-1">
                <img src="{{ Storage::disk('public')->url('img/backend/tickets/' . $tickets->file) }}" height="80" width="80">
            </div>
            <div class="col-lg-5">
                <div class="custom-file-input">
                    <input type="file" name="file" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" />
                    <label for="file-1"><i class="fa fa-upload"></i><span>Choose a file</span></label>
                </div>
            </div>
        @else
            <div class="col-lg-5">
                <div class="custom-file-input">
                        <input type="file" name="file" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" />
                        <label for="file-1"><i class="fa fa-upload"></i><span>Attach a image</span></label>
                </div>
            </div>
        @endif
    </div><!--form control-->
	
</div><!--box-body-->

