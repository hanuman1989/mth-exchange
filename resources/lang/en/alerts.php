	<?php
	return [
		"backend" => [
			"products" => [
				"created" => "The Product was successfully created.",
				"deleted" => "The Product was successfully deleted.",
				"updated" => "The Product was successfully updated.",
			],
			"emailtemplates" => [
				"created" => "The Email Template was successfully created.",
				"deleted" => "The Email Template was successfully deleted.",
				"updated" => "The Email Template was successfully updated.",
			],
			"master-data" => [
				"states" => [
					"created" => "State has been successfully created.",
					"updated" => "State has been successfully updated.",
					"deleted" => "State has been successfully removed from system.",
				],
				"countries" => [
					"created" => "Country has been successfully created.",
					"updated" => "Country has been successfully updated.",
					"deleted" => "Country has been successfully removed from system.",
				],
			],
			"clientdetails" => [
				"created" => "Client has been successfully created.",
				"deleted" => "Client has been successfully deleted.",
				"updated" => "Client has been successfully updated.",
			],
			"securityquestions" => [
				"created" => "Security Question has been successfully created.",
				"deleted" => "Security Question has been successfully deleted.",
				"updated" => "Security Question has been successfully updated.",
			],
			"languages" => [
				"created" => "Language has been successfully created.",
				"deleted" => "Language has been successfully deleted.",
				"updated" => "Language has been successfully updated.",
			],
			"activities" => [
				"created" => "The ClientLog was successfully created.",
				"deleted" => "The ClientLog was successfully deleted.",
				"updated" => "The ClientLog was successfully updated.",
			],
			"taxrules" => [
				"created" => "Tax rule has been successfully created.",
				"deleted" => "Tax rule has been successfully deleted.",
				"updated" => "Tax rule has been successfully updated.",
			],
			"notes" => [
				"created" => "The Note was successfully created.",
				"deleted" => "The Note was successfully deleted.",
				"updated" => "The Note was successfully updated.",
			],
			"invoices" => [
				"created" => "The Invoice was successfully created.",
				"deleted" => "The Invoice was successfully deleted.",
				"updated" => "The Invoice was successfully updated.",
			],
		],
	];