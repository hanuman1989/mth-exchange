<?php

namespace App\Models\ProductGroup;

use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\ProductGroup\Traits\ProductGroupAttribute;
use App\Models\ProductGroup\Traits\ProductGroupRelationship;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
* Class Product Group
*
**/
class ProductGroup extends Model
{
    use ModelTrait,
	    SoftDeletes,
        ProductGroupAttribute,
    	ProductGroupRelationship {
        }

    
    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'product_groups';

    /**
     * Mass Assignable fields of model
     * @var array
     */
    protected $fillable = [

    ];

    /**
     * Default values for model fields
     * @var array
     */
    protected $attributes = [

    ];

    /**
     * Dates
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Guarded fields of model
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * Constructor of Model
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
	
	
}
