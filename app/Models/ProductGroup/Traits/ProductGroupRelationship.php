<?php

namespace App\Models\ProductGroup\Traits;
use App\Models\GroupFeature\GroupFeature;
/**
 * Class ProductGroupRelationship
 */
trait ProductGroupRelationship
{
     /**
     * The payment gateway .
     */
    public function paymentGateway()
    {
        return $this->belongsToMany('App\Models\PaymentGateway\PaymentGateway','payment_gateway_product_group')->withPivot('payment_gateway_id');
    }
	
	/**
     * The product group realtion.
     */
    public function groupFeature()
    {
       return $this->belongsTo(GroupFeature::class, 'product_group_id');
    }
}
