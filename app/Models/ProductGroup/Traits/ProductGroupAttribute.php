<?php

namespace App\Models\ProductGroup\Traits;

/**
 * Class ProductGroupAttribute.
 */
trait ProductGroupAttribute
    
{

    /**
     * Action Button Attribute to show in grid
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return '<div class="btn-group action-btn"> '.$this->getEditButtonAttribute("edit-productgroup", "admin.productgroups.edit").'
                '.$this->getDeleteButtonAttribute("delete-productgroup", "admin.productgroups.destroy").'
                </div>';
    }
}
