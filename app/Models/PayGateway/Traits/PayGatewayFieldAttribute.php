<?php

namespace App\Models\PayGateway\Traits;

/**
 * Class PayGatewayAttribute.
 */
trait PayGatewayFieldAttribute
{
    // Make your attributes functions here
    // Further, see the documentation : https://laravel.com/docs/6.x/eloquent-mutators#defining-an-accessor


    /**
     * Action Button Attribute to show in grid
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return '<div class="btn-group action-btn"> {$this->getEditButtonAttribute("edit-paygateway", "admin.paygateways.edit")}
                {$this->getDeleteButtonAttribute("delete-paygateway", "admin.paygateways.destroy")}
                </div>';
    }
}
