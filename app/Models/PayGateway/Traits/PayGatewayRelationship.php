<?php

namespace App\Models\PayGateway\Traits;
use App\Models\PayGateway\PayGatewayField;
/**
 * Class PayGatewayRelationship
 */
trait PayGatewayRelationship
{
    /**
     * Payment gateway has many Fields has many relationship with tags.
     */
    public function fields()
    {
        return $this->hasMany(PayGatewayField::class, 'pay_gateways_id');
    }

    
}
