<?php

namespace App\Models\PayGateway;

use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\PayGateway\Traits\PayGatewayFieldAttribute;
use Symfony\Component\Yaml\Yaml;
use DB;
use Illuminate\Support\Facades\Storage;
class PayGatewayField extends Model
{
    use ModelTrait,
        PayGatewayFieldAttribute
    	{
            // PayGatewayAttribute::getEditButtonAttribute insteadof ModelTrait;
        }

    /**
     * NOTE : If you want to implement Soft Deletes in this model,
     * then follow the steps here : https://laravel.com/docs/6.x/eloquent#soft-deleting
     */

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'pay_gateway_fields';

    /**
     * Mass Assignable fields of model
     * @var array
     */
    protected $fillable = ['pay_gateways_id','field_name','field_value','created_at','updated_at','deleted_at'

    ];

    /**
     * Default values for model fields
     * @var array
     */
    protected $attributes = [

    ];

    /**
     * Dates
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Guarded fields of model
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * Constructor of Model
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        static::updating(function($model)
        {   
            $model->yamlParse();
        });
        static::saved(function($model)
        {
            $model->yamlParse();
        });
        static::updated(function($model)
        {
            $model->yamlParse();
        });
        static::deleted(function($model)
        {
            $model->yamlParse();
        });
    }
    protected function yamlParse()
    {
        
        $settings = DB::table('pay_gateway_fields')->pluck('field_value','field_name')->toArray();
        $listYaml = Yaml::dump($settings, 60,4);
        Storage::disk('public')->put('settings.yml', $listYaml);
    }


}
