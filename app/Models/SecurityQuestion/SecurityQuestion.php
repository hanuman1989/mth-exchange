<?php

namespace App\Models\SecurityQuestion;

use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\SecurityQuestion\Traits\SecurityQuestionAttribute;
use App\Models\SecurityQuestion\Traits\SecurityQuestionRelationship;

class SecurityQuestion extends Model
{
    use ModelTrait,
        SecurityQuestionAttribute,
    	SecurityQuestionRelationship {
            // SecurityQuestionAttribute::getEditButtonAttribute insteadof ModelTrait;
        }

    /**
     * NOTE : If you want to implement Soft Deletes in this model,
     * then follow the steps here : https://laravel.com/docs/6.x/eloquent#soft-deleting
     */

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'security_questions';

    /**
     * Mass Assignable fields of model
     * @var array
     */
    protected $fillable = [

    ];

    /**
     * Dates
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Guarded fields of model
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * Constructor of Model
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * Scope to get all active countries
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
}
