<?php

namespace App\Models\TaxRules;

use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\TaxRules\Traits\TaxRuleAttribute;
use App\Models\TaxRules\Traits\TaxRuleRelationship;

class TaxRule extends Model
{
    use ModelTrait,
        TaxRuleAttribute,
    	TaxRuleRelationship {
            // TaxRuleAttribute::getEditButtonAttribute insteadof ModelTrait;
        }

    /**
     * NOTE : If you want to implement Soft Deletes in this model,
     * then follow the steps here : https://laravel.com/docs/6.x/eloquent#soft-deleting
     */

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'tax_rules';

    /**
     * Mass Assignable fields of model
     * @var array
     */
    protected $fillable = [
        'tax_name','tax_rate','country_id', 'status'
    ];

    /**
     * Dates
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Guarded fields of model
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * Constructor of Model
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
