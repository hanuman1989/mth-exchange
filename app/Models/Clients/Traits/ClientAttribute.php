<?php

namespace App\Models\Clients\Traits;

/**
 * Class ClientAttribute.
 */
trait ClientAttribute
{
    // Make your attributes functions here
    // Further, see the documentation : https://laravel.com/docs/6.x/eloquent-mutators#defining-an-accessor


    /**
     * Action Button Attribute to show in grid
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return '<div class="btn-group action-btn"> {$this->getEditButtonAttribute("edit-clientdetail", "admin.clientdetails.edit")}
                {$this->getDeleteButtonAttribute("delete-clientdetail", "admin.clientdetails.destroy")}
                </div>';
    }
}
