<?php

namespace App\Models\Clients;

use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Clients\Traits\ClientAttribute;
use App\Models\Clients\Traits\ClientRelationship;

/**
 * Class Client for manage client details.
 */
class Client extends Model
{
    use ModelTrait,
        ClientAttribute;

    /**
     * NOTE : If you want to implement Soft Deletes in this model,
     * then follow the steps here : https://laravel.com/docs/6.x/eloquent#soft-deleting
     */

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'clients';

    /**
     * Mass Assignable fields of model
     * @var array
     */
    protected $fillable = [
        'user_id', 'company_name', 'security_question', 'security_answer', 'tax_id', 'address_1', 'address_2', 'city', 'state_id', 'postcode', 'country_id', 'phone', 'payment_method', 'billing_contact', 'language_id', 'currency', 'client_group', 'credit_balance', 'admin_notes', 'late_fees', 'overdue_notices', 'tax_exempt', 'seperate_invoice', 'disable_cc_processing', 'marketing_email_opt_in', 'status_update', 'allow_single_sign_on', 'send_account_info_message'
    ];

    /**
     * Dates
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Guarded fields of model
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * Constructor of Model
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
