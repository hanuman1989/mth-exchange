<?php

namespace App\Models\PaymentGateway\Traits;

/**
 * Class PaymentGatewayAttribute.
 */
trait PaymentGatewayAttribute
{
    // Make your attributes functions here
    // Further, see the documentation : https://laravel.com/docs/6.x/eloquent-mutators#defining-an-accessor


    /**
     * Action Button Attribute to show in grid
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return '<div class="btn-group action-btn"> '.$this->getEditButtonAttribute("edit-paymentgateway", "admin.paymentgateways.edit").'
                '.$this->getDeleteButtonAttribute("delete-paymentgateway", "admin.paymentgateways.destroy").'
                </div>';
    }
}
