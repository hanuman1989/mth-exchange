<?php

namespace App\Models\PaymentGateway;

use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\PaymentGateway\Traits\PaymentGatewayAttribute;

/**
*Class PaymentGateway
**/
class PaymentGateway extends Model
{
    use ModelTrait,
        PaymentGatewayAttribute{
           
        }

   
    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'payment_gateways';

    /**
     * Mass Assignable fields of model
     * @var array
     */
    protected $fillable = [

    ];

    /**
     * Default values for model fields
     * @var array
     */
    protected $attributes = [

    ];

    /**
     * Dates
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Guarded fields of model
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * Constructor of Model
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
	
	
	
	 /*
    * Get All Active payment gateways available in system 
    */
   public static function scopeActive($query) {
        return $query->where('status', 1);
    }
}
