<?php

namespace App\Models\Activity\Traits;

/**
 * Class ActivityAttribute for manage activity logs.
 */
trait ActivityAttribute
{
    // Make your attributes functions here
    // Further, see the documentation : https://laravel.com/docs/6.x/eloquent-mutators#defining-an-accessor


    /**
     * Action Button Attribute to show in grid
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return '<div class="btn-group action-btn">
        
        '.$this->getDeleteButtonAttribute("delete-activity", "admin.activities.destroy").'
        </div>';
                
    }
}
