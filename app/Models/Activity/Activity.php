<?php

namespace App\Models\Activity;

use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Activity\Traits\ActivityAttribute;
use App\Models\Activity\Traits\ActivityRelationship;

/**
 * Class Activity for manage activity logs.
 */
class Activity extends Model
{
    use ModelTrait,
        ActivityAttribute,
    	ActivityRelationship {
            // ActivityAttribute::getEditButtonAttribute insteadof ModelTrait;
        }

    /**
     * NOTE : If you want to implement Soft Deletes in this model,
     * then follow the steps here : https://laravel.com/docs/6.x/eloquent#soft-deleting
     */

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'activity_log';

    /**
     * Mass Assignable fields of model
     * @var array
     */
    protected $fillable = [
        'log_name', 'description', 'subject_id', 'subject_type', 'causer_id', 'causer_type', 'properties'
    ];

    /**
     * Default values for model fields
     * @var array
     */
    protected $attributes = [

    ];

    /**
     * Dates
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Guarded fields of model
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * Constructor of Model
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
