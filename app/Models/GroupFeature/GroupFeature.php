<?php

namespace App\Models\GroupFeature;

use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/*
*Class Group Feature 
*
*/
class GroupFeature extends Model
{
    use ModelTrait,
	    SoftDeletes {
        }

    
    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'group_features';

    /**
     * Mass Assignable fields of model
     * @var array
     */
    protected $fillable = [

    ];

    /**
     * Default values for model fields
     * @var array
     */
    protected $attributes = [

    ];

    /**
     * Dates
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Guarded fields of model
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * Constructor of Model
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
	
	/**
	* Get all group features values 
	*@var productGroupId
	*/
	
	public function scopesavedFeature($query, $product_group_id)
    {
        return $query->where('product_group_id', $product_group_id);
    }
	
	
}
