<?php

namespace App\Models\PaymentType;

use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\PaymentType\Traits\PaymentTypeAttribute;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
* Class PaymentType
*
**/
class PaymentType extends Model
{
    use ModelTrait,
	  SoftDeletes,
        PaymentTypeAttribute {
           
        }

   

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'payment_types';

    /**
     * Mass Assignable fields of model
     * @var array
     */
    protected $fillable = [

    ];

    /**
     * Default values for model fields
     * @var array
     */
    protected $attributes = [

    ];

    /**
     * Dates
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Guarded fields of model
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * Constructor of Model
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
