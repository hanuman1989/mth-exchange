<?php

namespace App\Models\TicketPriorty;

use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\TicketPriorty\Traits\TicketPriortyAttribute;;

/**
*Ticket Priorty
*/
class TicketPriorty extends Model
{
    use ModelTrait,
        TicketPriortyAttribute{
            
        }

   

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'ticket_priorities';

    /**
     * Mass Assignable fields of model
     * @var array
     */
    protected $fillable = ['name','status'

    ];

    /**
     * Default values for model fields
     * @var array
     */
    protected $attributes = [

    ];

    /**
     * Dates
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Guarded fields of model
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * Constructor of Model
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
	
	 /*
    * Get All Active orderform templates available in system 
    */
    public static function getAllProrities() {
        return TicketPriorty::where('status', 1)->pluck('title','id');
    }
}
