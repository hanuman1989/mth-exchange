<?php

namespace App\Models\TicketReply;

use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\TicketReply\Traits\TicketReplyAttribute;
use App\Models\TicketReply\Traits\TicketReplyRelationship;

/**
* Class Ticket Reply
**/
class TicketReply extends Model
{
    use ModelTrait,
        TicketReplyAttribute
    	{
            
        }

  

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'ticket_replies';

    /**
     * Mass Assignable fields of model
     * @var array
     */
    protected $fillable = [
   
    ];

    /**
     * Default values for model fields
     * @var array
     */
    protected $attributes = [

    ];

    /**
     * Dates
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Guarded fields of model
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * Constructor of Model
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
	
	/*
    * Get All Active subject from TicketReply
    */
    public static function getAllSubjects() {
       return TicketReply::where('status', 1)->pluck('subject','id');
    }
}
