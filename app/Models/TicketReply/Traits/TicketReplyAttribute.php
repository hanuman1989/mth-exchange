<?php

namespace App\Models\TicketReply\Traits;

/**
 * Class TicketReplyAttribute.
 */
trait TicketReplyAttribute
{
    
    /**
     * Action Button Attribute to show in grid
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return '<div class="btn-group action-btn"> '.$this->getEditButtonAttribute("edit-ticketreply", "admin.ticketreplies.edit").'
                '.$this->getDeleteButtonAttribute("delete-ticketreply", "admin.ticketreplies.destroy").'
                </div>';
    }
	
	/**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        if ($this->isActive()) {
            return "<label class='label label-success'>".trans('labels.general.active').'</label>';
        }

        return "<label class='label label-danger'>".trans('labels.general.inactive').'</label>';
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}
