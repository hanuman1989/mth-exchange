<?php

namespace App\Models\Language;

use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Language\Traits\LanguageAttribute;

class Language extends Model
{
    use ModelTrait,
        LanguageAttribute;

    /**
     * NOTE : If you want to implement Soft Deletes in this model,
     * then follow the steps here : https://laravel.com/docs/6.x/eloquent#soft-deleting
     */

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'languages';

    /**
     * Mass Assignable fields of model
     * @var array
     */
    protected $fillable = [
        'name', 'iso_639-1'
    ];

    /**
     * Dates
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Guarded fields of model
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * Constructor of Model
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * Scope to get all active countries
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
}
