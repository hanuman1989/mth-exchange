<?php

namespace App\Models\OrderFormTemplate\Traits;

/**
 * Class OrderFormTemplateAttribute.
 */
trait OrderFormTemplateAttribute
{
   
    /**
     * Action Button Attribute to show in grid
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return '<div class="btn-group action-btn"> '.$this->getEditButtonAttribute("edit-orderformtemplate", "admin.orderformtemplates.edit").'
                '.$this->getDeleteButtonAttribute("delete-orderformtemplate", "admin.orderformtemplates.destroy").'
                </div>';
    }
}
