<?php

namespace App\Models\OrderFormTemplate;

use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\OrderFormTemplate\Traits\OrderFormTemplateAttribute;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
* Class Order Form Template   
* 
**/

class OrderFormTemplate extends Model
{
    use ModelTrait,
	 SoftDeletes,
        OrderFormTemplateAttribute {
        
        }

    
    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'order_form_templates';

    /**
     * Mass Assignable fields of model
     * @var array
     */
    protected $fillable = [

    ];

    /**
     * Default values for model fields
     * @var array
     */
    protected $attributes = [

    ];

    /**
     * Dates
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Guarded fields of model
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * Constructor of Model
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
	
	/*
    * Get All Active orderform templates available in system 
    */
    public static function scopeActive($query) {
        return $query->where('status', 1);
    }
	
}
