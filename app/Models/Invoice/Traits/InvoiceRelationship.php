<?php

namespace App\Models\Invoice\Traits;

use App\Models\XeroSync\XeroSync;
use App\Domains\Auth\Models\User;

/**
 * Class InvoiceRelationship
 */
trait InvoiceRelationship
{
    /*
    * put you model relationships here
    * Take below example for reference
    */
    
    /**
     * Function to maintain xero invoices for users
     */
    public function xero_invoice() {
        return $this->hasMany(XeroSync::class, 'xerosyncable_id', 'id');
    }

    /**
     * Function to get client related to invoice
     */
    public function customers()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
