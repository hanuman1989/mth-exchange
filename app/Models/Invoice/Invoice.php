<?php

namespace App\Models\Invoice;

use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Invoice\Traits\InvoiceAttribute;
use App\Models\Invoice\Traits\InvoiceRelationship;

class Invoice extends Model
{
    use ModelTrait,
        InvoiceAttribute,
    	InvoiceRelationship {
            // InvoiceAttribute::getEditButtonAttribute insteadof ModelTrait;
        }

    /**
     * NOTE : If you want to implement Soft Deletes in this model,
     * then follow the steps here : https://laravel.com/docs/6.x/eloquent#soft-deleting
     */

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'invoices';

    /**
     * Mass Assignable fields of model
     * @var array
     */
    protected $fillable = [
        'invoice_number', 'user_id', 'line_item_description', 'payment_method', 'status', 'invoice_date', 'due_date', 'date_paid', 'due_from', 'due_to', 'invoice_file'
    ];

    /**
     * Dates
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Guarded fields of model
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * Constructor of Model
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
