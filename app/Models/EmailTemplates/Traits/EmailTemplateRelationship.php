<?php

namespace App\Models\EmailTemplates\Traits;

use App\Domains\Auth\Models\User;
/**
 * trait EmailTemplateRelationship it belongs to user created by.
 */
trait EmailTemplateRelationship
{
    public function owner()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
}
