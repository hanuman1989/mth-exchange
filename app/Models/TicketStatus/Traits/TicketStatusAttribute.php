<?php

namespace App\Models\TicketStatus\Traits;

/**
 * Class TicketStatusAttribute.
 */
trait TicketStatusAttribute
{



    /**
     * Action Button Attribute to show in grid
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return '<div class="btn-group action-btn"> '.$this->getEditButtonAttribute("edit-ticketstatus", "admin.ticketstatuses.edit").'
                '.$this->getDeleteButtonAttribute("delete-ticketstatus", "admin.ticketstatuses.destroy").'
                </div>';
    }
	
	/**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        if ($this->isActive()) {
            return "<label class='label label-success'>".trans('labels.general.active').'</label>';
        }

        return "<label class='label label-danger'>".trans('labels.general.inactive').'</label>';
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}
