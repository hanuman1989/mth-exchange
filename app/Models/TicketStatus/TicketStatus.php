<?php

namespace App\Models\TicketStatus;

use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\TicketStatus\Traits\TicketStatusAttribute;

/**
* Class TicketStatus
**/
class TicketStatus extends Model
{
    use ModelTrait,
        TicketStatusAttribute{
            
        }

   
    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'ticket_statuses';

    /**
     * Mass Assignable fields of model
     * @var array
     */
    protected $fillable = [

    ];

    /**
     * Default values for model fields
     * @var array
     */
    protected $attributes = [

    ];

    /**
     * Dates
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Guarded fields of model
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * Constructor of Model
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
