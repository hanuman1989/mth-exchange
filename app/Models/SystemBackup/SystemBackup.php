<?php

namespace App\Models\SystemBackup;

use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * Model to manage system and db backup both
 */
class SystemBackup extends Model
{

    /**
     * NOTE : If you want to implement Soft Deletes in this model,
     * then follow the steps here : https://laravel.com/docs/6.x/eloquent#soft-deleting
     */

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'backup_logs';

    /**
     * Mass Assignable fields of model
     * @var array
     */
    protected $fillable = [
        'file','file_path'
    ];

    /**
     * Dates
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Guarded fields of model
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * Constructor of Model
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
