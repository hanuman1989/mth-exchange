<?php

namespace App\Models\Ticket;

use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Ticket\Traits\TicketAttribute;
use App\Models\Ticket\Traits\TicketRelationship;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
* Class Ticket
**/
class Ticket extends Model
{
    use ModelTrait,
	    SoftDeletes,
        TicketAttribute,
    	TicketRelationship {
          
        }

    

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'tickets';

    /**
     * Mass Assignable fields of model
     * @var array
     */
    protected $fillable = [

    ];

    /**
     * Default values for model fields
     * @var array
     */
    protected $attributes = [

    ];

    /**
     * Dates
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Guarded fields of model
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * Constructor of Model
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
	
	 
}
