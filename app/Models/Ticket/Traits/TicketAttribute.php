<?php

namespace App\Models\Ticket\Traits;

/**
 * Class TicketAttribute.
 */
trait TicketAttribute
{
   

    /**
     * Action Button Attribute to show in grid
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return '<div class="btn-group action-btn"> '.$this->getEditButtonAttribute("edit-ticket", "admin.tickets.edit").'
                '.$this->getDeleteButtonAttribute("delete-ticket", "admin.tickets.destroy").'
                </div>';
    }
}
