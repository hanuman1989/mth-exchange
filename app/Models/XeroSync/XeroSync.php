<?php

namespace App\Models\XeroSync;

use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\Model;

class XeroSync extends Model
{
    use ModelTrait;

    /**
     * NOTE : If you want to implement Soft Deletes in this model,
     * then follow the steps here : https://laravel.com/docs/6.x/eloquent#soft-deleting
     */

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'xero_syncs';

    /**
     * Mass Assignable fields of model
     * @var array
     */
    protected $fillable = [
        'xero_action','xero_status','xero_response_id', 'xero_response','xero_error', 'retries','xerosyncable_id'
    ];

    /**
     * Default values for model fields
     * @var array
     */
    protected $attributes = [

    ];

    /**
     * Dates
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Guarded fields of model
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * Constructor of Model
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
