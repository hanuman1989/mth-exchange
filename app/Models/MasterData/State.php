<?php

namespace App\Models\MasterData;

use App\Models\BaseModel;
use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\MasterData\Traits\Attribute\StateAttribute;

class State extends Model
{
    use ModelTrait,
    StateAttribute;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The guarded field which are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

     /**
     * Default values for model fields
     * @var array
     */
    protected $attributes = [
        
    ];

    /**
     * Constructor of Model
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * Scope to get all active states
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
}
