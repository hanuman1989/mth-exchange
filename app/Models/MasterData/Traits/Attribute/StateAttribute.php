<?php

namespace App\Models\MasterData\Traits\Attribute;

/**
 * Class StateAttribute.
 */
trait StateAttribute
{
    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return '<div class="btn-group action-btn">
                    '.$this->getEditButtonAttribute('manage-master-data', 'admin.master-data.states.edit').'
                    '.$this->getDeleteButtonAttribute('manage-master-data', 'admin.master-data.states.delete').'
                </div>';
    }


    /**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        if ($this->isActive()) {
            return "<label class='label label-success'>".trans('labels.general.active').'</label>';
        }

        return "<label class='label label-danger'>".trans('labels.general.inactive').'</label>';
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}
