<?php

namespace App\Models\Notes;

use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Notes\Traits\NoteAttribute;
use App\Models\Notes\Traits\NoteRelationship;

class Note extends Model
{
    use ModelTrait,
        NoteAttribute,
    	NoteRelationship {
            // NoteAttribute::getEditButtonAttribute insteadof ModelTrait;
        }

    /**
     * NOTE : If you want to implement Soft Deletes in this model,
     * then follow the steps here : https://laravel.com/docs/6.x/eloquent#soft-deleting
     */

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'notes';

    /**
     * Mass Assignable fields of model
     * @var array
     */
    protected $fillable = [
        'notes','added_by','user_id'
    ];

    /**
     * Dates
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Guarded fields of model
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * Constructor of Model
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
