<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

/**
 * DeleteXeroContacts for manage functions to delete xero contacts
 */
class DeleteXeroContacts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crons:xero-delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This cron will be run in every 5 min and delete contacts from xero system';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $logs = \App\Model\XeroSync::where('xero_status', 3)->get();
        foreach($logs as $log){
            $obj = (new \App\Xero\Xero);
            $result = $obj->archiveXeroContact($log->xero_response_id);
            $log->delete();
        }
    }
}
