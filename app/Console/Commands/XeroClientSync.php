<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Xero\Xero;
use App\Domains\Auth\Models\User;
use App\Models\XeroSync\XeroSync;

/**
 * XeroClientSync for manage functions to sync clients on xero
 */
class XeroClientSync extends Command
{
    /**
    * The name and signature of the console command.
    *
    * @var string
    */
    protected $signature = 'crons:xero-client';
    
    /**
    * The console command description.
    *
    * @var string
    */
    protected $description = 'This cron will be run every 5 min and add client into xero system.';
    
    /**
    * Create a new command instance.
    *
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
    * Execute the console command.
    *
    * @return mixed
    */
    public function handle()
    {
        $clients = User::onlyActive()->with(['xero_contact'])
            ->where('type', 'user')
            ->whereDoesntHave('xero_contact', function($q){
                $q->where('xero_action', 1)->where('xero_status', 1);
            })->get();
            $obj = (new Xero);
            foreach($clients as $client){
                if($client->xero_contact && isset($client->xero_contact[0]->xero_response_id)){
                    $result = $obj->updateXeroContact($client);
                }else{
                    $result = $obj->createXeroContacts($client);
                }
                $ret = isset($client->xero_contact->retries) ? $client->xero_contact->retries : 0;
                $xeroSync = [];
                $xeroSync['xero_action'] = 1;

                if($result['status'] == true){
                    $xeroSync['xero_status'] = 1;
                    if(!isset($result['is_validation'])){
                        $ret = 0;
                        $xeroSync['xero_response_id'] = $result['data']->getContactId();
                    }
                    $xeroSync['xero_response'] = $result['response'][0];
                    $xeroSync['xero_error'] = NULL;
                }else{
                    $xeroSync['xero_status'] = 0;
                    $xeroSync['xero_response'] = $result['response'][0];
                    $xeroSync['xero_error'] = $result['message'] ?? '';
                }
                $xeroSync['retries'] = $ret+1;
                if($client->xero_contact && isset($client->xero_contact[0]->xero_response_id)) {
                    $client->xero_contact()->update($xeroSync);
                } else {
                    $client->xero_contact()->create($xeroSync);
                }
            }
        }
    }
