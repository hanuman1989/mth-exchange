<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Xero\Xero;
use App\Models\Invoice\Invoice;
use App\Models\XeroInvoice\XeroInvoice;

/**
 * XeroInvoiceSync for manage functions to sync invoices on xero
*/

class XeroInvoiceSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crons:xero-invoice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This cron will be run every day and create invoices on xero.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $invoiceables = Invoice::whereNotNull('invoice_number')
        ->whereDoesntHave('xero_invoice', function($q){
            $q->where('xero_action', 2)->where('xero_status', 1);
        })
        ->with(['customers','xero_invoice'])
        ->get();
        $obj = (new Xero);
        foreach($invoiceables as $invoice){
            $results = $obj->createInvoices($invoice);
            if($results['status'] == true){
                foreach($results['response'] as $res){
                    $ret = isset($invoice->xero_invoice->retries) ? $invoice->xero_invoice->retries : 0;
                    $xeroInvoice = [];
                    $xeroInvoice['xero_action'] = 2;
                    $inv_pdf = false;
                    if($res['status'] == true){
                        $xeroInvoice['xero_status'] = 1;
                        $ret = 0;
                        $xeroInvoice['xero_response_id'] = $res['invoice_id'];
                        $xeroInvoice['xero_response'] = json_encode($results);
                        $inv_pdf = true;
                    }else{
                        $xeroInvoice['xero_status'] = 0;
                        $xeroInvoice['xero_response'] = $res['object'];
                        $xeroInvoice['xero_error'] = $res['message'] ?? '';
                    }
                    $xeroInvoice['retries'] = $ret+1;
                    $invoice->xero_invoice()->create($xeroInvoice);
                    if($inv_pdf){
                        $invData['invoice_number'] = $invoice->invoice_number;
                        $invData['response_id'] = $res['invoice_id'];
                        $res = $obj->getXeroInvoicePDF($invData);
                        if($res['status'] == true){
                            $invoice->invoice_file = $res['filename'];
                            $invoice->save();
                        }
                    }
                }
            }else{

            }
        }
    }
}
