<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Models\Setting\Setting;

/**
 * XeroRefreshToken for manage function to refresh xero token
*/
class XeroRefreshToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crons:refresh-token';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This cron will be run every 15 min. and get latest token for xero api which is insered in settings table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $provider = new \League\OAuth2\Client\Provider\GenericProvider([
                'clientId'                => config("settings.XERO_CLIENT_ID"),
                'clientSecret'            => config("settings.XERO_CLIENT_SECRET"),
                'urlAuthorize'            => 'https://login.xero.com/identity/connect/authorize',
                'urlAccessToken'          => 'https://identity.xero.com/connect/token',
                'urlResourceOwnerDetails' => 'https://api.xero.com/api.xro/2.0/Organisation'
            ]);
            $refresh_token = Setting::where('slug','XERO_REFRESH_TOKEN')->first();
            $newAccessToken = $provider->getAccessToken('refresh_token', [
                'grant_type' => 'refresh_token',
                'refresh_token' => $refresh_token->config_value
            ]);
            $refreshToken = $newAccessToken->getRefreshToken();
            $setting = Setting::updateOrCreate(
                ['slug' => 'XERO_REFRESH_TOKEN'],
                ['config_value' => $refreshToken]
            );
        } catch (\Exception $e) {
            Log::channel('general-crons')->info("XERO_REFRESH_TOKEN: ". $e->getMessage()."\n");
        }
    }
}
