<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Console\Commands\XeroRefreshToken;
use App\Console\Commands\DeleteXeroContacts;
use App\Console\Commands\XeroClientSync;
use App\Console\Commands\XeroInvoiceSync;

/**
 * Class Kernel.
 */
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        XeroRefreshToken::class,
        XeroClientSync::class,
        DeleteXeroContacts::class,
        XeroInvoiceSync::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //Schedule commands
        $schedule->command('backup:run')->daily();
        $schedule->command('crons:refresh-token')->everyTenMinutes();
        $schedule->command('crons:xero-client')->everyMinute();
        $schedule->command('crons:xero-delete')->everyMinute();
        $schedule->command('crons:xero-invoice')->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
