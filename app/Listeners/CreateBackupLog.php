<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Spatie\Backup\Events\BackupZipWasCreated;
use App\Models\SystemBackup\SystemBackup;

class CreateBackupLog
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BackupWasSuccessful  $event
     * @return void
     */
    public function handle(BackupZipWasCreated $event)
    {
        $this->createLog($event->pathToZip);
    }

    /**
     * Function to create logs of backup
     */
    public function createLog($path)
    {
        try {
            //Create backup log
            $log = [
                'file_path' => $path,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ];
            SystemBackup::create($log);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }
}
