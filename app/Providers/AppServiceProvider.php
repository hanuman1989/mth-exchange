<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class AppServiceProvider.
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Illuminate\Support\Facades\View::composer('*', function ($view) {
            if(app('request')->route()){
                $action = app('request')->route() ? app('request')->route()->getAction() : '';
                $controller = class_basename($action['controller']);
                list($controller, $action) = explode('@', $controller);
                $view->with(['getController' => str_replace('Controller', '', $controller),'getAction' => $action]);
            }
        });
    }
}
