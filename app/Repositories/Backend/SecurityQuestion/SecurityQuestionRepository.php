<?php

namespace App\Repositories\Backend\SecurityQuestion;

use DB;
use Carbon\Carbon;
use App\Models\SecurityQuestion\SecurityQuestion;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SecurityQuestionRepository.
 */
class SecurityQuestionRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = SecurityQuestion::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.securityquestions.table').'.id',
                config('module.securityquestions.table').'.question',
                config('module.securityquestions.table').'.status',
                config('module.securityquestions.table').'.created_at',
                config('module.securityquestions.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        $input['status'] = isset($input['status']) ? 1 : 0;
        if (SecurityQuestion::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.securityquestions.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param SecurityQuestion $securityquestion
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(SecurityQuestion $securityquestion, array $input)
    {
        $input['status'] = isset($input['status']) ? 1 : 0;
    	if ($securityquestion->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.securityquestions.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param SecurityQuestion $securityquestion
     * @throws GeneralException
     * @return bool
     */
    public function delete(SecurityQuestion $securityquestion)
    {
        if ($securityquestion->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.securityquestions.delete_error'));
    }
}
