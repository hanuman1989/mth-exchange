<?php

namespace App\Repositories\Backend\PaymentType;

use DB;
use Carbon\Carbon;
use App\Models\PaymentType\PaymentType;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PaymentTypeRepository.
 */
class PaymentTypeRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = PaymentType::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.paymenttypes.table').'.id',
				config('module.paymenttypes.table').'.name',
				 config('module.paymenttypes.table').'.status',
                config('module.paymenttypes.table').'.created_at',
                config('module.paymenttypes.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        if (PaymentType::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.paymenttypes.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param PaymentType $paymenttype
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(PaymentType $paymenttype, array $input)
    {
    	if ($paymenttype->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.paymenttypes.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param PaymentType $paymenttype
     * @throws GeneralException
     * @return bool
     */
    public function delete(PaymentType $paymenttype)
    {
        if ($paymenttype->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.paymenttypes.delete_error'));
    }
}
