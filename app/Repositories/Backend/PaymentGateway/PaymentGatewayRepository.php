<?php

namespace App\Repositories\Backend\PaymentGateway;

use DB;
use Carbon\Carbon;
use App\Models\PaymentGateway\PaymentGateway;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PaymentGatewayRepository.
 */
class PaymentGatewayRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = PaymentGateway::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.paymentgateways.table').'.id',
                config('module.paymentgateways.table').'.created_at',
                config('module.paymentgateways.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        if (PaymentGateway::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.paymentgateways.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param PaymentGateway $paymentgateway
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(PaymentGateway $paymentgateway, array $input)
    {
    	if ($paymentgateway->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.paymentgateways.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param PaymentGateway $paymentgateway
     * @throws GeneralException
     * @return bool
     */
    public function delete(PaymentGateway $paymentgateway)
    {
        if ($paymentgateway->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.paymentgateways.delete_error'));
    }
}
