<?php

namespace App\Repositories\Backend\ClientLogs;

use DB;
use Carbon\Carbon;
use App\Models\Activity\Activity;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use App\Domains\Auth\Models\User;

/**
 * Class ActivityRepository.
 */
class ActivityRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Activity::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->leftjoin(config('access.users_table'), config('access.users_table').'.id', '=', config('module.activities.table').'.causer_id')
            ->select([
                config('module.activities.table').'.id',
                config('module.activities.table').'.log_name',
                config('module.activities.table').'.description',
                config('module.activities.table').'.subject_type',
                config('module.activities.table').'.causer_id',
                config('module.activities.table').'.properties',
                config('module.activities.table').'.created_at',
                config('module.activities.table').'.updated_at',
                config('access.users_table').'.first_name as user_name',
            ])->where('type','=',User::TYPE_USER);
    }

}
