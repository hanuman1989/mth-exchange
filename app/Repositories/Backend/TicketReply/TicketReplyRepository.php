<?php

namespace App\Repositories\Backend\TicketReply;

use DB;
use Carbon\Carbon;
use App\Models\TicketReply\TicketReply;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TicketReplyRepository.
 */
class TicketReplyRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = TicketReply::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.ticketreplies.table').'.id',
				config('module.ticketreplies.table').'.subject',
				config('module.ticketreplies.table').'.status',
                config('module.ticketreplies.table').'.created_at',
                config('module.ticketreplies.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
		$input['status'] = isset($input['status']) ? 1 : 0;
        if (TicketReply::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.ticketreplies.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param TicketReply $ticketreply
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(TicketReply $ticketreply, array $input)
    {
		$input['status'] = isset($input['status']) ? 1 : 0;
    	if ($ticketreply->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.ticketreplies.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param TicketReply $ticketreply
     * @throws GeneralException
     * @return bool
     */
    public function delete(TicketReply $ticketreply)
    {
        if ($ticketreply->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.ticketreplies.delete_error'));
    }
}
