<?php

namespace App\Repositories\Backend\Clients;

use DB;
use Carbon\Carbon;
use App\Models\Clients\Client;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use App\Domains\Auth\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Events\Backend\Access\User\UserCreated;
use App\Domains\Auth\Services\PermissionService;
use App\Domains\Auth\Services\RoleService;

/**
 * Class ClientRepository for manage client details.
 */
class ClientRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = User::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable($searchParams)
    {
        $query = $this->query()
            ->leftJoin('clients as client', function($join){
                $join->on('users.id', '=', 'client.user_id');
            })
            ->select([ config('access.users_table').'.id','client.company_name',config('access.users_table').'.first_name',
                config('access.users_table').'.last_name', config('access.users_table').'.email',config('access.users_table').'.active',
                config('access.users_table').'.created_at',config('access.users_table').'.updated_at','client.address_1',
                'client.address_2','client.city','client.state_id','client.postcode',
                'client.country_id','client.phone','client.payment_method','client.language_id',
                'client.currency','client.client_group','client.credit_balance','client.late_fees',
                'client.overdue_notices','client.tax_exempt','client.seperate_invoice',
                'client.marketing_email_opt_in','client.status_update'
            ])->where('type', 'user'); // will filter only clients
        
            $query = $query->company($searchParams['company_name'])->email($searchParams['email'])->phone($searchParams['phone'])
                    ->status($searchParams['status'])->address1($searchParams['address_1'])->address2($searchParams['address_2'])
                    ->city($searchParams['city'])->state($searchParams['state'])->postcode($searchParams['postcode'])
                    ->country($searchParams['country'])->paymentMethod($searchParams['payment_method'])
                    ->autoCreditCardBilling($searchParams['auto_credit_card_billing'])->creditBalance($searchParams['credit_balance'])
                    ->currency($searchParams['currency'])->language($searchParams['language'])->marketingEmailOptIn($searchParams['marketing_emails_opt_in'])
                    ->autoStatusUpdate($searchParams['auto_status_update'])->taxExemptStatus($searchParams['tax_exempt_status'])->lateFees($searchParams['late_fees'])
                    ->overdueNotices($searchParams['overdue_notices'])->seperateInvoice($searchParams['seperate_invoices']);
        
        return $query;

    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create($request)
    {
        $data = $request->except('assignees_roles', 'permissions');
        $roles = $request->get('assignees_roles');
        $permissions = $request->get('permissions');
        $user = $this->createUserStub($data);

        $this->checkUserByEmail($data, $user);

        DB::transaction(function () use ($user, $data, $roles, $permissions) {
            if ($user->save()) {

                //User Created, Validate Roles
                if (!count($roles)) {
                    throw new GeneralException(trans('exceptions.backend.access.users.role_needed_create'));
                }

                $user->syncRoles($data['roles'] ?? []);

                if (! config('boilerplate.access.user.only_roles')) {
                    $user->syncPermissions($data['permissions'] ?? []);
                }

                //create user detail stub
                $details = $this->createUserDetailsStub($data, $user);
                //attach all the details to this user
                if ($details->save()) {
    
                    return true;
                }
            }
            throw new GeneralException(trans('exceptions.backend.clientdetails.create_error'));
        });
    }

    /**
     * For updating the respective Model in storage
     *
     * @param User $user
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update($user, $request)
    {
        $data = $request->except('assignees_roles', 'permissions');
        $data['password'] = ($data['password'] != null) ? Hash::make($data['password']) : $user->password;
        $data['active'] = isset($data['status']) ?? 0;
    	DB::transaction(function () use ($user, $data) {
            if ($user->update($data)) {
                //attach all the details to this user
                if ($user->detail->update([
                    'company_name' => $data['company_name'],
                    'security_question' => $data['security_question'],
                    'security_answer' => $data['security_answer'],
                    'tax_id' => $data['tax_id'],
                    'address_1' => $data['address_1'],
                    'address_2' => $data['address_2'],
                    'city' => $data['city'],
                    'state_id' => $data['state_id'],
                    'postcode' => $data['postcode'],
                    'country_id' => $data['country_id'],
                    'phone' => $data['phone'],
                    'payment_method' => $data['payment_method'],
                    'billing_contact' => $data['billing_contact'],
                    'language_id' => $data['language_id'],
                    'currency' => $data['currency'],
                    'client_group' => $data['client_group'],
                    'credit_balance' => $data['credit_balance'],
                    'admin_notes' => $data['admin_notes'],
                    'late_fees' => isset($data['late_fees']) ?? 0,
                    'overdue_notices' => isset($data['overdue_notices']) ?? 0,
                    'tax_exempt' => isset($data['tax_exempt']) ?? 0,
                    'seperate_invoice' => isset($data['seperate_invoice']) ?? 0,
                    'disable_cc_processing' => isset($data['disable_cc_processing']) ?? 0,
                    'marketing_email_opt_in' => isset($data['marketing_email_opt_in']) ?? 0,
                    'status_update' => isset($data['status_update']) ?? 0,
                    'allow_single_sign_on' => isset($data['allow_single_sign_on']) ?? 0,
                    'send_account_info_message' => isset($data['send_account_info_message']) ?? 0
                ])) {
                    return true;
                }
            }
            throw new GeneralException(trans('exceptions.backend.clientdetails.create_error'));
        });

    }

    /**
     * For deleting the respective model from storage
     *
     * @param User $clientdetail
     * @throws GeneralException
     * @return bool
     */
    public function delete(User $clientdetail)
    {
        if ($clientdetail->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.clientdetails.delete_error'));
    }

     /**
     * @param  $input
     *
     * @return mixed
     */
    protected function createUserStub($input)
    {
        $user = new User();
        $user->first_name = $input['first_name'];
        $user->last_name = $input['last_name'];
        $user->email = $input['email'];
        $user->password = Hash::make($input['password']);
        $user->active = isset($input['status']) ? 1 : 0;

        return $user;
    }


    /**
     * Function to create detail of user stub
     */
    protected function createUserDetailsStub($input, $user) 
    {
        $detail = new Client();
        $detail->user_id = $user->id;
        $detail->company_name = $input['company_name'];
        $detail->security_question = $input['security_question'];
        $detail->security_answer = $input['security_answer'];
        $detail->tax_id = $input['tax_id'];
        $detail->address_1 = $input['address_1'];
        $detail->address_2 = $input['address_2'];
        $detail->city = $input['city'];
        $detail->state_id = $input['state_id'];
        $detail->postcode = $input['postcode'];
        $detail->country_id = $input['country_id'];
        $detail->phone = $input['phone'];
        $detail->payment_method = $input['payment_method'];
        $detail->billing_contact = $input['billing_contact'];
        $detail->language_id = $input['language_id'];
        $detail->currency = $input['currency'];
        $detail->client_group = $input['client_group'];
        $detail->credit_balance = $input['credit_balance'];
        $detail->admin_notes = $input['admin_notes'];
        $detail->late_fees = isset($input['late_fees']) ?? 0;
        $detail->overdue_notices = isset($input['overdue_notices']) ?? 0;
        $detail->tax_exempt = isset($input['tax_exempt']) ?? 0;
        $detail->seperate_invoice = isset($input['seperate_invoice']) ?? 0;
        $detail->disable_cc_processing = isset($input['disable_cc_processing']) ?? 0;
        $detail->marketing_email_opt_in = isset($input['marketing_email_opt_in']) ?? 0;
        $detail->status_update = isset($input['status_update']) ?? 0; 
        $detail->allow_single_sign_on = isset($input['allow_single_sign_on']) ?? 0;
        $detail->send_account_info_message = isset($input['send_account_info_message']) ?? 0;

        return $detail;
    }


    /**
     * @param  $input
     * @param  $user
     *
     * @throws GeneralException
     *
     * @return null
     */
    protected function checkUserByEmail($input, $user = null)
    {
        //Figure out if email is not the same
        if ($user && $user->email === $input['email']) {
            return;
        }

        //Check to see if email exists
        if ($this->query()->where('email', '=', $input['email'])->withTrashed()->exists()) {
            throw new GeneralException(trans('exceptions.backend.access.users.email_error'));
        }
    }
}
