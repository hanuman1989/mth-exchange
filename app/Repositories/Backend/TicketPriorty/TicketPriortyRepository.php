<?php

namespace App\Repositories\Backend\TicketPriorty;

use DB;
use Carbon\Carbon;
use App\Models\TicketPriorty\TicketPriorty;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TicketPriortyRepository.
 */
class TicketPriortyRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = TicketPriorty::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.ticketpriorties.table').'.id',
				config('module.ticketpriorties.table').'.title',
				config('module.ticketpriorties.table').'.status',
                config('module.ticketpriorties.table').'.created_at',
                config('module.ticketpriorties.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
		$input['status'] = isset($input['status']) ? 1 : 0;
        if (TicketPriorty::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.ticketpriorties.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param TicketPriorty $ticketpriorty
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(TicketPriorty $ticketpriorty, array $input)
    {
		$input['status'] = isset($input['status']) ? 1 : 0;
    	if ($ticketpriorty->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.ticketpriorties.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param TicketPriorty $ticketpriorty
     * @throws GeneralException
     * @return bool
     */
    public function delete(TicketPriorty $ticketpriorty)
    {
        if ($ticketpriorty->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.ticketpriorties.delete_error'));
    }
}
