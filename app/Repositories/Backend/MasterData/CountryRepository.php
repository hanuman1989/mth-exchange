<?php

namespace App\Repositories\Backend\MasterData;

use DB;
use App\Models\MasterData\Country;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CountryRepository.
 */
class CountryRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Country::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForCountryDataTable()
    {
        return $this->query()
            ->select([
                'id',
                'name',
                'status',
                'country_code',
                'created_at',
                'updated_at',
            ]);
    }

    /**
     * @param array $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create(array $input)
    {

        DB::transaction(function () use ($input) {

            $input['status'] = isset($input['status']) ? 1 : 0;
            if ($country = Country::create($input)) {
                return true;
            }

            throw new GeneralException(trans('exceptions.backend.master-data.countries.create_error'));
        });
    }

    /**
     * Update Country.
     *
     * @param \App\Models\MasterData\Country $country
     * @param array                  $input
     */
    public function update(Country $country, array $input)
    {
        $input['status'] = isset($input['status']) ? 1 : 0;

        DB::transaction(function () use ($country, $input) {
            if ($country->update($input)) {
                return true;
            }

            throw new GeneralException(
                trans('exceptions.backend.master-data.countries.update_error')
            );
        });
    }

     /**
     * @param \App\Models\MasterData\Country $Country
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(Country $country)
    {
        DB::transaction(function () use ($country) {
            if ($country->delete()) {
                return true;
            }

            throw new GeneralException(trans('exceptions.backend.master-data.countries.delete_error'));
        });
    }
}
