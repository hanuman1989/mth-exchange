<?php

namespace App\Repositories\Backend\MasterData;

use DB;
use App\Models\MasterData\State;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class StateRepository.
 */
class StateRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = State::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForStateDataTable()
    {
        return $this->query()
            ->select([
                'id',
                'name',
                'status',
                'code',
                'created_at',
                'updated_at',
            ]);
    }

     /**
     * @param array $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create(array $input)
    {

        DB::transaction(function () use ($input) {

            $input['status'] = isset($input['status']) ? 1 : 0;
            if ($state = State::create($input)) {
                return true;
            }

            throw new GeneralException(trans('exceptions.backend.master-data.states.create_error'));
        });
    }

    /**
     * Update State.
     *
     * @param \App\Models\MasterData\State $state
     * @param array                  $input
     */
    public function update(State $state, array $input)
    {
        $input['status'] = isset($input['status']) ? 1 : 0;

        DB::transaction(function () use ($state, $input) {
            if ($state->update($input)) {
                return true;
            }

            throw new GeneralException(
                trans('exceptions.backend.master-data.states.update_error')
            );
        });
    }

     /**
     * @param \App\Models\MasterData\State $state
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(State $state)
    {
        DB::transaction(function () use ($state) {
            if ($state->delete()) {
                return true;
            }

            throw new GeneralException(trans('exceptions.backend.master-data.states.delete_error'));
        });
    }
}
