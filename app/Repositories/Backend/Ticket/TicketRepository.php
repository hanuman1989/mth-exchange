<?php

namespace App\Repositories\Backend\Ticket;

use DB;
use Carbon\Carbon;
use App\Models\Ticket\Ticket;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Auth;
/**
 * Class TicketRepository.
 */
class TicketRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Ticket::class;
	
	 public function __construct()
    {
        $this->upload_path = 'img'.DIRECTORY_SEPARATOR.'backend'.DIRECTORY_SEPARATOR.'tickets'.DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('public');
    }

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
         return $this->query()
            ->leftjoin(config('module.ticketreplies.table'), config('module.ticketreplies.table').'.id', '=', config('module.tickets.table').'.ticket_reply_id')
            ->select([
                config('module.tickets.table').'.id',
				config('module.tickets.table').'.ticket_reply_id',
				config('module.tickets.table').'.ticket_reply_id',
                config('module.tickets.table').'.created_at',
                config('module.ticketreplies.table').'.subject',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
		 if (isset($input['file']) && !empty($input['file'])) {
		    $input = $this->uploadImage($input);
		 }
		$input['user_id'] = Auth::user()->id;
        if (Ticket::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.tickets.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Ticket $ticket
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Ticket $ticket, array $input)
    {
		
		// Uploading Image
        if (isset($input['file'])) {
            $this->deleteOldFile($ticket);
            $input = $this->uploadImage($input);
        }
    	if ($ticket->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.tickets.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Ticket $ticket
     * @throws GeneralException
     * @return bool
     */
    public function delete(Ticket $ticket)
    {
        if ($ticket->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.tickets.delete_error'));
    }
	
	  /**
     * Upload Image.
     *
     * @param array $input
     *
     * @return array $input
     */
    public function uploadImage($input)
    {
        $avatar = $input['file'];
      
	   
        if (isset($input['file']) && !empty($input['file'])) {
            $fileName = time().$avatar->getClientOriginalName();

            $this->storage->put($this->upload_path.$fileName, file_get_contents($avatar->getRealPath()));

            $input = array_merge($input, ['file' => $fileName]);

            return $input;
        }
    }

    /**
     * Destroy Old Image.
     *
     * @param int $id
     */
    public function deleteOldFile($model)
    {
        $fileName = $model->file;

        return $this->storage->delete($this->upload_path.$fileName);
    }
}
