<?php

namespace App\Repositories\Backend\ProductGroup;

use DB;
use Carbon\Carbon;
use App\Models\ProductGroup\ProductGroup;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductGroupRepository.
 */
class ProductGroupRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = ProductGroup::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
			config('module.product_groups.table').'.id',
                config('module.product_groups.table').'.name',
				 config('module.product_groups.table').'.head_line',
				  config('module.product_groups.table').'.tag_line',
                config('module.product_groups.table').'.created_at',
                config('module.product_groups.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input,$paymentGateway)
    {
           
        if ($value = ProductGroup::create($input)) {
			$value->paymentGateway()->attach($paymentGateway);
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.productgroups.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param ProductGroup $productgroup
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(ProductGroup $productgroup, array $input,$paymentGateway)
    {
    	if ($value = $productgroup->update($input))
			$productgroup->paymentGateway()->sync($paymentGateway);
            return true;

        throw new GeneralException(trans('exceptions.backend.productgroups.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param ProductGroup $productgroup
     * @throws GeneralException
     * @return bool
     */
    public function delete(ProductGroup $productgroup)
    {
        if ($productgroup->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.productgroups.delete_error'));
    }
}
