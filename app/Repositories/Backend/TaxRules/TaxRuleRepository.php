<?php

namespace App\Repositories\Backend\TaxRules;

use DB;
use Carbon\Carbon;
use App\Models\TaxRules\TaxRule;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TaxRuleRepository.
 */
class TaxRuleRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = TaxRule::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->leftJoin('countries as country', function($join){
                $join->on('country.id', '=', 'tax_rules.country_id');
            })
            ->select([
                "country.name",
                'tax_rules.id',
                'tax_rules.tax_name',
                'tax_rules.tax_rate',
                'tax_rules.country_id',
                'tax_rules.status',
                'tax_rules.created_at',
                'tax_rules.updated_at'
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        $input['status'] = isset($input['status']) ? 1 : 0;
        if (TaxRule::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.taxrules.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param TaxRule $taxrule
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(TaxRule $taxrule, array $input)
    {
        $input['status'] = isset($input['status']) ? 1 : 0;
    	if ($taxrule->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.taxrules.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param TaxRule $taxrule
     * @throws GeneralException
     * @return bool
     */
    public function delete(TaxRule $taxrule)
    {
        if ($taxrule->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.taxrules.delete_error'));
    }
}
