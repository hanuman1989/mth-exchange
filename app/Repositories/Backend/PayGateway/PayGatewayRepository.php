<?php

namespace App\Repositories\Backend\PayGateway;

use DB;
use Carbon\Carbon;
use App\Models\PayGateway\PayGateway;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PayGatewayRepository.
 */
class PayGatewayRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = PayGateway::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.paygateways.table') . '.id',
                config('module.paygateways.table') . '.title',
                config('module.paygateways.table') . '.created_at',
                config('module.paygateways.table') . '.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        if (PayGateway::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.paygateways.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param PayGateway $paygateway
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(PayGateway $paygateway, array $input)
    {
        if ($paygateway->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.paygateways.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param PayGateway $paygateway
     * @throws GeneralException
     * @return bool
     */
    public function delete(PayGateway $paygateway)
    {
        if ($paygateway->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.paygateways.delete_error'));
    }
}
