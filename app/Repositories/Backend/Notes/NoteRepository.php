<?php

namespace App\Repositories\Backend\Notes;

use DB;
use Carbon\Carbon;
use App\Models\Notes\Note;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class NoteRepository.
 */
class NoteRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Note::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.notes.table').'.id',
                config('module.notes.table').'.notes',
                config('module.notes.table').'.added_by',
                config('module.notes.table').'.created_at',
                config('module.notes.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        if (Note::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.notes.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Note $note
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Note $note, array $input)
    {
    	if ($note->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.notes.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Note $note
     * @throws GeneralException
     * @return bool
     */
    public function delete(Note $note)
    {
        if ($note->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.notes.delete_error'));
    }
}
