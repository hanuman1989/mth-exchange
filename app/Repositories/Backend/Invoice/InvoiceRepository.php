<?php

namespace App\Repositories\Backend\Invoice;

use DB;
use Carbon\Carbon;
use App\Models\Invoice\Invoice;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class InvoiceRepository.
 */
class InvoiceRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Invoice::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable($client_id)
    {
        return $this->query()
            ->select([
                config('module.invoices.table').'.id',
                config('module.invoices.table').'.invoice_number',
                config('module.invoices.table').'.line_item_description',
                config('module.invoices.table').'.payment_method',
                config('module.invoices.table').'.status',
                config('module.invoices.table').'.invoice_date',
                config('module.invoices.table').'.due_date',
                config('module.invoices.table').'.date_paid',
                config('module.invoices.table').'.due_from',
                config('module.invoices.table').'.due_to',
                config('module.invoices.table').'.created_at',
                config('module.invoices.table').'.updated_at',
            ])->where('user_id', $client_id);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        if (Invoice::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.invoices.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Invoice $invoice
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Invoice $invoice, array $input)
    {
    	if ($invoice->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.invoices.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Invoice $invoice
     * @throws GeneralException
     * @return bool
     */
    public function delete(Invoice $invoice)
    {
        if ($invoice->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.invoices.delete_error'));
    }
}
