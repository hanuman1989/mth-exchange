<?php

namespace App\Repositories\Backend\TicketStatus;

use DB;
use Carbon\Carbon;
use App\Models\TicketStatus\TicketStatus;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TicketStatusRepository.
 */
class TicketStatusRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = TicketStatus::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.ticketstatuses.table').'.id',
				config('module.ticketstatuses.table').'.title',
				config('module.ticketstatuses.table').'.status',
                config('module.ticketstatuses.table').'.created_at',
                config('module.ticketstatuses.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
		$input['status'] = isset($input['status']) ? 1 : 0;
        if (TicketStatus::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.ticketstatuses.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param TicketStatus $ticketstatus
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(TicketStatus $ticketstatus, array $input)
    {
		$input['status'] = isset($input['status']) ? 1 : 0;
    	if ($ticketstatus->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.ticketstatuses.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param TicketStatus $ticketstatus
     * @throws GeneralException
     * @return bool
     */
    public function delete(TicketStatus $ticketstatus)
    {
        if ($ticketstatus->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.ticketstatuses.delete_error'));
    }
}
