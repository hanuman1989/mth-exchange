<?php

namespace App\Repositories\Backend\BackupLogs;

use DB;
use App\Models\SystemBackup\SystemBackup;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BackupLogsRepository.
 */
class BackupLogsRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = SystemBackup::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                'backup_logs.id',
                'backup_logs.file',
                'backup_logs.file_path',
                'backup_logs.updated_at',
            ]);
    }

}
