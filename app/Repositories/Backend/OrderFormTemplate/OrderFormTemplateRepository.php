<?php

namespace App\Repositories\Backend\OrderFormTemplate;

use DB;
use Carbon\Carbon;
use App\Models\OrderFormTemplate\OrderFormTemplate;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

/**
 * Class OrderFormTemplateRepository.
 */
class OrderFormTemplateRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = OrderFormTemplate::class;
	 protected $upload_path;
	 
	 /**
     * Storage Class Object.
     *
     * @var \Illuminate\Support\Facades\Storage
     */
    protected $storage;
	
	 public function __construct()
    {
        $this->upload_path = 'img'.DIRECTORY_SEPARATOR.'backend'.DIRECTORY_SEPARATOR.'order_form_template_images'.DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('public');
    }

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.orderformtemplates.table').'.id',
				config('module.orderformtemplates.table').'.name',
				 config('module.orderformtemplates.table').'.status',
                config('module.orderformtemplates.table').'.created_at',
                config('module.orderformtemplates.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
		 $input = $this->uploadImage($input);
		 
        if (OrderFormTemplate::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.orderformtemplates.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param OrderFormTemplate $orderformtemplate
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(OrderFormTemplate $orderformtemplate, array $input)
    {
		// Uploading Image
        if ($input->hasFile('image')) {
            $this->deleteOldFile($orderformtemplate);
            $input = $this->uploadImage($input);
        }
    	if ($orderformtemplate->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.orderformtemplates.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param OrderFormTemplate $orderformtemplate
     * @throws GeneralException
     * @return bool
     */
    public function delete(OrderFormTemplate $orderformtemplate)
    {
        if ($orderformtemplate->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.orderformtemplates.delete_error'));
    }
	
	  /**
     * Upload Image.
     *
     * @param array $input
     *
     * @return array $input
     */
    public function uploadImage($input)
    {
        $avatar = $input['image'];
      
	   
        if (isset($input['image']) && !empty($input['image'])) {
            $fileName = time().$avatar->getClientOriginalName();

            $this->storage->put($this->upload_path.$fileName, file_get_contents($avatar->getRealPath()));

            $input = array_merge($input, ['image' => $fileName]);

            return $input;
        }
    }

    /**
     * Destroy Old Image.
     *
     * @param int $id
     */
    public function deleteOldFile($model)
    {
        $fileName = $model->image;

        return $this->storage->delete($this->upload_path.$fileName);
    }
}
