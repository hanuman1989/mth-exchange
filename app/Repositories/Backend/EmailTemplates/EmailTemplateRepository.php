<?php

namespace App\Repositories\Backend\EmailTemplates;
use App\Exceptions\GeneralException;
use App\Models\EmailTemplates\EmailTemplate;
use App\Repositories\BaseRepository;
use Illuminate\Support\Str;

/**
 * Class EmailTemplateRepository.
 */
class EmailTemplateRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = EmailTemplate::class;

    /**
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->leftjoin(config('access.users_table'), config('access.users_table').'.id', '=', config('module.emailtemplates.table').'.created_by')
            ->select([
                config('module.emailtemplates.table').'.id',
                config('module.emailtemplates.table').'.title',
                config('module.emailtemplates.table').'.page_slug',
                config('module.emailtemplates.table').'.status',
                config('module.emailtemplates.table').'.created_at',
                config('module.emailtemplates.table').'.updated_at',
                config('access.users_table').'.name as created_by',
            ]);
    }

    /**
     * @param array $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create(array $input)
    {
        if ($this->query()->where('title', $input['title'])->first()) {
            throw new GeneralException(trans('exceptions.backend.emailtemplates.already_exists'));
        }

        // Making extra fields
        $input['page_slug'] = Str::slug($input['title']);
        $input['status'] = isset($input['status']) ? 1 : 0;
        $input['created_by'] = auth()->id();

        if ($email = EmailTemplate::create($input)) {
            

            return $email;
        }

        throw new GeneralException(trans('exceptions.backend.emailtemplates.create_error'));
    }

    /**
     * @param App\Models\EmailTemplates\EmailTemplate $email
     * @param array                 $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function update($email, array $input)
    {
        if ($this->query()->where('title', $input['title'])->where('id', '!=', $email->id)->first()) {
            throw new GeneralException(trans('exceptions.backend.emailtemplates.already_exists'));
        }

        // Making extra fields
        $input['page_slug'] = Str::slug($input['title']);
        $input['status'] = isset($input['status']) ? 1 : 0;
        $input['updated_by'] = access()->user()->id;

        if ($email->update($input)) {
            

            return true;
        }

        throw new GeneralException(trans('exceptions.backend.emailtemplates.update_error'));
    }

    /**
     * @param App\Models\EmailTemplates\EmailTemplate $email
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function delete($email)
    {
        if ($email->delete()) {
            

            return true;
        }

        throw new GeneralException(trans('exceptions.backend.emailtemplates.delete_error'));
    }
}
