<?php

namespace App\Domains\Auth\Models\Traits\Attribute;

use Illuminate\Support\Facades\Hash;

/**
 * Trait UserAttribute.
 */
trait UserAttribute
{
    /**
     * @param $password
     */
    public function setPasswordAttribute($password): void
    {
        // If password was accidentally passed in already hashed, try not to double hash it
        // Note: Password Histories are logged from the \App\Domains\Auth\Observer\UserObserver class
        $this->attributes['password'] =
            (strlen($password) === 60 && preg_match('/^\$2y\$/', $password)) ||
            (strlen($password) === 95 && preg_match('/^\$argon2i\$/', $password)) ?
                $password :
                Hash::make($password);
    }

    /**
     * @return mixed
     */
    public function getAvatarAttribute()
    {
        return $this->getAvatar();
    }

    /**
     * @return mixed 
     * Return contact name
     */
    public function getContactNameAttribute() 
    {
        return $this->first_name." ".$this->last_name;
    }

    /**
     * @return string
     */
    public function getPermissionsLabelAttribute()
    {
        if ($this->hasAllAccess()) {
            return 'All';
        }

        if (! $this->permissions->count()) {
            return 'None';
        }

        return collect($this->getPermissionDescriptions())
            ->implode('<br/>');
    }

    /**
     * @return string
     */
    public function getRolesLabelAttribute()
    {
        if ($this->hasAllAccess()) {
            return 'All';
        }

        if (! $this->roles->count()) {
            return 'None';
        }

        return collect($this->getRoleNames())
            ->each(function ($role) {
                return ucwords($role);
            })
            ->implode('<br/>');
    }

    /**
     * @return string
     */
    public function getClientActionButtonsAttribute()
    {
        return '<div class="btn-group action-btn">
                '.$this->getClientEditButtonAttribute('btn btn-default btn-flat').'
                '.$this->getClientDeleteButtonAttribute('admin.clients.destroy').'
                '.$this->getClientViewButtonAttribute('admin.clients.show').'
            </div>';
    }

    /**
     * Function to edit client details from client panel
     */
    public function getClientEditButtonAttribute($class) 
    {
        return '<a class="'.$class.'" href="'.route('admin.clients.edit', $this).'">
                <i data-toggle="tooltip" data-placement="top" title="Edit" class="fa fa-pencil"></i>
            </a>';
    }

    /**
     * Function to delete client details from client panel
     */
    public function getClientDeleteButtonAttribute($route) 
    {
        return '<a href="'.route($route, $this).'" 
                class="btn btn-flat btn-default" data-method="delete"
                data-trans-button-cancel="'.trans('buttons.general.cancel').'"
                data-trans-button-confirm="'.trans('buttons.general.crud.delete').'"
                data-trans-title="'.trans('strings.backend.general.are_you_sure').'">
                    <i data-toggle="tooltip" data-placement="top" title="Delete" class="fa fa-trash"></i>
            </a>';
    }

    /**
     * Function to view client details from client panel
     */
    public function getClientviewButtonAttribute($route) 
    {
        return '<a class="btn btn-flat btn-default" href="'.route('admin.clients.show', $this).'">
                <i data-toggle="tooltip" data-placement="top" title="View" class="fa fa-eye"></i>
            </a>';
    }

     /**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        if ($this->isUserActive()) {
            return "<label class='label label-success'>".trans('labels.general.active').'</label>';
        }

        return "<label class='label label-danger'>".trans('labels.general.inactive').'</label>';
    }

    /**
     * @return bool
     */
    public function isUserActive()
    {
        return $this->active == 1;
    }
}
