<?php

namespace App\Domains\Auth\Models\Traits\Scope;

/**
 * Class UserScope.
 */
trait UserScope
{
    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeOnlyDeactivated($query)
    {
        return $query->whereActive(false);
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeOnlyActive($query)
    {
        return $query->whereActive(true);
    }

    /**
     * @param $query
     * @param $type
     *
     * @return mixed
     */
    public function scopeByType($query, $type)
    {
        return $query->where('type', $type);
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeAllAccess($query)
    {
        return $query->whereHas('roles', function ($query) {
            $query->where('name', config('boilerplate.access.role.admin'));
        });
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeAdmins($query)
    {
        return $query->where('type', $this::TYPE_ADMIN);
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeUsers($query)
    {
        return $query->where('type', $this::TYPE_USER);
    }

    /**
     * @param $query
     * 
     * Scope a query to filter record with tax exempt status
     * @return mixed
     */
    public function scopeTaxExemptStatus($query, $param) {
        if($param != NULL) {
            $query = $query->where('client.tax_exempt', $param);
        }
        return $query;
    }

    /**
     * @param $query
     * Scope a query to filter record with company
     * @return mixed
     */
    public function scopeCompany($query, $param) {
        if($param != NULL) {
            $query = $query->where('client.company_name','LIKE', '%'.$param.'%');
        }
        return $query;
    }

    /**
     * @param $query
     * Scope a query to filter record with email
     * @return mixed
     */
    public function scopeEmail($query, $param) {
        if($param != NULL) {
            $query = $query->where('users.email', 'LIKE','%'.$param.'%');
        }
        return $query;
    }

    /**
     * @param $query
     * Scope a query to filter record with client phone number
     * @return mixed
     */
    public function scopePhone($query, $param) {
        if($param != NULL) {
            $query = $query->where('client.phone','LIKE', '%'.$param.'%');
        }
        return $query;
    }

    /**
     * @param $query
     * Scope a query to filter record with client status
     * @return mixed
     */
    public function scopeStatus($query, $param) {
        if($param != NULL) {
            $query = $query->where('users.status', $param);
        }
        return $query;
    }

    /**
     * @param $query
     * Scope a query to filter record with client address line 1
     * @return mixed
     */
    public function scopeAddress1($query, $param) {
        if($param != NULL) {
            $query = $query->where('client.address1', 'LIKE', '%'.$param.'%');
        }
        return $query;
    }

    /**
     * @param $query
     * Scope a query to filter record with client address line 2
     * @return mixed
     */
    public function scopeAddress2($query, $param) {
        if($param != NULL) {
            $query = $query->where('client.address2', 'LIKE', '%'.$param.'%');
        }
        return $query;
    }

    /**
     * @param $query
     * Scope a query to filter record with client city
     * @return mixed
     */
    public function scopeCity($query, $param) {
        if($param != NULL) {
            $query = $query->where('client.city', 'LIKE','%'.$param.'%');
        }
        return $query;
    }

    /**
     * @param $query
     * Scope a query to filter record with client state
     * @return mixed
     */
    public function scopeState($query, $param) {
        if($param != NULL) {
            $query = $query->where('client.state', $param);
        }
        return $query;
    }


    /**
     * @param $query
     * Scope a query to filter record with client postcode
     * @return mixed
     */
    public function scopePostcode($query, $param) {
        if($param != NULL) {
            $query = $query->where('client.postcode','LIKE', '%'.$param.'%');
        }
        return $query;
    }

    /**
     * @param $query
     * Scope a query to filter record with client country
     * @return mixed
     */
    public function scopeCountry($query, $param) {
        if($param != NULL) {
            $query = $query->where('client.country', $param);
        }
        return $query;
    }

    /**
     * @param $query
     * Scope a query to filter record with client payment method
     * @return mixed
     */
    public function scopePaymentMethod($query, $param) {
        if($param != NULL) {
            $query = $query->where('client.payment_method', $param);
        }
        return $query;
    }

    /**
     * @param $query
     * Scope a query to filter record with client auto credit card billing option
     * @return mixed
     */
    public function scopeAutoCreditCardBilling($query, $param) {
        if($param != NULL) {
            $query = $query->where('client.auto_credit_card_billing', $param);
        }
        return $query;
    }

    /**
     * @param $query
     * Scope a query to filter record with client credit balance
     * @return mixed
     */
    public function scopeCreditBalance($query, $param) {
        if($param != NULL) {
            $query = $query->where('client.credit_balance', $param);
        }
        return $query;
    }


    /**
     * @param $query
     * Scope a query to filter record with client currency
     * @return mixed
     */
    public function scopeCurrency($query, $param) {
        if($param != NULL) {
            $query = $query->where('client.currency', $param);
        }
        return $query;
    }

    /**
     * @param $query
     * Scope a query to filter record with client language
     * @return mixed
     */
    public function scopeLanguage($query, $param) {
        if($param != NULL) {
            $query = $query->where('client.language', $param);
        }
        return $query;
    }

    /**
     * @param $query
     * Scope a query to filter record with client marketing email opt in option
     * @return mixed
     */
    public function scopeMarketingEmailOptIn($query, $param) {
        if($param != NULL) {
            $query = $query->where('client.marketing_emails_opt_in', $param);
        }
        return $query;
    }

    /**
     * @param $query
     * Scope a query to filter record with clientauto status update
     * @return mixed
     */
    public function scopeAutoStatusUpdate($query, $param) {
        if($param != NULL) {
            $query = $query->where('client.auto_status_update', $param);
        }
        return $query;
    }

    /**
     * @param $query
     * Scope a query to filter record with client late fees option
     * @return mixed
     */
    public function scopeLateFees($query, $param) {
        if($param != NULL) {
            $query = $query->where('client.late_fees', $param);
        }
        return $query;
    }

    /**
     * @param $query
     * Scope a query to filter record with client overdue notice option
     * @return mixed
     */
    public function scopeOverdueNotices($query, $param) {
        if($param != NULL) {
            $query = $query->where('client.overdue_notices', $param);
        }
        return $query;
    }

    /**
     * @param $query
     * Scope a query to filter record with client seperate invoice option
     * @return mixed
     */
    public function scopeSeperateInvoice($query, $param) {
        if($param != NULL) {
            $query = $query->where('client.seperate_invoice', $param);
        }
        return $query;
    }
}
