<?php

namespace App\Domains\Auth\Models\Traits\Relationship;

use App\Domains\Auth\Models\PasswordHistory;
use App\Models\Clients\Client;
use App\Models\XeroSync\XeroSync;

/**
 * Class UserRelationship.
 */
trait UserRelationship
{
    /**
     * @return mixed
     */
    public function passwordHistories()
    {
        return $this->morphMany(PasswordHistory::class, 'model');
    }
  

    

    public function allow($nameOrId)
    {
        /*
         *
         * Update for this function due to issue of user custom permission
         */

         $permissions=\App\Domains\Auth\Models\Permission::get();

         
        //Check permissions directly tied to user
        foreach ($permissions as $perm) {

            //First check to see if it's an ID
            if (is_numeric($nameOrId)) {
                if ($perm->id == $nameOrId) {
                    return true;
                }
            }

            //Otherwise check by name
            if ($perm->name == $nameOrId) {

                return true;

            }
        }

        

        return false;
    }

     /**
     * Return client type user details
     */
    public function detail()
    {
        return $this->hasOne(Client::class);
    }

    /**
     * Function to maintain xero syncs for users
     */
    public function xero_contact() {
        return $this->hasMany(XeroSync::class, 'xerosyncable_id', 'id');
    }
}
