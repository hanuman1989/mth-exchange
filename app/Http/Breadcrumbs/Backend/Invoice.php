<?php

Breadcrumbs::register('admin.invoices.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(trans('menus.backend.invoices.management'), route('admin.invoices.index'));
});

Breadcrumbs::register('admin.invoices.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.invoices.index');
    $breadcrumbs->push(trans('menus.backend.invoices.create'), route('admin.invoices.create'));
});

Breadcrumbs::register('admin.invoices.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.invoices.index');
    $breadcrumbs->push(trans('menus.backend.invoices.edit'), route('admin.invoices.edit', $id));
});
