<?php

Breadcrumbs::register('admin.securityquestions.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(trans('menus.backend.securityquestions.management'), route('admin.securityquestions.index'));
});

Breadcrumbs::register('admin.securityquestions.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.securityquestions.index');
    $breadcrumbs->push(trans('menus.backend.securityquestions.create'), route('admin.securityquestions.create'));
});

Breadcrumbs::register('admin.securityquestions.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.securityquestions.index');
    $breadcrumbs->push(trans('menus.backend.securityquestions.edit'), route('admin.securityquestions.edit', $id));
});
