<?php

Breadcrumbs::register('admin.notes.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(trans('menus.backend.notes.management'), route('admin.notes.index'));
});

Breadcrumbs::register('admin.notes.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.notes.index');
    $breadcrumbs->push(trans('menus.backend.notes.create'), route('admin.notes.create'));
});

Breadcrumbs::register('admin.notes.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.notes.index');
    $breadcrumbs->push(trans('menus.backend.notes.edit'), route('admin.notes.edit', $id));
});
