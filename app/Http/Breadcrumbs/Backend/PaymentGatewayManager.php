<?php

Breadcrumbs::register('admin.paymentgatewaymanagers.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(trans('menus.backend.paymentgatewaymanagers.management'), route('admin.paymentgatewaymanagers.index'));
});

Breadcrumbs::register('admin.paymentgatewaymanagers.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.paymentgatewaymanagers.index');
    $breadcrumbs->push(trans('menus.backend.paymentgatewaymanagers.create'), route('admin.paymentgatewaymanagers.create'));
});

Breadcrumbs::register('admin.paymentgatewaymanagers.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.paymentgatewaymanagers.index');
    $breadcrumbs->push(trans('menus.backend.paymentgatewaymanagers.edit'), route('admin.paymentgatewaymanagers.edit', $id));
});
