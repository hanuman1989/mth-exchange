<?php

Breadcrumbs::register('admin.clientdetails.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(trans('menus.backend.clientdetails.management'), route('admin.clientdetails.index'));
});

Breadcrumbs::register('admin.clientdetails.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.clientdetails.index');
    $breadcrumbs->push(trans('menus.backend.clientdetails.create'), route('admin.clientdetails.create'));
});

Breadcrumbs::register('admin.clientdetails.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.clientdetails.index');
    $breadcrumbs->push(trans('menus.backend.clientdetails.edit'), route('admin.clientdetails.edit', $id));
});
