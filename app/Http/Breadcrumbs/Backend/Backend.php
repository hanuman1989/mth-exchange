<?php

Breadcrumbs::register('admin.dashboard', function ($breadcrumbs) {
    $breadcrumbs->push(__('navs.backend.dashboard'), route('admin.dashboard'));
});

require __DIR__.'/Search.php';
require __DIR__.'/Access/User.php';
require __DIR__.'/Access/Role.php';
require __DIR__.'/Access/Permission.php';
require __DIR__.'/Page.php';
require __DIR__.'/Setting.php';
require __DIR__.'/Blog_Category.php';
require __DIR__.'/Blog_Tag.php';
require __DIR__.'/Blog_Management.php';
require __DIR__.'/Faqs.php';
require __DIR__.'/Menu.php';
require __DIR__.'/LogViewer.php';

require __DIR__.'/PaymentGatewayManager.php';
require __DIR__.'/Testingss.php';
require __DIR__.'/Laptop.php';
require __DIR__.'/Hanuman.php';
require __DIR__.'/Deepak.php';
require __DIR__.'/Mobile.php';
require __DIR__.'/Deltum.php';
require __DIR__.'/Akhilesh.php';
require __DIR__.'/Note.php';
require __DIR__.'/Invoice.php';