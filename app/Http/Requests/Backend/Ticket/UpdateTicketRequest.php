<?php

namespace App\Http\Requests\Backend\Ticket;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTicketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('update-ticket');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'ticket_reply_id' => 'required',
		   'content'=> 'required',
		   'file'    => 'mimes:jpg,jpeg,png|image|max:10000',
        ];
    }

    public function messages()
    {
        return [
            'ticket_reply_id.required' => 'Please select subject.',
			'content.required' => 'Please enter content',
            'file.mimes'     => 'Invalid image - should be jpg,jpeg,png type',
            'file.max'     => 'image size must be less than 10 MB.',
        ];
    }
}
