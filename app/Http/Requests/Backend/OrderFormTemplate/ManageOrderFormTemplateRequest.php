<?php

namespace App\Http\Requests\Backend\OrderFormTemplate;

use Illuminate\Foundation\Http\FormRequest;

class ManageOrderFormTemplateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('manage-orderformtemplate');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           
        ];
    }

    public function messages()
    {
        return [
           
        ];
    }
}
