<?php

namespace App\Http\Requests\Backend\OrderFormTemplate;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrderFormTemplateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('store-orderformtemplate');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'name'              => 'required|max:191',
		   'image'    => 'required|mimes:jpg,jpeg,png|image|max:10000',
        ];
    }

    public function messages()
    {
        return [
           'name.required' => 'Please enter  name.',
           'name.max' => 'name should not be more than 150 charcters long.',
           'image.mimes'     => 'Invalid image - should be jpg,jpeg,png type',
           'image.required'     => 'Please upload image',
           'image.max'     => 'image size must be less than 10 MB.',
        ];
    }
}
