<?php

namespace App\Http\Requests\Backend\BackupLogs;

use Illuminate\Foundation\Http\FormRequest;

/**
 * BackupLogsRequest for manage authorization of backup logs
 * @method authorize
 */
class BackupLogsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('manage-backup-logs');
    }

     /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
        ];
    }

    public function messages()
    {
        return [
            
        ];
    }
}
