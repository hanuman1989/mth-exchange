<?php

namespace App\Http\Requests\Backend\Clients;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * UpdateClientRequest for manage client
 * @method authorize,rules,messages
 */
class UpdateClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('update-clientdetail');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'      => 'required|max:255',
            'last_name'       => 'required|max:255',
            // 'email'           => 'required|email|unique:users,email,'.$this->clients->id.',id,deleted_at,NULL',
            'password'        => 'sometimes|nullable|min:6'
            
        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => 'Please enter first name.',
            'last_name.required' => 'Please enter last name.',
            'email.required' => 'Please enter email address.'
        ];
    }
}
