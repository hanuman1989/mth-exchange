<?php

namespace App\Http\Requests\Backend\Clients;

use Illuminate\Foundation\Http\FormRequest;

/**
 * ClientRequest for manage client
 * @method authorize
 */
class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('manage-clientdetail');
    }

     /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
        ];
    }

    public function messages()
    {
        return [
            
        ];
    }
}
