<?php

namespace App\Http\Requests\Backend\Clients;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * StoreClientRequest for manage client
 * @method authorize,rules,messages
 */
class StoreClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('store-clientdetail');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'first_name'      => 'required|max:255',
                'last_name'       => 'required|max:255',
                'email'           => ['required', 'email', 'max:255', Rule::unique('users')],
                'password'        => 'required|min:6',
                'assignees_roles' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => 'Please enter first name.',
            'last_name.required' => 'Please enter last name.',
            'email.required' => 'Please enter email address.',
            'password.required' => 'Please enter password.'
        ];
    }
}
