<?php

namespace App\Http\Requests\Backend\SecurityQuestion;

use Illuminate\Foundation\Http\FormRequest;

/**
 * UpdateSecurityQuestionRequest for manage Security Question
 * @method authorize,rules,messages
 */
class UpdateSecurityQuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('update-securityquestion');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
        ];
    }

    public function messages()
    {
        return [
            
        ];
    }
}
