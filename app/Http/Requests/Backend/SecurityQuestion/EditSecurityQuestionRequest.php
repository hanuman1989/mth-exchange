<?php

namespace App\Http\Requests\Backend\SecurityQuestion;

use Illuminate\Foundation\Http\FormRequest;

/**
 * EditSecurityQuestionRequest for manage Security Question
 * @method authorize,rules,messages
 */
class EditSecurityQuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('edit-securityquestion');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
        ];
    }

    public function messages()
    {
        return [
            
        ];
    }
}
