<?php

namespace App\Http\Requests\Backend\SecurityQuestion;

use Illuminate\Foundation\Http\FormRequest;

/**
 * CreateSecurityQuestionRequest for manage Security Question
 * @method authorize,rules,messages
 */
class CreateSecurityQuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('create-securityquestion');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
        ];
    }

    public function messages()
    {
        return [
            
        ];
    }
}
