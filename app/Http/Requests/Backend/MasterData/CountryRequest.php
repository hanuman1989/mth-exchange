<?php

namespace App\Http\Requests\Backend\MasterData;

use App\Http\Requests\Request;

/**
 * Class ManageMasterDataRequest.
 */
class CountryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('manage-master-data');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
        ];
    }

    /**
     * Get the validation messages for each rule.
     *
     * @return array
     */
    public function messages()
    {
        return [
            
        ];
    }
}
