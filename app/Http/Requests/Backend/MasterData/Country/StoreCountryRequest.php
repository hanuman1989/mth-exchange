<?php

namespace App\Http\Requests\Backend\MasterData\Country;

use App\Http\Requests\Request;

/**
 * StoreCountryRequest for manage coutries
 * @method authorize,rules,messages
 */
class StoreCountryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('manage-master-data');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_code' => 'required|max:2',
            'name' => 'required|max:100'
        ];
    }

    /**
     * Get the validation messages for each rule.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'country_code.required' => 'Please enter country code.',
            'name' => 'Please enter country name.'
        ];
    }

}
