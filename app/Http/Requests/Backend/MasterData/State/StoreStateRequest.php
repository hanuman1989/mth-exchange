<?php

namespace App\Http\Requests\Backend\MasterData\State;

use App\Http\Requests\Request;

/**
 * StoreStateRequest for manage states
 * @method authorize,rules,messages
 */
class StoreStateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('manage-master-data');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|max:2',
            'name' => 'required|max:100'
        ];
    }

    /**
     * Get the validation messages for each rule.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'code.required' => 'Please Enter Code.',
            'name' => 'Please enter state name.'
        ];
    }

}
