<?php

namespace App\Http\Requests\Backend\EmailTemplates;

use App\Http\Requests\Request;

/**
 * Class StorePageRequest.
 */
class StoreEmailTemplateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'       => 'required|max:191',
            'template_data' => 'required',
        ];
    }
}
