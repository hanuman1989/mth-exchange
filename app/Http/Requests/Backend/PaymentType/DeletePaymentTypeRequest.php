<?php

namespace App\Http\Requests\Backend\PaymentType;

use Illuminate\Foundation\Http\FormRequest;

class DeletePaymentTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('delete-paymenttype');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           
        ];
    }

    public function messages()
    {
        return [
           
        ];
    }
}
