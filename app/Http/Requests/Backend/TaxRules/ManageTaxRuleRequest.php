<?php

namespace App\Http\Requests\Backend\TaxRules;

use Illuminate\Foundation\Http\FormRequest;

class ManageTaxRuleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('manage-taxrules');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function messages()
    {
        return [
            
        ];
    }
}
