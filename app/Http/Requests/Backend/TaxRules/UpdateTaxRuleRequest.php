<?php

namespace App\Http\Requests\Backend\TaxRules;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTaxRuleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('update-taxrules');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "tax_name" => 'required|max:100',
            "tax_rate" => 'required|numeric|between:0,99.99',
            "country_id" => 'required'
        ];
    }

    public function messages()
    {
        return [
            "country_id.required" => 'Please select country.'
        ];
    }
}
