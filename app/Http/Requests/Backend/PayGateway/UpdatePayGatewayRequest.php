<?php

namespace App\Http\Requests\Backend\PayGateway;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePayGatewayRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('update-paygateway');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'       => 'required|max:191',
        ];
    }

    public function messages()
    {
        return [
            'title.required'       => 'Display Name Cannot be blank',
            'title.max'       => 'Display Name Cannot be greater than 190 characters',
        ];
    }
}
