<?php

namespace App\Http\Controllers\Backend\TicketReply;

use App\Models\TicketReply\TicketReply;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\TicketReply\CreateResponse;
use App\Http\Responses\Backend\TicketReply\EditResponse;
use App\Repositories\Backend\TicketReply\TicketReplyRepository;
use App\Http\Requests\Backend\TicketReply\ManageTicketReplyRequest;
use App\Http\Requests\Backend\TicketReply\StoreTicketReplyRequest;
use App\Http\Requests\Backend\TicketReply\UpdateTicketReplyRequest;
use App\Http\Requests\Backend\TicketReply\DeleteTicketReplyRequest;

/**
 * TicketRepliesController
 */
class TicketRepliesController extends Controller
{
    /**
     * variable to store the repository object
     * @var TicketReplyRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param TicketReplyRepository $repository;
     */
    public function __construct(TicketReplyRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\TicketReply\ManageTicketReplyRequest  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManageTicketReplyRequest $request)
    {
        return new ViewResponse('backend.ticketreplies.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreateTicketReplyRequestNamespace  $request
     * @return \App\Http\Responses\Backend\TicketReply\CreateResponse
     */
    public function create(Request $request)
    {
        return new CreateResponse('backend.ticketreplies.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreTicketReplyRequestNamespace  $request
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreTicketReplyRequest $request)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Create the model using repository create method
        $this->repository->create($input);
        //return with successfull message
        return new RedirectResponse(route('admin.ticketreplies.index'), ['flash_success' => trans('alerts.backend.ticketreplies.created')]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\TicketReply\TicketReply  $ticketreply
     * @param  EditTicketReplyRequestNamespace  $request
     * @return \App\Http\Responses\Backend\TicketReply\EditResponse
     */
    public function edit(TicketReply $ticketreply,Request $request)
    {
        return new EditResponse($ticketreply);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateTicketReplyRequestNamespace  $request
     * @param  App\Models\TicketReply\TicketReply  $ticketreply
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(UpdateTicketReplyRequest $request, TicketReply $ticketreply)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Update the model using repository update method
        $this->repository->update( $ticketreply, $input );
        //return with successfull message
        return new RedirectResponse(route('admin.ticketreplies.index'), ['flash_success' => trans('alerts.backend.ticketreplies.updated')]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeleteTicketReplyRequestNamespace  $request
     * @param  App\Models\TicketReply\TicketReply  $ticketreply
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(TicketReply $ticketreply, DeleteTicketReplyRequest $request)
    {
        //Calling the delete method on repository
        $this->repository->delete($ticketreply);
        //returning with successfull message
        return new RedirectResponse(route('admin.ticketreplies.index'), ['flash_success' => trans('alerts.backend.ticketreplies.deleted')]);
    }
    
}
