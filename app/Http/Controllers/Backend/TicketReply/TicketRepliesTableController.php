<?php

namespace App\Http\Controllers\Backend\TicketReply;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\TicketReply\TicketReplyRepository;
use App\Http\Requests\Backend\TicketReply\ManageTicketReplyRequest;

/**
 * Class TicketRepliesTableController.
 */
class TicketRepliesTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var TicketReplyRepository
     */
    protected $ticketreply;

    /**
     * contructor to initialize repository object
     * @param TicketReplyRepository $ticketreply;
     */
    public function __construct(TicketReplyRepository $ticketreply)
    {
        $this->ticketreply = $ticketreply;
    }

    /**
     * This method return the data of the model
     * @param ManageTicketReplyRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageTicketReplyRequest $request)
    {
        return Datatables::of($this->ticketreply->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('created_at', function ($ticketreply) {
                return Carbon::parse($ticketreply->created_at)->toDateString();
            })
            ->addColumn('actions', function ($ticketreply) {
                return $ticketreply->action_buttons;
            })
            ->make(true);
    }
}
