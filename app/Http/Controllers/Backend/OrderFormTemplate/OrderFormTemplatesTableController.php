<?php

namespace App\Http\Controllers\Backend\OrderFormTemplate;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\OrderFormTemplate\OrderFormTemplateRepository;
use App\Http\Requests\Backend\OrderFormTemplate\ManageOrderFormTemplateRequest;

/**
 * Class OrderFormTemplatesTableController.
 * saving orderTemplate for product groups.
 */
class OrderFormTemplatesTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var OrderFormTemplateRepository
     */
    protected $orderformtemplate;

    /**
     * contructor to initialize repository object
     * @param OrderFormTemplateRepository $orderformtemplate;
     */
    public function __construct(OrderFormTemplateRepository $orderformtemplate)
    {
        $this->orderformtemplate = $orderformtemplate;
    }

    /**
     * This method return the data of the model
     * @param ManageOrderFormTemplateRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageOrderFormTemplateRequest $request)
    {
        return Datatables::of($this->orderformtemplate->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('created_at', function ($orderformtemplate) {
                return Carbon::parse($orderformtemplate->created_at)->toDateString();
            })
            ->addColumn('actions', function ($orderformtemplate) {
                return $orderformtemplate->action_buttons;
            })
            ->make(true);
    }
}
