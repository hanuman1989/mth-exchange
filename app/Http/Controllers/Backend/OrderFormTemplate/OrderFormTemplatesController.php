<?php

namespace App\Http\Controllers\Backend\OrderFormTemplate;

use App\Models\OrderFormTemplate\OrderFormTemplate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\OrderFormTemplate\CreateResponse;
use App\Http\Responses\Backend\OrderFormTemplate\EditResponse;
use App\Repositories\Backend\OrderFormTemplate\OrderFormTemplateRepository;
use App\Http\Requests\Backend\OrderFormTemplate\ManageOrderFormTemplateRequest;

use App\Http\Requests\Backend\OrderFormTemplate\StoreOrderFormTemplateRequest;
use App\Http\Requests\Backend\OrderFormTemplate\UpdateOrderFormTemplateRequest;
use App\Http\Requests\Backend\OrderFormTemplate\DeleteOrderFormTemplateRequest;

/**
 * OrderFormTemplatesController
 * used for listing of orderFormTemplates
 */
class OrderFormTemplatesController extends Controller
{
    /**
     * variable to store the repository object
     * @var OrderFormTemplateRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param OrderFormTemplateRepository $repository;
     */
    public function __construct(OrderFormTemplateRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\OrderFormTemplate\ManageOrderFormTemplateRequest  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManageOrderFormTemplateRequest $request)
    {
        return new ViewResponse('backend.orderformtemplates.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreateOrderFormTemplateRequestNamespace  $request
     * @return \App\Http\Responses\Backend\OrderFormTemplate\CreateResponse
     */
    public function create(Request $request)
    {
        return new CreateResponse('backend.orderformtemplates.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreOrderFormTemplateRequestNamespace  $request
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreOrderFormTemplateRequest $request)
    {
		
        //Input received from the request
        $input = $request->except(['_token']);
        //Create the model using repository create method
        $this->repository->create($input);
        //return with successfull message
        return new RedirectResponse(route('admin.orderformtemplates.index'), ['flash_success' => trans('alerts.backend.orderformtemplates.created')]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\OrderFormTemplate\OrderFormTemplate  $orderformtemplate
     * @param  EditOrderFormTemplateRequestNamespace  $request
     * @return \App\Http\Responses\Backend\OrderFormTemplate\EditResponse
     */
    public function edit(OrderFormTemplate $orderformtemplate, Request $request)
    {
        return new EditResponse($orderformtemplate);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateOrderFormTemplateRequestNamespace  $request
     * @param  App\Models\OrderFormTemplate\OrderFormTemplate  $orderformtemplate
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(UpdateOrderFormTemplateRequest $request, OrderFormTemplate $orderformtemplate)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Update the model using repository update method
        $this->repository->update( $orderformtemplate, $input );
        //return with successfull message
        return new RedirectResponse(route('admin.orderformtemplates.index'), ['flash_success' => trans('alerts.backend.orderformtemplates.updated')]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeleteOrderFormTemplateRequestNamespace  $request
     * @param  App\Models\OrderFormTemplate\OrderFormTemplate  $orderformtemplate
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(OrderFormTemplate $orderformtemplate, DeleteOrderFormTemplateRequest $request)
    {
        //Calling the delete method on repository
        $this->repository->delete($orderformtemplate);
        //returning with successfull message
        return new RedirectResponse(route('admin.orderformtemplates.index'), ['flash_success' => trans('alerts.backend.orderformtemplates.deleted')]);
    }
    
}
