<?php

namespace App\Http\Controllers\Backend\Language;

use App\Models\Language\Language;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\Language\CreateResponse;
use App\Http\Responses\Backend\Language\EditResponse;
use App\Repositories\Backend\Language\LanguageRepository;
use App\Http\Requests\Backend\Language\LanguageRequest;
use App\Http\Requests\Backend\Language\CreateLanguageRequest;
use App\Http\Requests\Backend\Language\StoreLanguageRequest;
use App\Http\Requests\Backend\Language\EditLanguageRequest;
use App\Http\Requests\Backend\Language\UpdateLanguageRequest;
use App\Http\Requests\Backend\Language\DeleteLanguageRequest;

/**
 * LanguagesController for manage languages
 * @method index,create,edit,update,destroy
 */
class LanguagesController extends Controller
{
    /**
     * variable to store the repository object
     * @var LanguageRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param LanguageRepository $repository;
     */
    public function __construct(LanguageRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\Language\LanguageRequest  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(LanguageRequest $request)
    {
        return new ViewResponse('backend.languages.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreateLanguageRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Language\CreateResponse
     */
    public function create(CreateLanguageRequest $request)
    {
        return new CreateResponse('backend.languages.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreLanguageRequestNamespace  $request
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreLanguageRequest $request)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Create the model using repository create method
        $this->repository->create($input);
        //return with successfull message
        return new RedirectResponse(route('admin.languages.index'), ['flash_success' => trans('alerts.backend.languages.created')]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\Language\Language  $language
     * @param  EditLanguageRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Language\EditResponse
     */
    public function edit(Language $language, EditLanguageRequest $request)
    {
        return new EditResponse($language);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateLanguageRequestNamespace  $request
     * @param  App\Models\Language\Language  $language
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(UpdateLanguageRequest $request, Language $language)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Update the model using repository update method
        $this->repository->update( $language, $input );
        //return with successfull message
        return new RedirectResponse(route('admin.languages.index'), ['flash_success' => trans('alerts.backend.languages.updated')]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeleteLanguageRequestNamespace  $request
     * @param  App\Models\Language\Language  $language
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(Language $language, DeleteLanguageRequest $request)
    {
        //Calling the delete method on repository
        $this->repository->delete($language);
        //returning with successfull message
        return new RedirectResponse(route('admin.languages.index'), ['flash_success' => trans('alerts.backend.languages.deleted')]);
    }
    
}
