<?php

namespace App\Http\Controllers\Backend\Clients;

use App\Models\Clients\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\Clients\CreateResponse;
use App\Http\Responses\Backend\Clients\EditResponse;
use App\Http\Responses\Backend\Clients\ShowResponse;
use App\Repositories\Backend\Clients\ClientRepository;
use App\Http\Requests\Backend\Clients\ClientRequest;
use App\Http\Requests\Backend\Clients\StoreClientRequest;
use App\Http\Requests\Backend\Clients\UpdateClientRequest;
use App\Repositories\Backend\Access\Role\RoleRepository;
use App\Domains\Auth\Models\User;
use App\Models\MasterData\State;
use App\Models\MasterData\Country;
use App\Models\SecurityQuestion\SecurityQuestion;
use App\Models\Language\Language;
use App\Domains\Auth\Services\PermissionService;
use App\Domains\Auth\Services\RoleService;

/**
 * ClientsController for manage client details
 * @method index,create,edit,update,destroy
 */
class ClientsController extends Controller
{
    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @var RoleService
     */
    protected $roleService;

    /**
     * @var PermissionService
     */
    protected $permissionService;

    /**
     * contructor to initialize repository object
     * @param ClientRepository $repository;
     * @param  UserService  $userService
     * @param  RoleService  $roleService
     * @param  PermissionService  $permissionService
     */
    public function __construct(ClientRepository $repository, RoleService $roleService, PermissionService $permissionService)
    {
        $this->repository = $repository;
        $this->roleService = $roleService;
        $this->permissionService = $permissionService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\Clients\ClientRequest  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ClientRequest $request)
    {
        $countries = Country::active()->pluck('name','id')->toArray();
        $states = State::active()->pluck('name','id')->toArray();
        $securityQuestions = SecurityQuestion::active()->pluck('question','id')->toArray();
        $languages = Language::active()->pluck('name','id')->toArray();
        return view('backend.clients.index')->with([
            'countries' => $countries,
            'states' => $states,
            'languages' => $languages
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  ClientRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Clients\CreateResponse
     */
    public function create(ClientRequest $request)
    {
        $roles = $this->roleService->get();
        $permissions = $this->permissionService->getCategorizedPermissions();
        return new CreateResponse($roles, $permissions);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreClientRequestNamespace  $request
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreClientRequest $request)
    {
        //Create the model using repository create method
        $this->repository->create($request);
        //return with successfull message
        return new RedirectResponse(route('admin.clients.index'), ['flash_success' => trans('alerts.backend.clientdetails.created')]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\Access\User\User  $user
     * @param  EditClientRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Clients\EditResponse
     */
    public function edit(User $client, ClientRequest $request)
    {
        $roles = $this->roleService->get();
        $permissions = $this->permissionService->getCategorizedPermissions();
        return new EditResponse($client, $roles, $permissions);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateClientRequestNamespace  $request
     * @param  App\Models\Access\User\User  $user
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(UpdateClientRequest $request, User $client)
    {
        //Update the model using repository update method
        $this->repository->update( $client , $request);
        //return with successfull message
        return new RedirectResponse(route('admin.clients.index'), ['flash_success' => trans('alerts.backend.clientdetails.updated')]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeleteClientRequestNamespace  $request
     * @param  App\Models\Access\User\User  $client
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(User $client, ClientRequest $request)
    {
        //Calling the delete method on repository
        $this->repository->delete($user);
        //returning with successfull message
        return new RedirectResponse(route('admin.clients.index'), ['flash_success' => trans('alerts.backend.clientdetails.deleted')]);
    }

    /**
     * @param \App\Models\Access\User\User                           $client
     * @param \App\Http\Requests\Backend\Client\ManageClientRequest $request
     *
     * @return \App\Http\Responses\Backend\Access\User\ShowResponse
     */
    public function show(User $client, ClientRequest $request)
    {
        return new ShowResponse($client);
    }
    
}
