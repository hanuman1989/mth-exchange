<?php

namespace App\Http\Controllers\Backend\Clients;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\Clients\ClientRepository;
use App\Http\Requests\Backend\Clients\ClientRequest;

/**
 * ClientsTableController for manage client details
 */
class ClientsTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var ClientRepository
     */
    protected $user;

    /**
     * contructor to initialize repository object
     * @param ClientRepository $user;
     */
    public function __construct(ClientRepository $user)
    {
        $this->user = $user;
    }

    /**
     * This method return the data of the model
     * @param ClientRequest $request
     *
     * @return mixed
     */
    public function __invoke(ClientRequest $request)
    {
        $searchParamsQuery = $request->all();
        $searchParams =  array();
        parse_str($searchParamsQuery['form'], $searchParams);
        return Datatables::of($this->user->getForDataTable($searchParams))
            ->escapeColumns(['id'])
            ->addColumn('created_at', function ($user) {
                return Carbon::parse($user->created_at)->toDateString();
            })
            ->addColumn('status', function ($user) {
                return $user->status_label;
            })
            ->addColumn('actions', function ($user) {
                return $user->client_action_buttons;
            })
            ->make(true);
    }
}
