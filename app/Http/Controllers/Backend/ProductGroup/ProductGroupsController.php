<?php 
    namespace App\Http\Controllers\Backend\ProductGroup;
    use App\Models\ProductGroup\ProductGroup;
	use App\Models\GroupFeature\GroupFeature;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Http\Responses\RedirectResponse;
    use App\Http\Responses\ViewResponse;
    use App\Http\Responses\Backend\ProductGroup\CreateResponse;
    use App\Http\Responses\Backend\ProductGroup\EditResponse;
    use App\Repositories\Backend\ProductGroup\ProductGroupRepository;
    use App\Http\Requests\Backend\ProductGroup\ManageProductGroupRequest;
    use App\Http\Requests\Backend\ProductGroup\StoreProductGroupRequest;
    use App\Http\Requests\Backend\ProductGroup\UpdateProductGroupRequest;
    use App\Http\Requests\Backend\ProductGroup\DeleteProductGroupRequest;
    use App\Models\OrderFormTemplate\OrderFormTemplate;
	use App\Models\PaymentGateway\PaymentGateway;

    /**
     * Class ProductGroupsController
     */
    class ProductGroupsController extends Controller
    {
        /**
         * variable to store the repository object
         * @var ProductGroupRepository
         */
        protected $repository;

        /**
         * contructor to initialize repository object
         * @param ProductGroupRepository $repository;
         */
        public function __construct(ProductGroupRepository $repository)
        {
            $this->repository = $repository;
        }

        /**
         * Display a listing of the resource.
         *
         * @param  App\Http\Requests\Backend\ProductGroup\ManageProductGroupRequest  $request
         * @return \App\Http\Responses\ViewResponse
         */
        public function index(ManageProductGroupRequest $request)
        {
            return new ViewResponse('backend.productgroups.index');
        }
        /**
         * Show the form for creating a new resource.
         *
         * @param  CreateProductGroupRequestNamespace  $request
         * @return \App\Http\Responses\Backend\ProductGroup\CreateResponse
         */
        public function create(Request $request)
        {
    		$orderForms = OrderFormTemplate::active()->get();
			$paymentRecords = PaymentGateway::active()->get();
    		return view('backend.productgroups.create')->with([
    				'orderForms' => $orderForms,
					'paymentRecords'=>$paymentRecords
    			]);
           
           
        }
        /**
         * Store a newly created resource in storage.
         *
         * @param  StoreProductGroupRequestNamespace  $request
         * @return \App\Http\Responses\RedirectResponse
         */
        public function store(StoreProductGroupRequest $request)
        {
			
            //Input received from the request
            $input = $request->except(['_token','payment_gateway_id']);
			
			$paymentGateway = $request->payment_gateway_id;
			
            //Create the model using repository create method
            $this->repository->create($input,$paymentGateway);
            //return with successfull message
            return new RedirectResponse(route('admin.productgroups.index'), ['flash_success' => trans('alerts.backend.productgroups.created')]);
        }
        /**
         * Show the form for editing the specified resource.
         *
         * @param  App\Models\ProductGroup\ProductGroup  $productgroup
         * @param  EditProductGroupRequestNamespace  $request
         * @return \App\Http\Responses\Backend\ProductGroup\EditResponse
         */
        public function edit(ProductGroup $productgroup,Request $request)
        {
            $selectedGroupFeatures = GroupFeature::savedFeature($productgroup->id)->get();
			$orderForms = OrderFormTemplate::active()->get();
			$paymentRecords = PaymentGateway::active()->get();
          
			 return view('backend.productgroups.edit')->with([
    				'orderForms' => $orderForms,
					'productgroups'=>$productgroup,
					'selectedGroupFeatures'=>$selectedGroupFeatures,
					'paymentRecords'=>$paymentRecords
    			]);

        }
		
		/**
         * Show the form for group featuers the specified resource.
         *
         * @param  App\Models\GroupFeature\GroupFeature  $productgroup
         * @param  SaveGroupFeature  $request
         
         */
		
		public function groupFeature(Request $request){
			$groupFeature = new GroupFeature();
	        $groupFeature->name = $request->featureValue;
			 $groupFeature->product_group_id = $request->product_group_id;
		    if($groupFeature->save()){
            return json_encode(['id' => $groupFeature->id, 'status' => 'success']);
                }else{
                   return json_encode(['status' => 'failed']);
                }
			
		}
		
		/**
         * Show the form for delete featuers the specified resource.
         *
         * @param  App\Models\GroupFeature\GroupFeature  $productgroup
         * @param  DeleteGroupFeature  $request
         
         */
		
		public function deleteFeature(Request $request){
		    if(GroupFeature::destroy($request->id)){
            return json_encode(['status' => 'success']);
                }else{
            return json_encode(['status' => 'failed']);
                }
			
		}
		
		
		
		
        /**
         * Update the specified resource in storage.
         *
         * @param  UpdateProductGroupRequestNamespace  $request
         * @param  App\Models\ProductGroup\ProductGroup  $productgroup
         * @return \App\Http\Responses\RedirectResponse
         */
        public function update(UpdateProductGroupRequest $request, ProductGroup $productgroup)
        {
            //Input received from the request
            $input = $request->except(['_token','payment_gateway_id','group_features']);
			$paymentGateway = $request->payment_gateway_id;			
			
            //Update the model using repository update method
            $this->repository->update( $productgroup, $input,$paymentGateway );
            //return with successfull message
            return new RedirectResponse(route('admin.productgroups.index'), ['flash_success' => trans('alerts.backend.productgroups.updated')]);
        }
        /**
         * Remove the specified resource from storage.
         *
         * @param  DeleteProductGroupRequestNamespace  $request
         * @param  App\Models\ProductGroup\ProductGroup  $productgroup
         * @return \App\Http\Responses\RedirectResponse
         */
        public function destroy(ProductGroup $productgroup, DeleteProductGroupRequest $request)
        {
            //Calling the delete method on repository
            $this->repository->delete($productgroup);
            //returning with successfull message
            return new RedirectResponse(route('admin.productgroups.index'), ['flash_success' => trans('alerts.backend.productgroups.deleted')]);
        }
        
    }
