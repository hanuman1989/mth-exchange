<?php
    namespace App\Http\Controllers\Backend\ProductGroup;

    use Carbon\Carbon;
    use App\Http\Controllers\Controller;
    use Yajra\DataTables\Facades\DataTables;
    use App\Repositories\Backend\ProductGroup\ProductGroupRepository;
    use App\Http\Requests\Backend\ProductGroup\ManageProductGroupRequest;

    /**
     * Class ProductGroupsTableController.
     */
    class ProductGroupsTableController extends Controller
    {
        /**
         * variable to store the repository object
         * @var ProductGroupRepository
         */
        protected $productgroup;

        /**
         * contructor to initialize repository object
         * @param ProductGroupRepository $productgroup;
         */
        public function __construct(ProductGroupRepository $productgroup)
        {
            $this->productgroup = $productgroup;
        }

        /**
         * This method return the data of the model
         * @param ManageProductGroupRequest $request
         *
         * @return mixed
         */
        public function __invoke(ManageProductGroupRequest $request)
        {
            return Datatables::of($this->productgroup->getForDataTable())
                ->escapeColumns(['id'])
                ->addColumn('created_at', function ($productgroup) {
                    return Carbon::parse($productgroup->created_at)->toDateString();
                })
                ->addColumn('actions', function ($productgroup) {
                    return $productgroup->action_buttons;
                })
                ->make(true);
        }
    }
