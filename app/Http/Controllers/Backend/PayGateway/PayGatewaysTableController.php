<?php

namespace App\Http\Controllers\Backend\PayGateway;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\PayGateway\PayGatewayRepository;
use App\Http\Requests\Backend\PayGateway\ManagePayGatewayRequest;

/**
 * Class PayGatewaysTableController.
 */
class PayGatewaysTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var PayGatewayRepository
     */
    protected $paygateway;

    /**
     * contructor to initialize repository object
     * @param PayGatewayRepository $paygateway;
     */
    public function __construct(PayGatewayRepository $paygateway)
    {
        $this->paygateway = $paygateway;
    }

    /**
     * This method return the data of the model
     * @param ManagePayGatewayRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManagePayGatewayRequest $request)
    {
        return Datatables::of($this->paygateway->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('created_at', function ($paygateway) {
                return Carbon::parse($paygateway->created_at)->toDateString();
            })
            ->addColumn('actions', function ($paygateway) {
                return $paygateway->action_buttons;
            })
            ->make(true);
    }
}
