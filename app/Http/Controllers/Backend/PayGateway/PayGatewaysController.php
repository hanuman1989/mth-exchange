<?php

namespace App\Http\Controllers\Backend\PayGateway;

use App\Models\PayGateway\PayGateway;
use App\Models\PayGateway\PayGatewayField;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\PayGateway\CreateResponse;
use App\Http\Responses\Backend\PayGateway\EditResponse;
use App\Repositories\Backend\PayGateway\PayGatewayRepository;
use App\Http\Requests\Backend\PayGateway\ManagePayGatewayRequest;
use App\Http\Requests\Backend\PayGateway\StorePayGatewayRequest;
use App\Http\Requests\Backend\PayGateway\UpdatePayGatewayRequest;
use Symfony\Component\Yaml\Yaml;
use DB;

/**
 * PayGatewaysController to mange differnt paymentgateway in admin settings
 */
class PayGatewaysController extends Controller
{
    /**
     * variable to store the repository object
     * @var PayGatewayRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param PayGatewayRepository $repository;
     */
    public function __construct(PayGatewayRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\PayGateway\ManagePayGatewayRequest  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManagePayGatewayRequest $request)
    {
        return new ViewResponse('backend.paygateways.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreatePayGatewayRequestNamespace  $request
     * @return \App\Http\Responses\Backend\PayGateway\CreateResponse
     */
    public function create(Request $request)
    {   
        
        return new CreateResponse('backend.paygateways.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StorePayGatewayRequestNamespace  $request
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StorePayGatewayRequest $request)
    {
        $input = $request->input();
        $input['status']=isset($input['status'])? 1:0;
        $pay_gateway = PayGateway::create($input);

            if($request->only(['fields'])){
            $fieldRequest = [];
            $data = $request->only(['fields']);

            foreach ($data['fields'] as $field) {
            $fieldRequest[] = new PayGatewayField($field);
            }

            $pay_gateway->fields()->saveMany($fieldRequest);
            }


        
        //return with successfull message
        return new RedirectResponse(route('admin.paygateways.index'), ['flash_success' => trans('alerts.backend.paygateways.created')]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\PayGateway\PayGateway  $paygateway
     * @param  EditPayGatewayRequestNamespace  $request
     * @return \App\Http\Responses\Backend\PayGateway\EditResponse
     */
    public function edit(PayGateway $paygateway, Request $request)
    {
        return new EditResponse($paygateway);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdatePayGatewayRequestNamespace  $request
     * @param  App\Models\PayGateway\PayGateway  $paygateway
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(UpdatePayGatewayRequest $request, PayGateway $paygateway)
    {
        //Input received from the request
        $input = $request->input();
        $input['status']=isset($input['status'])? 1:0;
        $pay_gateways_id=$input['pay_gateways_id'];
        
        foreach ($input as $key => $value) {
            PayGatewayField::where('pay_gateways_id', $pay_gateways_id)->where('id', $key)->update(["field_value"=>$value,]);
        }
        $this->repository->update($paygateway,$input);
        
        
        //return with successfull message
        return new RedirectResponse(route('admin.paygateways.index'), ['flash_success' => trans('alerts.backend.paygateways.updated')]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeletePayGatewayRequestNamespace  $request
     * @param  App\Models\PayGateway\PayGateway  $paygateway
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(PayGateway $paygateway, Request $request)
    {
        //Calling the delete method on repository
        $this->repository->delete($paygateway);
        //returning with successfull message
        return new RedirectResponse(route('admin.paygateways.index'), ['flash_success' => trans('alerts.backend.paygateways.deleted')]);
    }
    
}
