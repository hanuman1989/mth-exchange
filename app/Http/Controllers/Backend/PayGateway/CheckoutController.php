<?php

namespace App\Http\Controllers\Backend\PayGateway;

use Exception;
use Illuminate\Http\Response;
use Stripe\PaymentIntent;
use Stripe\Stripe;
use App\Http\Controllers\Controller;

class CheckoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('backend.paygateways.checkout');
    }

    public function paymentItents()
    {  /*Amount last two digit represent decimal for egs:20.00 as 2000*/
        try {
            Stripe::setApiKey(env('STRIPE_SECRET'));
            $intent = PaymentIntent::create([
                'amount' => 2000,
                'currency' => env("STRIPE_CURRENCY"),
            ]);
            ob_start();
            echo("New Order: " . $intent->id);
            error_log(ob_get_clean(), 4);
            return response()->json($intent);
        } catch (Exception $e) {
            return back()->withErrors('Error! ' . $e->getMessage());
        }
    }
}
