<?php

namespace App\Http\Controllers\Backend\TaxRules;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\TaxRules\TaxRuleRepository;
use App\Http\Requests\Backend\TaxRules\ManageTaxRuleRequest;

/**
 * Class TaxRulesTableController.
 */
class TaxRulesTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var TaxRuleRepository
     */
    protected $taxrule;

    /**
     * contructor to initialize repository object
     * @param TaxRuleRepository $taxrule;
     */
    public function __construct(TaxRuleRepository $taxrule)
    {
        $this->taxrule = $taxrule;
    }

    /**
     * This method return the data of the model
     * @param ManageTaxRuleRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageTaxRuleRequest $request)
    {
        return Datatables::of($this->taxrule->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('created_at', function ($taxrule) {
                return Carbon::parse($taxrule->created_at)->toDateString();
            })
            ->addColumn('status', function ($taxrule) {
                return $taxrule->status_label;
            })
            ->addColumn('country_id', function ($taxrule) {
                return $taxrule->country->name ?? 'NA';
            })
            ->addColumn('actions', function ($taxrule) {
                return $taxrule->action_buttons;
            })
            ->make(true);
    }
}
