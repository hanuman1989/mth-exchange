<?php

namespace App\Http\Controllers\Backend\TaxRules;

use App\Models\TaxRules\TaxRule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\TaxRules\CreateResponse;
use App\Http\Responses\Backend\TaxRules\EditResponse;
use App\Repositories\Backend\TaxRules\TaxRuleRepository;
use App\Http\Requests\Backend\TaxRules\ManageTaxRuleRequest;
use App\Http\Requests\Backend\TaxRules\StoreTaxRuleRequest;
use App\Http\Requests\Backend\TaxRules\UpdateTaxRuleRequest;

/**
 * TaxRulesController
 */
class TaxRulesController extends Controller
{
    /**
     * variable to store the repository object
     * @var TaxRuleRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param TaxRuleRepository $repository;
     */
    public function __construct(TaxRuleRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\TaxRules\ManageTaxRuleRequest  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManageTaxRuleRequest $request)
    {
        return new ViewResponse('backend.taxrules.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreateTaxRuleRequestNamespace  $request
     * @return \App\Http\Responses\Backend\TaxRules\CreateResponse
     */
    public function create(ManageTaxRuleRequest $request)
    {
        return new CreateResponse('backend.taxrules.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreTaxRuleRequestNamespace  $request
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreTaxRuleRequest $request)
    {
        //Create the model using repository create method
        $this->repository->create($request->except(['_token']));
        //return with successfull message
        return new RedirectResponse(route('admin.taxrules.index'), ['flash_success' => trans('alerts.backend.taxrules.created')]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\TaxRules\TaxRule  $taxrule
     * @param  ManageTaxRuleRequestNamespace  $request
     * @return \App\Http\Responses\Backend\TaxRules\EditResponse
     */
    public function edit(TaxRule $taxrule, ManageTaxRuleRequest $request)
    {
        return new EditResponse($taxrule);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateTaxRuleRequestNamespace  $request
     * @param  App\Models\TaxRules\TaxRule  $taxrule
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(UpdateTaxRuleRequest $request, TaxRule $taxrule)
    {
        //Update the model using repository update method
        $this->repository->update( $taxrule, $request->except(['_token']) );
        //return with successfull message
        return new RedirectResponse(route('admin.taxrules.index'), ['flash_success' => trans('alerts.backend.taxrules.updated')]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  ManageTaxRuleRequestNamespace  $request
     * @param  App\Models\TaxRules\TaxRule  $taxrule
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(TaxRule $taxrule, ManageTaxRuleRequest $request)
    {
        //Calling the delete method on repository
        $this->repository->delete($taxrule);
        //returning with successfull message
        return new RedirectResponse(route('admin.taxrules.index'), ['flash_success' => trans('alerts.backend.taxrules.deleted')]);
    }
    
}
