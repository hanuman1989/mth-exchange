<?php

namespace App\Http\Controllers\Backend\SecurityQuestion;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\SecurityQuestion\SecurityQuestionRepository;
use App\Http\Requests\Backend\SecurityQuestion\SecurityQuestionRequest;

/**
 * Class SecurityQuestionsTableController for manage Security Questions.
 */
class SecurityQuestionsTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var SecurityQuestionRepository
     */
    protected $securityquestion;

    /**
     * contructor to initialize repository object
     * @param SecurityQuestionRepository $securityquestion;
     */
    public function __construct(SecurityQuestionRepository $securityquestion)
    {
        $this->securityquestion = $securityquestion;
    }

    /**
     * This method return the data of the model
     * @param SecurityQuestionRequest $request
     *
     * @return mixed
     */
    public function __invoke(SecurityQuestionRequest $request)
    {
        return Datatables::of($this->securityquestion->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('created_at', function ($securityquestion) {
                return Carbon::parse($securityquestion->created_at)->toDateString();
            })
            ->addColumn('status', function ($securityquestion) {
                return $securityquestion->status_label;
            })
            ->addColumn('actions', function ($securityquestion) {
                return $securityquestion->action_buttons;
            })
            ->make(true);
    }
}
