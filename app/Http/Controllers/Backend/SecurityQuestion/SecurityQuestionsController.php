<?php

namespace App\Http\Controllers\Backend\SecurityQuestion;

use App\Models\SecurityQuestion\SecurityQuestion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\SecurityQuestion\CreateResponse;
use App\Http\Responses\Backend\SecurityQuestion\EditResponse;
use App\Repositories\Backend\SecurityQuestion\SecurityQuestionRepository;
use App\Http\Requests\Backend\SecurityQuestion\SecurityQuestionRequest;
use App\Http\Requests\Backend\SecurityQuestion\CreateSecurityQuestionRequest;
use App\Http\Requests\Backend\SecurityQuestion\StoreSecurityQuestionRequest;
use App\Http\Requests\Backend\SecurityQuestion\EditSecurityQuestionRequest;
use App\Http\Requests\Backend\SecurityQuestion\UpdateSecurityQuestionRequest;
use App\Http\Requests\Backend\SecurityQuestion\DeleteSecurityQuestionRequest;

/**
 * SecurityQuestionsController for manage Security Questions
 * @method index,create,edit,update,destroy
 */
class SecurityQuestionsController extends Controller
{
    /**
     * variable to store the repository object
     * @var SecurityQuestionRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param SecurityQuestionRepository $repository;
     */
    public function __construct(SecurityQuestionRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\SecurityQuestion\SecurityQuestionRequest  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(SecurityQuestionRequest $request)
    {
        return new ViewResponse('backend.securityquestions.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreateSecurityQuestionRequestNamespace  $request
     * @return \App\Http\Responses\Backend\SecurityQuestion\CreateResponse
     */
    public function create(CreateSecurityQuestionRequest $request)
    {
        return new CreateResponse('backend.securityquestions.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreSecurityQuestionRequestNamespace  $request
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreSecurityQuestionRequest $request)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Create the model using repository create method
        $this->repository->create($input);
        //return with successfull message
        return new RedirectResponse(route('admin.securityquestions.index'), ['flash_success' => trans('alerts.backend.securityquestions.created')]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\SecurityQuestion\SecurityQuestion  $securityquestion
     * @param  EditSecurityQuestionRequestNamespace  $request
     * @return \App\Http\Responses\Backend\SecurityQuestion\EditResponse
     */
    public function edit(SecurityQuestion $securityquestion, EditSecurityQuestionRequest $request)
    {
        return new EditResponse($securityquestion);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateSecurityQuestionRequestNamespace  $request
     * @param  App\Models\SecurityQuestion\SecurityQuestion  $securityquestion
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(UpdateSecurityQuestionRequest $request, SecurityQuestion $securityquestion)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Update the model using repository update method
        $this->repository->update( $securityquestion, $input );
        //return with successfull message
        return new RedirectResponse(route('admin.securityquestions.index'), ['flash_success' => trans('alerts.backend.securityquestions.updated')]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeleteSecurityQuestionRequestNamespace  $request
     * @param  App\Models\SecurityQuestion\SecurityQuestion  $securityquestion
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(SecurityQuestion $securityquestion, DeleteSecurityQuestionRequest $request)
    {
        //Calling the delete method on repository
        $this->repository->delete($securityquestion);
        //returning with successfull message
        return new RedirectResponse(route('admin.securityquestions.index'), ['flash_success' => trans('alerts.backend.securityquestions.deleted')]);
    }
    
}
