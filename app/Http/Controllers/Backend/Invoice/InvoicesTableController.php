<?php

namespace App\Http\Controllers\Backend\Invoice;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\Invoice\InvoiceRepository;
use App\Http\Requests\Backend\Invoice\ManageInvoiceRequest;

/**
 * Class InvoicesTableController.
 */
class InvoicesTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var InvoiceRepository
     */
    protected $invoice;

    /**
     * contructor to initialize repository object
     * @param InvoiceRepository $invoice;
     */
    public function __construct(InvoiceRepository $invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * This method return the data of the model
     * @param ManageInvoiceRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageInvoiceRequest $request)
    {
        //get client id to get invoices of one client
        $params = $request->all();
        return Datatables::of($this->invoice->getForDataTable($params['client_id']))
            ->escapeColumns(['id'])
            ->addColumn('created_at', function ($invoice) {
                return Carbon::parse($invoice->created_at)->toDateString();
            })
            ->addColumn('actions', function ($invoice) {
                return $invoice->action_buttons;
            })
            ->make(true);
    }
}
