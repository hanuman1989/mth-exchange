<?php

namespace App\Http\Controllers\Backend\Invoice;

use App\Models\Invoice\Invoice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\Invoice\CreateResponse;
use App\Http\Responses\Backend\Invoice\EditResponse;
use App\Repositories\Backend\Invoice\InvoiceRepository;
use App\Http\Requests\Backend\Invoice\ManageInvoiceRequest;
use App\Http\Requests\Backend\Invoice\CreateInvoiceRequest;
use App\Http\Requests\Backend\Invoice\StoreInvoiceRequest;
use App\Http\Requests\Backend\Invoice\EditInvoiceRequest;
use App\Http\Requests\Backend\Invoice\UpdateInvoiceRequest;
use App\Http\Requests\Backend\Invoice\DeleteInvoiceRequest;
use App\Domains\Auth\Models\User;

/**
 * InvoicesController
 */
class InvoicesController extends Controller
{
    /**
     * variable to store the repository object
     * @var InvoiceRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param InvoiceRepository $repository;
     */
    public function __construct(InvoiceRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\Invoice\ManageInvoiceRequest  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManageInvoiceRequest $request)
    {
        $client = User::whereId(app('request')->query('client_id'))->first();
        return view('backend.invoices.index', compact('client'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreateInvoiceRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Invoice\CreateResponse
     */
    public function create(CreateInvoiceRequest $request)
    {
        return new CreateResponse('backend.invoices.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreInvoiceRequestNamespace  $request
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreInvoiceRequest $request)
    {
        //Create the model using repository create method
        $this->repository->create($request->except(['_token']));
        //return with successfull message
        return new RedirectResponse(route('admin.invoices.index', ['client_id' => $request['user_id']]), ['flash_success' => trans('alerts.backend.invoices.created')]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\Invoice\Invoice  $invoice
     * @param  EditInvoiceRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Invoice\EditResponse
     */
    public function edit(Invoice $invoice, EditInvoiceRequest $request)
    {
        return new EditResponse($invoice);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateInvoiceRequestNamespace  $request
     * @param  App\Models\Invoice\Invoice  $invoice
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(UpdateInvoiceRequest $request, Invoice $invoice)
    {
        //Update the model using repository update method
        $this->repository->update( $invoice, $request->except(['_token']) );
        //return with successfull message
        return new RedirectResponse(route('admin.invoices.index'), ['flash_success' => trans('alerts.backend.invoices.updated')]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeleteInvoiceRequestNamespace  $request
     * @param  App\Models\Invoice\Invoice  $invoice
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(Invoice $invoice, DeleteInvoiceRequest $request)
    {
        $this->repository->delete($invoice);
        //returning with successfull message
        return new RedirectResponse(route('admin.invoices.index'), ['flash_success' => trans('alerts.backend.invoices.deleted')]);
    }
    
}
