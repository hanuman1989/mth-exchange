<?php

namespace App\Http\Controllers\Backend\ClientLogs;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\ClientLogs\ActivityRepository;
use App\Http\Requests\Backend\ClientLogs\ActivityRequest;

/**
 * Class ActivitiesTableController for manage activities logs.
 */
class ActivitiesTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var ActivityRepository
     */
    protected $activity;

    /**
     * contructor to initialize repository object
     * @param ActivityRepository $activity;
     */
    public function __construct(ActivityRepository $activity)
    {
        $this->activity = $activity;
    }

    /**
     * This method return the data of the model
     * @param ActivityRequest $request
     *
     * @return mixed
     */
    public function __invoke(ActivityRequest $request)
    {
        return Datatables::of($this->activity->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('causer_id', function ($activity) {
                return $activity->user_name;
            })
            ->addColumn('properties', function ($activity) {
                $strs= (string)$activity->properties;
                return wordwrap($strs,40,"\n");
            })
            ->addColumn('created_at', function ($activity) {
                return Carbon::parse($activity->created_at)->toDateString();
            })
            ->addColumn('actions', function ($activity) {
                return $activity->action_buttons;
            })
            ->make(true);
    }

}
