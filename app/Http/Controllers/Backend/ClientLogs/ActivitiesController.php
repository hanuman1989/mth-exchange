<?php

namespace App\Http\Controllers\Backend\ClientLogs;

use App\Models\Activity\Activity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\ClientLogs\CreateResponse;
use App\Http\Responses\Backend\ClientLogs\EditResponse;
use App\Repositories\Backend\ClientLogs\ActivityRepository;
use App\Http\Requests\Backend\ClientLogs\ActivityRequest;

/**
 * ActivitiesController for manage activity logs
 * @method index,destroy
 */
class ActivitiesController extends Controller
{
    /**
     * variable to store the repository object
     * @var ActivityRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param ActivityRepository $repository;
     */
    public function __construct(ActivityRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\ClientLogs\ActivityRequest  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ActivityRequest $request)
    {
        return new ViewResponse('backend.clientlogs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  ActivityRequest  $request
     * @param  App\Models\Activity\Activity  $activity
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(Activity $activities, ActivityRequest $request)
    {
        //Calling the delete method on repository
        $activities->delete();
        //returning with successfull message
        return new RedirectResponse(route('admin.activities.index'), ['flash_success' => trans('Client log has been successfully deleted.')]);
    }
    
}
