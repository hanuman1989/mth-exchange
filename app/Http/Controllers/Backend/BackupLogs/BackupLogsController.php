<?php

namespace App\Http\Controllers\Backend\BackupLogs;

use App\Models\SystemBackup\SystemBackup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Repositories\Backend\BackupLogs\BackupLogsRepository;
use App\Http\Requests\Backend\BackupLogs\BackupLogsRequest;

/**
 * BackupLogsController for manage system backup and db backup logs
 * @method index
 */
class BackupLogsController extends Controller
{
    /**
     * variable to store the repository object
     * @var BackupLogsRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param BackupLogsRepository $repository;
     */
    public function __construct(BackupLogsRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\BackupLogs\BackupLogsRequest  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(BackupLogsRequest $request)
    {
        return new ViewResponse('backend.backuplogs.index');
    }
}
