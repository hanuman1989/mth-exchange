<?php

namespace App\Http\Controllers\Backend\BackupLogs;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\BackupLogs\BackupLogsRepository;
use App\Http\Requests\Backend\BackupLogs\BackupLogsRequest;

/**
 * Class BackupLogsTableController for manage activities logs.
 */
class BackupLogsTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var BackupLogsRepository
     */
    protected $backupLogs;

    /**
     * contructor to initialize repository object
     * @param BackupLogsRepository $activity;
     */
    public function __construct(BackupLogsRepository $backupLogs)
    {
        $this->backupLogs = $backupLogs;
    }

    /**
     * This method return the data of the model
     * @param ActivityRequest $request
     *
     * @return mixed
     */
    public function __invoke(BackupLogsRequest $request)
    {
        return Datatables::of($this->backupLogs->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('file', function ($backupLogs) {
                $getSpliitedPath = explode("\\", $backupLogs->file_path);
                $fileName = $getSpliitedPath[count($getSpliitedPath) - 1];
                return '<a>'.$fileName.'</a>';
            })
            ->addColumn('created_at', function ($backupLogs) {
                return Carbon::parse($backupLogs->created_at)->toDateString();
            })
            ->make(true);
    }

}
