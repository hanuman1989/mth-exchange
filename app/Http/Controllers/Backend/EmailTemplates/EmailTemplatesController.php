<?php

namespace App\Http\Controllers\Backend\EmailTemplates;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\EmailTemplates\StoreEmailTemplateRequest;
use App\Http\Requests\Backend\EmailTemplates\UpdateEmailTemplateRequest;
use App\Http\Responses\Backend\EmailTemplate\EditResponse;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Models\EmailTemplates\EmailTemplate;
use App\Repositories\Backend\EmailTemplates\EmailTemplateRepository;

/**
 * Class EmailTemplatesController to manage Email Template Crud Operation.
 */
class EmailTemplatesController extends Controller
{
    protected $emails;

    /**
     * @param \App\Repositories\Backend\EmailTemplates\EmailTemplateRepository $emails
     */
    public function __construct(EmailTemplateRepository $emails)
    {
        $this->emails = $emails;
    }

    /**
     * 
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function index()
    {
        return new ViewResponse('backend.emailtemplates.index');
    }

    /**
     * @return \App\Http\Responses\ViewResponse
     */
    public function create(Request $request)
    {
        return new ViewResponse('backend.emailtemplates.create');
    }

    /**
     * @param \App\Http\Requests\Backend\EmailTemplates\StoreEmailTemplateRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreEmailTemplateRequest $request)
    {
        $this->emails->create($request->input());

        return new RedirectResponse(route('admin.email.index'), ['flash_success' => trans('alerts.backend.emailtemplates.created')]);
    }

    /**
     * @param \App\Models\EmailTemplates\EmailTemplate   $email
     
     *
     * @return \App\Http\Responses\Backend\EmailTemplate\EditResponse
     */
    public function edit(EmailTemplate $email, Request $request)
    {   
        
        return new EditResponse($email);
    }

    /**
     * @param \App\Models\EmailTemplates\EmailTemplate    $email
     * @param \App\Http\Requests\Backend\EmailTemplates\UpdateEmailTemplateRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(EmailTemplate $email, UpdateEmailTemplateRequest $request)
    {
        $this->emails->update($email, $request->input());

        return new RedirectResponse(route('admin.email.index'), ['flash_success' => trans('alerts.backend.emailtemplates.updated')]);
    }

    /**
     * @param \App\Models\EmailTemplates\EmailTemplate    $email
     *
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(EmailTemplate $email, Request $request)
    {
        $this->emails->delete($email);

        return new RedirectResponse(route('admin.email.index'), ['flash_success' => trans('alerts.backend.emailtemplates.deleted')]);
    }
}
