<?php

namespace App\Http\Controllers\Backend\EmailTemplates;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\EmailTemplates\EmailTemplateRepository;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class EmailTemplatesTableController.
 */
class EmailTemplatesTableController extends Controller
{
    protected $emails;

    /**
     * @param EmailTemplateRepository $emails
     */
    public function __construct(EmailTemplateRepository $emails)
    {
        $this->emails = $emails;
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function __invoke(Request $request)
    {
        return Datatables::of($this->emails->getForDataTable())
            ->escapeColumns(['title'])
            ->addColumn('status', function ($email) {
                return $email->status_label;
            })
            ->addColumn('created_at', function ($email) {
                return $email->created_at->toDateString();
            })
            ->addColumn('created_by', function ($email) {
                return $email->created_by;
            })
            ->addColumn('actions', function ($email) {
                return $email->action_buttons;
            })
            ->make(true);
    }
}
