<?php

namespace App\Http\Controllers\Backend\Notes;

use App\Models\Notes\Note;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\Notes\CreateResponse;
use App\Http\Responses\Backend\Notes\EditResponse;
use App\Repositories\Backend\Notes\NoteRepository;
use App\Http\Requests\Backend\Notes\ManageNoteRequest;
use App\Http\Requests\Backend\Notes\CreateNoteRequest;
use App\Http\Requests\Backend\Notes\StoreNoteRequest;
use App\Http\Requests\Backend\Notes\EditNoteRequest;
use App\Http\Requests\Backend\Notes\UpdateNoteRequest;
use App\Http\Requests\Backend\Notes\DeleteNoteRequest;
use App\Domains\Auth\Models\User;

/**
 * NotesController to manage notes add on client
 */
class NotesController extends Controller
{
    /**
     * variable to store the repository object
     * @var NoteRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param NoteRepository $repository;
     */
    public function __construct(NoteRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\Notes\ManageNoteRequest  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManageNoteRequest $request)
    {
        $client = User::whereId(app('request')->query('client_id'))->first();
        return view('backend.notes.index')->withClient($client);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreateNoteRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Notes\CreateResponse
     */
    public function create(CreateNoteRequest $request)
    {
        $client = User::whereId(app('request')->query('client_id'))->first();
        return view('backend.notes.create')->withClient($client);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreNoteRequestNamespace  $request
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreNoteRequest $request)
    {
        //Create the model using repository create method
        $this->repository->create($request->except(['_token']));
        //return with successfull message
        return new RedirectResponse(route('admin.notes.index',['client_id' => $request['user_id']]), ['flash_success' => trans('alerts.backend.notes.created')]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\Notes\Note  $note
     * @param  EditNoteRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Notes\EditResponse
     */
    public function edit(Note $note, EditNoteRequest $request)
    {
        $client = User::whereId($note->user_id)->first();
        return view('backend.notes.edit')
                ->withNotes($note)
                ->withClient($client);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateNoteRequestNamespace  $request
     * @param  App\Models\Notes\Note  $note
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(UpdateNoteRequest $request, Note $note)
    {
        //Update the model using repository update method
        $this->repository->update( $note, $request->except(['_token']) );
        //return with successfull message
        return new RedirectResponse(route('admin.notes.index', ['client_id' => $note->user_id]), ['flash_success' => trans('alerts.backend.notes.updated')]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeleteNoteRequestNamespace  $request
     * @param  App\Models\Notes\Note  $note
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(Note $note, DeleteNoteRequest $request)
    {
        $client_id = $note->user_id;
        //Calling the delete method on repository
        $this->repository->delete($note);
        //returning with successfull message
        return new RedirectResponse(route('admin.notes.index', ['client_id' => $client_id]), ['flash_success' => trans('alerts.backend.notes.deleted')]);
    }
    
}
