<?php

namespace App\Http\Controllers\Backend\Notes;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\Notes\NoteRepository;
use App\Http\Requests\Backend\Notes\ManageNoteRequest;

/**
 * Class NotesTableController.
 */
class NotesTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var NoteRepository
     */
    protected $note;

    /**
     * contructor to initialize repository object
     * @param NoteRepository $note;
     */
    public function __construct(NoteRepository $note)
    {
        $this->note = $note;
    }

    /**
     * This method return the data of the model
     * @param ManageNoteRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageNoteRequest $request)
    {
        return Datatables::of($this->note->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('created_at', function ($note) {
                return Carbon::parse($note->created_at)->toDateString();
            })
            ->addColumn('actions', function ($note) {
                return $note->action_buttons;
            })
            ->make(true);
    }
}
