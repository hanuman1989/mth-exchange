<?php

namespace App\Http\Controllers\Backend\PaymentType;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\PaymentType\PaymentTypeRepository;
use App\Http\Requests\Backend\PaymentType\ManagePaymentTypeRequest;

/**
 * Class PaymentTypesTableController.
 */
class PaymentTypesTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var PaymentTypeRepository
     */
    protected $paymenttype;

    /**
     * contructor to initialize repository object
     * @param PaymentTypeRepository $paymenttype;
     */
    public function __construct(PaymentTypeRepository $paymenttype)
    {
        $this->paymenttype = $paymenttype;
    }

    /**
     * This method return the data of the model
     * @param ManagePaymentTypeRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManagePaymentTypeRequest $request)
    {
        return Datatables::of($this->paymenttype->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('created_at', function ($paymenttype) {
                return Carbon::parse($paymenttype->created_at)->toDateString();
            })
            ->addColumn('actions', function ($paymenttype) {
                return $paymenttype->action_buttons;
            })
            ->make(true);
    }
}
