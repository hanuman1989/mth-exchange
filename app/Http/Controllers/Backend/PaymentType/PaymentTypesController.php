<?php

namespace App\Http\Controllers\Backend\PaymentType;

use App\Models\PaymentType\PaymentType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\PaymentType\CreateResponse;
use App\Http\Responses\Backend\PaymentType\EditResponse;
use App\Repositories\Backend\PaymentType\PaymentTypeRepository;
use App\Http\Requests\Backend\PaymentType\ManagePaymentTypeRequest;
use App\Http\Requests\Backend\PaymentType\StorePaymentTypeRequest;
use App\Http\Requests\Backend\PaymentType\UpdatePaymentTypeRequest;
use App\Http\Requests\Backend\PaymentType\DeletePaymentTypeRequest;

/**
 * Class PaymentTypesController
 * types of payment saving
 */
class PaymentTypesController extends Controller
{
    /**
     * variable to store the repository object
     * @var PaymentTypeRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param PaymentTypeRepository $repository;
     */
    public function __construct(PaymentTypeRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\PaymentType\ManagePaymentTypeRequest  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManagePaymentTypeRequest $request)
    {
        return new ViewResponse('backend.paymenttypes.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreatePaymentTypeRequestNamespace  $request
     * @return \App\Http\Responses\Backend\PaymentType\CreateResponse
     */
    public function create(Request $request)
    {
        return new CreateResponse('backend.paymenttypes.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StorePaymentTypeRequestNamespace  $request
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StorePaymentTypeRequest $request)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Create the model using repository create method
        $this->repository->create($input);
        //return with successfull message
        return new RedirectResponse(route('admin.paymenttypes.index'), ['flash_success' => trans('alerts.backend.paymenttypes.created')]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\PaymentType\PaymentType  $paymenttype
     * @param  EditPaymentTypeRequestNamespace  $request
     * @return \App\Http\Responses\Backend\PaymentType\EditResponse
     */
    public function edit(PaymentType $paymenttype,Request $request)
    {
		
        return new EditResponse($paymenttype);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdatePaymentTypeRequestNamespace  $request
     * @param  App\Models\PaymentType\PaymentType  $paymenttype
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(UpdatePaymentTypeRequest $request, PaymentType $paymenttype)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Update the model using repository update method
        $this->repository->update( $paymenttype, $input );
        //return with successfull message
        return new RedirectResponse(route('admin.paymenttypes.index'), ['flash_success' => trans('alerts.backend.paymenttypes.updated')]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeletePaymentTypeRequestNamespace  $request
     * @param  App\Models\PaymentType\PaymentType  $paymenttype
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(PaymentType $paymenttype, DeletePaymentTypeRequest $request)
    {
        //Calling the delete method on repository
        $this->repository->delete($paymenttype);
        //returning with successfull message
        return new RedirectResponse(route('admin.paymenttypes.index'), ['flash_success' => trans('alerts.backend.paymenttypes.deleted')]);
    }
    
}
