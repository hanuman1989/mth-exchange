<?php

namespace App\Http\Controllers\Backend\TicketPriorty;

use App\Models\TicketPriorty\TicketPriorty;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\TicketPriorty\CreateResponse;
use App\Http\Responses\Backend\TicketPriorty\EditResponse;
use App\Repositories\Backend\TicketPriorty\TicketPriortyRepository;
use App\Http\Requests\Backend\TicketPriorty\ManageTicketPriortyRequest;
use App\Http\Requests\Backend\TicketPriorty\StoreTicketPriortyRequest;
use App\Http\Requests\Backend\TicketPriorty\UpdateTicketPriortyRequest;
use App\Http\Requests\Backend\TicketPriorty\DeleteTicketPriortyRequest;

/**
 * TicketPriortiesController
 */
class TicketPriortiesController extends Controller
{
    /**
     * variable to store the repository object
     * @var TicketPriortyRepository
     */
    protected $repository;
     
	
	
    /**
     * contructor to initialize repository object
     * @param TicketPriortyRepository $repository;
     */
    public function __construct(TicketPriortyRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\TicketPriorty\ManageTicketPriortyRequest  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManageTicketPriortyRequest $request)
    {
        return new ViewResponse('backend.ticketpriorties.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreateTicketPriortyRequestNamespace  $request
     * @return \App\Http\Responses\Backend\TicketPriorty\CreateResponse
     */
    public function create(Request $request)
    {
        return new CreateResponse('backend.ticketpriorties.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreTicketPriortyRequestNamespace  $request
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreTicketPriortyRequest $request)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Create the model using repository create method
        $this->repository->create($input);
        //return with successfull message
        return new RedirectResponse(route('admin.ticketpriorties.index'), ['flash_success' => trans('alerts.backend.ticketpriorties.created')]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\TicketPriorty\TicketPriorty  $ticketpriorty
     * @param  EditTicketPriortyRequestNamespace  $request
     * @return \App\Http\Responses\Backend\TicketPriorty\EditResponse
     */
    public function edit(TicketPriorty $ticketpriorty, Request $request)
    {
        return new EditResponse($ticketpriorty);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateTicketPriortyRequestNamespace  $request
     * @param  App\Models\TicketPriorty\TicketPriorty  $ticketpriorty
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(UpdateTicketPriortyRequest $request, TicketPriorty $ticketpriorty)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Update the model using repository update method
        $this->repository->update( $ticketpriorty, $input );
        //return with successfull message
        return new RedirectResponse(route('admin.ticketpriorties.index'), ['flash_success' => trans('alerts.backend.ticketpriorties.updated')]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeleteTicketPriortyRequestNamespace  $request
     * @param  App\Models\TicketPriorty\TicketPriorty  $ticketpriorty
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(TicketPriorty $ticketpriorty, DeleteTicketPriortyRequest $request)
    {
        //Calling the delete method on repository
        $this->repository->delete($ticketpriorty);
        //returning with successfull message
        return new RedirectResponse(route('admin.ticketpriorties.index'), ['flash_success' => trans('alerts.backend.ticketpriorties.deleted')]);
    }
    
}
