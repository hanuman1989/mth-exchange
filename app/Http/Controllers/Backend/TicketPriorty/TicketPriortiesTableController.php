<?php

namespace App\Http\Controllers\Backend\TicketPriorty;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\TicketPriorty\TicketPriortyRepository;
use App\Http\Requests\Backend\TicketPriorty\ManageTicketPriortyRequest;

/**
 * Class TicketPriortiesTableController.
 */
class TicketPriortiesTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var TicketPriortyRepository
     */
    protected $ticketpriorty;

    /**
     * contructor to initialize repository object
     * @param TicketPriortyRepository $ticketpriorty;
     */
    public function __construct(TicketPriortyRepository $ticketpriorty)
    {
        $this->ticketpriorty = $ticketpriorty;
    }

    /**
     * This method return the data of the model
     * @param ManageTicketPriortyRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageTicketPriortyRequest $request)
    {
        return Datatables::of($this->ticketpriorty->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('created_at', function ($ticketpriorty) {
                return Carbon::parse($ticketpriorty->created_at)->toDateString();
            })
            ->addColumn('actions', function ($ticketpriorty) {
                return $ticketpriorty->action_buttons;
            })
            ->make(true);
    }
}
