<?php

namespace App\Http\Controllers\Backend\PaymentGateway;

use App\Models\PaymentGateway\PaymentGateway;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\PaymentGateway\CreateResponse;
use App\Http\Responses\Backend\PaymentGateway\EditResponse;
use App\Repositories\Backend\PaymentGateway\PaymentGatewayRepository;
use App\Http\Requests\Backend\PaymentGateway\ManagePaymentGatewayRequest;
use App\Http\Requests\Backend\PaymentGateway\CreatePaymentGatewayRequest;
use App\Http\Requests\Backend\PaymentGateway\StorePaymentGatewayRequest;
use App\Http\Requests\Backend\PaymentGateway\EditPaymentGatewayRequest;
use App\Http\Requests\Backend\PaymentGateway\UpdatePaymentGatewayRequest;
use App\Http\Requests\Backend\PaymentGateway\DeletePaymentGatewayRequest;

/**
 * PaymentGatewaysController
 * Getting all paymentGateway
 */
class PaymentGatewaysController extends Controller
{
    /**
     * variable to store the repository object
     * @var PaymentGatewayRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param PaymentGatewayRepository $repository;
     */
    public function __construct(PaymentGatewayRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\PaymentGateway\ManagePaymentGatewayRequest  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManagePaymentGatewayRequest $request)
    {
        return new ViewResponse('backend.paymentgateways.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreatePaymentGatewayRequestNamespace  $request
     * @return \App\Http\Responses\Backend\PaymentGateway\CreateResponse
     */
    public function create(CreatePaymentGatewayRequest $request)
    {
        return new CreateResponse('backend.paymentgateways.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StorePaymentGatewayRequestNamespace  $request
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StorePaymentGatewayRequest $request)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Create the model using repository create method
        $this->repository->create($input);
        //return with successfull message
        return new RedirectResponse(route('admin.paymentgateways.index'), ['flash_success' => trans('alerts.backend.paymentgateways.created')]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\PaymentGateway\PaymentGateway  $paymentgateway
     * @param  EditPaymentGatewayRequestNamespace  $request
     * @return \App\Http\Responses\Backend\PaymentGateway\EditResponse
     */
    public function edit(PaymentGateway $paymentgateway, EditPaymentGatewayRequest $request)
    {
        return new EditResponse($paymentgateway);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdatePaymentGatewayRequestNamespace  $request
     * @param  App\Models\PaymentGateway\PaymentGateway  $paymentgateway
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(UpdatePaymentGatewayRequest $request, PaymentGateway $paymentgateway)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Update the model using repository update method
        $this->repository->update( $paymentgateway, $input );
        //return with successfull message
        return new RedirectResponse(route('admin.paymentgateways.index'), ['flash_success' => trans('alerts.backend.paymentgateways.updated')]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeletePaymentGatewayRequestNamespace  $request
     * @param  App\Models\PaymentGateway\PaymentGateway  $paymentgateway
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(PaymentGateway $paymentgateway, DeletePaymentGatewayRequest $request)
    {
        //Calling the delete method on repository
        $this->repository->delete($paymentgateway);
        //returning with successfull message
        return new RedirectResponse(route('admin.paymentgateways.index'), ['flash_success' => trans('alerts.backend.paymentgateways.deleted')]);
    }
    
}
