<?php

namespace App\Http\Controllers\Backend\PaymentGateway;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\PaymentGateway\PaymentGatewayRepository;
use App\Http\Requests\Backend\PaymentGateway\ManagePaymentGatewayRequest;

/**
 * Class PaymentGatewaysTableController.
 */
class PaymentGatewaysTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var PaymentGatewayRepository
     */
    protected $paymentgateway;

    /**
     * contructor to initialize repository object
     * @param PaymentGatewayRepository $paymentgateway;
     */
    public function __construct(PaymentGatewayRepository $paymentgateway)
    {
        $this->paymentgateway = $paymentgateway;
    }

    /**
     * This method return the data of the model
     * @param ManagePaymentGatewayRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManagePaymentGatewayRequest $request)
    {
        return Datatables::of($this->paymentgateway->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('created_at', function ($paymentgateway) {
                return Carbon::parse($paymentgateway->created_at)->toDateString();
            })
            ->addColumn('actions', function ($paymentgateway) {
                return $paymentgateway->action_buttons;
            })
            ->make(true);
    }
}
