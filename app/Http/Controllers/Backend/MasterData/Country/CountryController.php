<?php

namespace App\Http\Controllers\Backend\MasterData\Country;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\MasterData\CountryRequest;
use Illuminate\Http\Request;
use App\Http\Requests\Backend\MasterData\Country\StoreCountryRequest;
use App\Http\Responses\Backend\MasterData\Country\EditResponse;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Models\MasterData\Country;
use App\Repositories\Backend\MasterData\CountryRepository;

/**
 * CountryController for manage Countries
 * @method index,create,edit,update,destroy
 */
class CountryController extends Controller
{
    /**
     * Country Model Object.
     *
     * @var \App\Models\MasterData\Country
     */
    protected $country;

    /**
     * @param \App\Repositories\Backend\MasterData\CountryRepository $mascountryterData
     */
    public function __construct(CountryRepository $country)
    {
        $this->country = $country;
    }

    /**
     * Display a listing of the resource country.
     *
     * @param \App\Http\Requests\Backend\MasterData\CountryRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(CountryRequest $request)
    {
        return new ViewResponse('backend.master-data.countries.index');
    }

    
    /**
     * @param \App\Http\Requests\Backend\MasterData\CountryRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function create(CountryRequest $request)
    {
        return new ViewResponse('backend.master-data.countries.create');
    }

    /**
     * @param \App\Http\Requests\Backend\MasterData\Country\StoreCountryRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreCountryRequest $request)
    {
        $this->country->create($request->except(['_token']));

        return new RedirectResponse(route('admin.master-data.countries.index'), ['flash_success' => trans('alerts.backend.master-data.countries.created')]);
    }

    /**
     * @param \App\Models\MasterData\Country $country
     * @param \App\Http\Requests\Backend\MasterData\CountryRequest $request
     *
     * @return \App\Http\Responses\Backend\MasterData\EditResponse
     */
    public function edit(Country $country, CountryRequest $request)
    {
        return new EditResponse($country);
    }

    /**
     * @param \App\Models\MasterData\Country $country
     * @param \App\Http\Requests\Backend\MasterData\Country\StoreCountryRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(Country $country, StoreCountryRequest $request)
    {
        $this->country->update($country, $request->except(['_method', '_token']));

        return new RedirectResponse(route('admin.master-data.countries.index'), ['flash_success' => trans('alerts.backend.master-data.countries.updated')]);
    }

    /**
     * @param \App\Models\MasterData\Country $country
     * @param \App\Http\Requests\Backend\MasterData\CountryRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(Country $country, CountryRequest $request)
    {
        $this->country->delete($country);

        return new RedirectResponse(route('admin.master-data.countries.index'), ['flash_success' => trans('alerts.backend.master-data.countries.deleted')]);
    }
}
