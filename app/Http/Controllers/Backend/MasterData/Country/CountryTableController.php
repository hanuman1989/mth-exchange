<?php

namespace App\Http\Controllers\Backend\MasterData\Country;

use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\MasterData\CountryRepository;
use App\Http\Requests\Backend\MasterData\CountryRequest;
use Carbon\Carbon;

/**
 * Class CountryTableController for manage countries.
 */
class CountryTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var CountryRepository
     */
    protected $country;

    /**
     * contructor to initialize repository object
     * @param CountryRepository $country;
     */
    public function __construct(CountryRepository $country)
    {
        $this->country = $country;
    }

    /**
     * This method return the data of the model
     * @param CountryRequest $request
     *
     * @return mixed
     */
    public function __invoke(CountryRequest $request)
    {
        return Datatables::make($this->country->getForCountryDataTable())
            ->escapeColumns(['id'])
            ->addColumn('created_at', function ($countries) {
                return Carbon::parse($countries->updated_at)->toDateString();
            })
            ->addColumn('status', function ($countries) {
                return $countries->status_label;
            })
            ->addColumn('updated_at', function ($countries) {
                return Carbon::parse($countries->updated_at)->toDateString();
            })
            ->addColumn('actions', function ($countries) {
                return $countries->action_buttons;
            })
            ->make(true);
    }
}
