<?php

namespace App\Http\Controllers\Backend\MasterData\State;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\MasterData\StateRequest;
use Illuminate\Http\Request;
use App\Http\Requests\Backend\MasterData\State\StoreStateRequest;
use App\Http\Responses\Backend\MasterData\State\EditResponse;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Models\MasterData\State;
use App\Repositories\Backend\MasterData\StateRepository;


/**
 * StateController to manage master states
 * @method index,create,edit,update,destroy
 */
class StateController extends Controller
{
    /**
     * State Model Object.
     *
     * @var \App\Models\MasterData\State
     */
    protected $state;

    /**
     * @param \App\Repositories\Backend\MasterData\StateRepository $state
     */
    public function __construct(StateRepository $state)
    {
        $this->state = $state;
    }

    /**
     * Display a listing of the resource state.
     *
     * @param \App\Http\Requests\Backend\MasterData\StateRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(StateRequest $request)
    {
        return new ViewResponse('backend.master-data.states.index');
    }

    /**
     * @param \App\Http\Requests\Backend\MasterData\StateRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function create(Request $request)
    {
        return new ViewResponse('backend.master-data.states.create');
    }

    /**
     * @param \App\Http\Requests\Backend\MasterData\State\StoreStateRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreStateRequest $request)
    {
        $this->state->create($request->except(['_token']));

        return new RedirectResponse(route('admin.master-data.states.index'), ['flash_success' => trans('alerts.backend.master-data.states.created')]);
    }

    /**
     * @param \App\Models\MasterData\State $state
     * @param \App\Http\Requests\Backend\MasterData\StateRequest $request
     *
     * @return \App\Http\Responses\Backend\MasterData\EditResponse
     */
    public function edit(State $state, StateRequest $request)
    {
        return new EditResponse($state);
    }

    /**
     * @param \App\Models\MasterData\State $state
     * @param \App\Http\Requests\Backend\MasterData\State\StoreStateRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(State $state, StoreStateRequest $request)
    {
        $this->state->update($state, $request->except(['_method', '_token']));

        return new RedirectResponse(route('admin.master-data.states.index'), ['flash_success' => trans('alerts.backend.master-data.states.updated')]);
    }

    /**
     * @param \App\Models\MasterData\State $state
     * @param \App\Http\Requests\Backend\MasterData\StateRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(State $state, StateRequest $request)
    {
        $this->state->delete($state);

        return new RedirectResponse(route('admin.master-data.states.index'), ['flash_success' => trans('alerts.backend.master-data.states.deleted')]);
    }
}
