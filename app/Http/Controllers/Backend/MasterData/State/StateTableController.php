<?php

namespace App\Http\Controllers\Backend\MasterData\State;

use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\MasterData\StateRepository;
use App\Http\Requests\Backend\MasterData\StateRequest;
use Carbon\Carbon;

/**
 * Class StateTableController for manage states.
 */
class StateTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var StateRepository
     */
    protected $state;

    /**
     * contructor to initialize repository object
     * @param StateRepository $state;
     */
    public function __construct(StateRepository $state)
    {
        $this->state = $state;
    }

    /**
     * This method return the data of the model
     * @param StateRequest $request
     *
     * @return mixed
     */
    public function __invoke(StateRequest $request)
    {
        return Datatables::make($this->state->getForStateDataTable())
            ->escapeColumns(['id'])
            ->addColumn('created_at', function ($states) {
                return Carbon::parse($states->updated_at)->toDateString();
            })
            ->addColumn('status', function ($states) {
                // print_r($countries->status_label);
                return $states->status_label;
            })
            ->addColumn('updated_at', function ($states) {
                return Carbon::parse($states->updated_at)->toDateString();
            })
            ->addColumn('actions', function ($states) {
                return $states->action_buttons;
            })
            ->make(true);
    }
}
