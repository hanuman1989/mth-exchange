<?php

namespace App\Http\Controllers\Backend\TicketStatus;

use App\Models\TicketStatus\TicketStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\TicketStatus\CreateResponse;
use App\Http\Responses\Backend\TicketStatus\EditResponse;
use App\Repositories\Backend\TicketStatus\TicketStatusRepository;
use App\Http\Requests\Backend\TicketStatus\ManageTicketStatusRequest;
use App\Http\Requests\Backend\TicketStatus\StoreTicketStatusRequest;
use App\Http\Requests\Backend\TicketStatus\UpdateTicketStatusRequest;
use App\Http\Requests\Backend\TicketStatus\DeleteTicketStatusRequest;

/**
 * TicketStatusesController
 */
class TicketStatusesController extends Controller
{
    /**
     * variable to store the repository object
     * @var TicketStatusRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param TicketStatusRepository $repository;
     */
    public function __construct(TicketStatusRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\TicketStatus\ManageTicketStatusRequest  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManageTicketStatusRequest $request)
    {
        return new ViewResponse('backend.ticketstatuses.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreateTicketStatusRequestNamespace  $request
     * @return \App\Http\Responses\Backend\TicketStatus\CreateResponse
     */
    public function create(Request $request)
    {
        return new CreateResponse('backend.ticketstatuses.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreTicketStatusRequestNamespace  $request
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreTicketStatusRequest $request)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Create the model using repository create method
        $this->repository->create($input);
        //return with successfull message
        return new RedirectResponse(route('admin.ticketstatuses.index'), ['flash_success' => trans('alerts.backend.ticketstatuses.created')]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\TicketStatus\TicketStatus  $ticketstatus
     * @param  EditTicketStatusRequestNamespace  $request
     * @return \App\Http\Responses\Backend\TicketStatus\EditResponse
     */
    public function edit(TicketStatus $ticketstatus,Request $request)
    {
        return new EditResponse($ticketstatus);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateTicketStatusRequestNamespace  $request
     * @param  App\Models\TicketStatus\TicketStatus  $ticketstatus
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(UpdateTicketStatusRequest $request, TicketStatus $ticketstatus)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Update the model using repository update method
        $this->repository->update( $ticketstatus, $input );
        //return with successfull message
        return new RedirectResponse(route('admin.ticketstatuses.index'), ['flash_success' => trans('alerts.backend.ticketstatuses.updated')]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeleteTicketStatusRequestNamespace  $request
     * @param  App\Models\TicketStatus\TicketStatus  $ticketstatus
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(TicketStatus $ticketstatus, DeleteTicketStatusRequest $request)
    {
        //Calling the delete method on repository
        $this->repository->delete($ticketstatus);
        //returning with successfull message
        return new RedirectResponse(route('admin.ticketstatuses.index'), ['flash_success' => trans('alerts.backend.ticketstatuses.deleted')]);
    }
    
}
