<?php

namespace App\Http\Controllers\Backend\TicketStatus;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\TicketStatus\TicketStatusRepository;
use App\Http\Requests\Backend\TicketStatus\ManageTicketStatusRequest;

/**
 * Class TicketStatusesTableController.
 */
class TicketStatusesTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var TicketStatusRepository
     */
    protected $ticketstatus;

    /**
     * contructor to initialize repository object
     * @param TicketStatusRepository $ticketstatus;
     */
    public function __construct(TicketStatusRepository $ticketstatus)
    {
        $this->ticketstatus = $ticketstatus;
    }

    /**
     * This method return the data of the model
     * @param ManageTicketStatusRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageTicketStatusRequest $request)
    {
        return Datatables::of($this->ticketstatus->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('created_at', function ($ticketstatus) {
                return Carbon::parse($ticketstatus->created_at)->toDateString();
            })
            ->addColumn('actions', function ($ticketstatus) {
                return $ticketstatus->action_buttons;
            })
            ->make(true);
    }
}
