<?php

namespace App\Http\Responses\Backend\PaymentType;

use Illuminate\Contracts\Support\Responsable;

class EditResponse implements Responsable
{
    /**
     * @var App\Models\PaymentType\PaymentType
     */
    protected $paymenttypes;

    /**
     * @param App\Models\PaymentType\PaymentType $paymenttypes
     */
    public function __construct($paymenttypes)
    {
        $this->paymenttypes = $paymenttypes;
    }

    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        return view('backend.paymenttypes.edit')->with([
            'paymenttypes' => $this->paymenttypes
        ]);
    }
}