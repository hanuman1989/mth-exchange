<?php

namespace App\Http\Responses\Backend\Notes;

use Illuminate\Contracts\Support\Responsable;

class EditResponse implements Responsable
{
    /**
     * @var App\Models\Notes\Note
     */
    protected $notes;

    /**
     * @param App\Models\Notes\Note $notes
     */
    public function __construct($notes)
    {
        $this->notes = $notes;
    }

    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        return view('backend.notes.edit')->with([
            'notes' => $this->notes
        ]);
    }
}