<?php

namespace App\Http\Responses\Backend\TaxRules;

use Illuminate\Contracts\Support\Responsable;
use App\Models\MasterData\Country;

class EditResponse implements Responsable
{
    /**
     * @var App\Models\TaxRules\TaxRule
     */
    protected $taxrules;

    /**
     * @param App\Models\TaxRules\TaxRule $taxrules
     */
    public function __construct($taxrules)
    {
        $this->taxrules = $taxrules;
    }

    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        $countries = Country::active(true)->pluck('name','id')->toArray();
        return view('backend.taxrules.edit')->with([
            'taxrules' => $this->taxrules,
            'countries' => $countries
        ]);
    }
}