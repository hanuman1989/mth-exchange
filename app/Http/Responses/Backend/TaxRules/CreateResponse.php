<?php

namespace App\Http\Responses\Backend\TaxRules;

use Illuminate\Contracts\Support\Responsable;
use App\Models\MasterData\Country;

class CreateResponse implements Responsable
{
    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        $countries = Country::active(true)->pluck('name','id')->toArray();
        return view('backend.taxrules.create')->with([
            'countries' => $countries
        ]);
    }
}