<?php

namespace App\Http\Responses\Backend\PaymentGateway;

use Illuminate\Contracts\Support\Responsable;

class EditResponse implements Responsable
{
    /**
     * @var App\Models\PaymentGateway\PaymentGateway
     */
    protected $paymentgateways;

    /**
     * @param App\Models\PaymentGateway\PaymentGateway $paymentgateways
     */
    public function __construct($paymentgateways)
    {
        $this->paymentgateways = $paymentgateways;
    }

    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        return view('backend.paymentgateways.edit')->with([
            'paymentgateways' => $this->paymentgateways
        ]);
    }
}