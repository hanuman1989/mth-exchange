<?php

namespace App\Http\Responses\Backend\Language;

use Illuminate\Contracts\Support\Responsable;

class EditResponse implements Responsable
{
    /**
     * @var App\Models\Language\Language
     */
    protected $languages;

    /**
     * @param App\Models\Language\Language $languages
     */
    public function __construct($languages)
    {
        $this->languages = $languages;
    }

    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        return view('backend.languages.edit')->with([
            'language' => $this->languages
        ]);
    }
}