<?php

namespace App\Http\Responses\Backend\Clients;

use Illuminate\Contracts\Support\Responsable;
use App\Models\MasterData\State;
use App\Models\MasterData\Country;
use App\Models\SecurityQuestion\SecurityQuestion;
use App\Models\Language\Language;

class EditResponse implements Responsable
{
    /**
     * @var App\Models\Clients\ClientDetail
     */
    protected $clientdetails;

    protected $roles;

    protected $permissions;


    /**
     * @param App\Models\Clients\ClientDetail $clientdetails
     */
    public function __construct($clientdetails, $roles, $permissions)
    {
        $this->clientdetails = $clientdetails;
        $this->roles = $roles;
        $this->permissions = $permissions;
    }

    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        $countries = Country::active(true)->pluck('name','id')->toArray();
        $states = State::active(true)->pluck('name','id')->toArray();
        $securityQuestions = SecurityQuestion::active(true)->pluck('question','id')->toArray();
        $languages = Language::active(true)->pluck('name','id')->toArray();

        return view('backend.clients.edit')->with([
            'clientdetail' => $this->clientdetails,
            'countries' => $countries,
            'states' => $states,
            'roles' => $this->roles,
            'permissions' => $this->permissions,
            'securityQuestions' => $securityQuestions,
            'languages' => $languages
        ]);
    }
}