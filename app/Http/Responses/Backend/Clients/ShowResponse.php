<?php

namespace App\Http\Responses\Backend\Clients;

use Illuminate\Contracts\Support\Responsable;

class ShowResponse implements Responsable
{
    /**
     * @var \App\Models\Access\User\User
     */
    protected $client;

    /**
     * @param \App\Models\Access\User\User $user
     */
    public function __construct($client)
    {
        $this->client = $client;
    }

    /**
     * In Response.
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        return view('backend.clients.show')->withClient($this->client);
    }
}
