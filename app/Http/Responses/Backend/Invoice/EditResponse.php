<?php

namespace App\Http\Responses\Backend\Invoice;

use Illuminate\Contracts\Support\Responsable;

/**
 * EditResponse to redirect invoice create view
 */
class EditResponse implements Responsable
{
    /**
     * @var App\Models\Invoice\Invoice
     */
    protected $invoices;

    /**
     * @param App\Models\Invoice\Invoice $invoices
     */
    public function __construct($invoices)
    {
        $this->invoices = $invoices;
    }

    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        return view('backend.invoices.edit')->with([
            'invoices' => $this->invoices
        ]);
    }
}