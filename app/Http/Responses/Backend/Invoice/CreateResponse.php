<?php

namespace App\Http\Responses\Backend\Invoice;

use Illuminate\Contracts\Support\Responsable;

/**
 * CreateResponse to redirect invoice create view
 */
class CreateResponse implements Responsable
{
    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        return view('backend.invoices.create');
    }
}