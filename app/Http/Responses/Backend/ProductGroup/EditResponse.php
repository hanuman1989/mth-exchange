<?php

namespace App\Http\Responses\Backend\ProductGroup;

use Illuminate\Contracts\Support\Responsable;

class EditResponse implements Responsable
{
    /**
     * @var App\Models\ProductGroup\ProductGroup
     */
    protected $productgroups;

    /**
     * @param App\Models\ProductGroup\ProductGroup $productgroups
     */
    public function __construct($productgroups)
    {
        $this->productgroups = $productgroups;
    }

    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
		
        return view('backend.productgroups.edit')->with([
            'productgroups' => $this->productgroups
        ]);
    }
}