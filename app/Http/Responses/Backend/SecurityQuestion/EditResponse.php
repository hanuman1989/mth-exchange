<?php

namespace App\Http\Responses\Backend\SecurityQuestion;

use Illuminate\Contracts\Support\Responsable;

class EditResponse implements Responsable
{
    /**
     * @var App\Models\SecurityQuestion\SecurityQuestion
     */
    protected $securityquestions;

    /**
     * @param App\Models\SecurityQuestion\SecurityQuestion $securityquestions
     */
    public function __construct($securityquestions)
    {
        $this->securityquestions = $securityquestions;
    }

    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        return view('backend.securityquestions.edit')->with([
            'securityquestion' => $this->securityquestions
        ]);
    }
}