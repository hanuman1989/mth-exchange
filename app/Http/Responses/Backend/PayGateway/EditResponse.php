<?php

namespace App\Http\Responses\Backend\PayGateway;

use Illuminate\Contracts\Support\Responsable;

class EditResponse implements Responsable
{
    /**
     * @var App\Models\PayGateway\PayGateway
     */
    protected $paygateways;

    /**
     * @param App\Models\PayGateway\PayGateway $paygateways
     */
    public function __construct($paygateways)
    {
        $this->paygateways = $paygateways;
    }

    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        return view('backend.paygateways.edit')->with([
            'paygateways' => $this->paygateways
        ]);
    }
}