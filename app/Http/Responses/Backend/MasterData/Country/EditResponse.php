<?php

namespace App\Http\Responses\Backend\MasterData\Country;

use Illuminate\Contracts\Support\Responsable;
use App\Models\MasterData\Country;

class EditResponse implements Responsable
{

    /**
     * @var \App\Models\MasterData\Country
     */
    protected $country;

     /**
     * @param \App\Models\MasterData\Country $country
     */
    public function __construct($country)
    {
        $this->country = $country;
    }

    /**
     * toReponse.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        return view('backend.master-data.countries.edit')->with([
            'country' => $this->country
        ]);
    }
}
