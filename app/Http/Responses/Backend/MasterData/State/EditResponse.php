<?php

namespace App\Http\Responses\Backend\MasterData\State;

use Illuminate\Contracts\Support\Responsable;
use App\Models\MasterData\State;

class EditResponse implements Responsable
{

    /**
     * @var \App\Models\MasterData\State
     */
    protected $state;

     /**
     * @param \App\Models\MasterData\State $state
     */
    public function __construct($state)
    {
        $this->state = $state;
    }

    /**
     * toReponse.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        return view('backend.master-data.states.edit')->with([
            'state' => $this->state
        ]);
    }
}
