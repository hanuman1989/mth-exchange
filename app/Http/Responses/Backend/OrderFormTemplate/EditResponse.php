<?php

namespace App\Http\Responses\Backend\OrderFormTemplate;

use Illuminate\Contracts\Support\Responsable;

class EditResponse implements Responsable
{
    /**
     * @var App\Models\OrderFormTemplate\OrderFormTemplate
     */
    protected $orderformtemplates;

    /**
     * @param App\Models\OrderFormTemplate\OrderFormTemplate $orderformtemplates
     */
    public function __construct($orderformtemplates)
    {
        $this->orderformtemplates = $orderformtemplates;
    }

    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        return view('backend.orderformtemplates.edit')->with([
            'orderformtemplates' => $this->orderformtemplates
        ]);
    }
}