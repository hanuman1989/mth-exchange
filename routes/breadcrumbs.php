<?php
use Tabuna\Breadcrumbs\Breadcrumbs;
use Tabuna\Breadcrumbs\Trail;

Breadcrumbs::for('admin.dashboard', function ($trail) {
    $trail->push('Home', route('admin.dashboard'), ['icon' => 'fa fa-dashboard']);
});

Breadcrumbs::for('common', function ($trail, $append) {
     
	$trail->parent('admin.dashboard', route('admin.dashboard'));

    if(!empty($append)){
        foreach($append as $crum){
            $label = Str::title(str_replace("-", " ", Str::kebab(ucfirst($crum['label']))));

            if($crum['action'] != 'index') {
                $trail->push($label, route($crum['route'].'.index'));
                $trail->push($crum['action'],  route($crum['route'].".".$crum['action'],$crum['id']));
            } else {
                $trail->push($label, route($crum['route'].'.index'));
            }
        }
    }
});




