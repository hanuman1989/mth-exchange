<?php

/*
 * CMS Pages Management
 */
Route::group(['namespace' => 'Backend\EmailTemplates'], function () {
    Route::resource('email', 'EmailTemplatesController', ['except' => ['show']]);
    Route::any('email/destroy/{email}', 'EmailTemplatesController@destroy')->name('email.delete');;
    //For DataTables
    
    Route::post('email/get', 'EmailTemplatesTableController')->name('email.get');
});
