<?php

/**
 * All route names are prefixed with 'admin.access'.
 */
Route::group([
    'prefix'    => 'master-data',
    'as'        => 'master-data.',
    'namespace' => 'Backend\MasterData',
], function () {

    /*
     * User Management
     */
    Route::group([
        /* 'middleware' => 'access.routeNeedsPermission:edit-settings', */
    ], function () {
        //Resource route for country master data
        Route::group(['namespace'=>'Country'], function() {
            Route::resource('countries', 'CountryController', ['except' => 'show']);
            Route::any('countries/destroy/{country}', 'CountryController@destroy')->name('countries.delete');;
            //For DataTables
            Route::post('countries/get', 'CountryTableController')
            ->name('countries.get');
        });

        //Resource route for state master data
        Route::group(['namespace'=>'State'], function() {
            Route::resource('states', 'StateController', ['except' => 'show']);
            Route::any('states/destroy/{state}', 'StateController@destroy')->name('states.delete');;
            //For DataTables
            Route::post('states/get', 'StateTableController')
            ->name('states.get');
        });
    });
});
