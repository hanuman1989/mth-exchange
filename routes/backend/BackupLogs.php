<?php

/*
 * CMS Pages Management
 */
Route::group(['namespace' => 'Backend\BackupLogs'], function () {
    Route::resource('backuplogs', 'BackupLogsController', ['except' => ['show', 'edit', 'update', 'create', 'store']]);
    //For DataTables
    Route::post('backuplogs/get', 'BackupLogsTableController')->name('backuplogs.get');
});
