<?php
/**
 * TicketReply
 *
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    
    Route::group( ['namespace' => 'TicketReply'], function () {
        Route::resource('ticketreplies', 'TicketRepliesController');
        //For Datatable
        Route::post('ticketreplies/get', 'TicketRepliesTableController')->name('ticketreplies.get');
    });
    
});