<?php
/**
 * Invoice
 *
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    
    Route::group( ['namespace' => 'Invoice'], function () {
        Route::resource('invoices', 'InvoicesController', ['except' => ['show']]);
        Route::any('invoices/destroy/{invoice}', 'InvoicesController@destroy')->name('invoices.delete');;

        //For Datatable
        Route::post('invoices/get', 'InvoicesTableController')->name('invoices.get');
    });
    
});