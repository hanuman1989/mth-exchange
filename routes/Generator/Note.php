<?php
/**
 * Notes
 *
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    
    Route::group( ['namespace' => 'Notes'], function () {
        Route::any('notes/destroy/{note}', 'NotesController@destroy')->name('notes.delete');;
        Route::resource('notes', 'NotesController');
        //For Datatable
        Route::post('notes/get', 'NotesTableController')->name('notes.get');
    });
    
});