<?php
/**
 * OrderFormTemplate
 *
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    
    Route::group( ['namespace' => 'OrderFormTemplate'], function () {
        Route::resource('orderformtemplates', 'OrderFormTemplatesController');
        //For Datatable
        Route::post('orderformtemplates/get', 'OrderFormTemplatesTableController')->name('orderformtemplates.get');
    });
    
});