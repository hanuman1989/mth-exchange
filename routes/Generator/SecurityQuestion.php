<?php
/**
 * SecurityQuestion
 *
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    
    Route::group( ['namespace' => 'SecurityQuestion'], function () {
        Route::resource('securityquestions', 'SecurityQuestionsController', ['except' => ['show']]);
        Route::any('securityquestions/destroy/{securityquestion}', 'SecurityQuestionsController@destroy')->name('securityquestions.delete');;
        //For Datatable
        Route::post('securityquestions/get', 'SecurityQuestionsTableController')->name('securityquestions.get');
    });
    
});