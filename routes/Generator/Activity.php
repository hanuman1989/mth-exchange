<?php
/**
 * Routes for : ClientLogs
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
	
	Route::group( ['namespace' => 'ClientLogs'], function () {
	    Route::get('activities', 'ActivitiesController@index')->name('activities.index');
	    Route::delete('activities/{activities}', 'ActivitiesController@destroy')->name('activities.destroy');
	    
	    
	    //For Datatable
	    Route::post('activities/get', 'ActivitiesTableController')->name('activities.get');
	});
	
});