<?php
/**
 * TaxRules
 *
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    
    Route::group( ['namespace' => 'TaxRules'], function () {
        Route::resource('taxrules', 'TaxRulesController');
        Route::any('taxrules/destroy/{taxrule}', 'TaxRulesController@destroy')->name('taxrules.delete');;
        
        //For Datatable
        Route::post('taxrules/get', 'TaxRulesTableController')->name('taxrules.get');
    });
    
});