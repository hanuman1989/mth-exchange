<?php
/**
 * TicketStatus
 *
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
 
    Route::group( ['namespace' => 'TicketStatus'], function () {
        Route::resource('ticketstatuses', 'TicketStatusesController');
        //For Datatable
        Route::post('ticketstatuses/get', 'TicketStatusesTableController')->name('ticketstatuses.get');
    });
    
});