<?php
/**
 * ClientManagement
 *
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    
    Route::group( ['namespace' => 'Clients'], function () {
        Route::resource('clients', 'ClientsController');
        //For Datatable
        Route::post('clients/get', 'ClientsTableController')->name('clients.get');
    });
    
});