<?php
/**
 * ProductGroup
 *
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    
    Route::group( ['namespace' => 'ProductGroup'], function () {
        Route::resource('productgroups', 'ProductGroupsController');
		//For Group Features
		Route::post('/group_features', 'ProductGroupsController@groupFeature')->name('productgroups.group_features');
		Route::post('/delete_features', 'ProductGroupsController@deleteFeature')->name('productgroups.delete_features');
		
		
        //For Datatable
		
        Route::post('productgroups/get', 'ProductGroupsTableController')->name('productgroups.get');
    });
    
});