<?php
/**
 * PayGateway
 *
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    
      Route::group( ['namespace' => 'PayGateway'], function () {
        Route::resource('paygateways', 'PayGatewaysController', ['except' => ['show']]);
        Route::any('paygateways/destroy/{paygateway}', 'PayGatewaysController@destroy')->name('paygateways.delete');
        //For Datatable
        Route::post('paygateways/get', 'PayGatewaysTableController')->name('paygateways.get');
        Route::get('stripe', 'CheckoutController@index')->name('checkout.index');
		Route::get('payment_intents', 'CheckoutController@paymentItents')->name('checkout.itent');
		Route::post('stripe/webhook', 'WebhooksController@handle');
		Route::get('paywithpaypal',   'PaymentController@payWithPaypal')->name('paywithpaypal');
		// route for post request
		Route::post('paypal', 'PaymentController@postPaymentWithpaypal')->name('addmoney.paypal');
		// route for check status responce
		Route::get('status', 'PaymentController@getPaymentStatus')->name('status');
    });

	
    
});