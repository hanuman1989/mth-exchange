<?php
/**
 * TicketPriorty
 *
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    Route::group( ['namespace' => 'TicketPriorty'], function () {
        Route::resource('ticketpriorties', 'TicketPriortiesController');
        //For Datatable
        Route::post('ticketpriorties/get', 'TicketPriortiesTableController')->name('ticketpriorties.get');
    });
    
});