<?php
/**
 * PaymentType
 *
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    
    Route::group( ['namespace' => 'PaymentType'], function () {
        Route::resource('paymenttypes', 'PaymentTypesController');
        //For Datatable
        Route::post('paymenttypes/get', 'PaymentTypesTableController')->name('paymenttypes.get');
    });
    
});