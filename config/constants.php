<?php

return [
    'payment_method' => [
        'Default' => 'Default',
        'Stripe' => 'Stripe',
        'BankTransfer' => 'Bank Transfer'
    ],

    'billing_contact' => [
        'Default' => 'Default'
    ],

    'currency' => [
        'GBP' => 'GBP',
        'USD' => 'USD'
    ],

    'client_group' => [
        'NONE' => 'NONE'
    ],

    'status' => [
        1 => 'Yes', 0 => 'No'
    ],

    'invoice_status' => [
        'Draft' => 'Draft',
        'Unpaid' => 'Unpaid', 
        'Overdue' => 'Overdue',
        'Paid' => 'Paid',
        'Cancelled' => 'Cancelled',
        'Refunded' => 'Refunded',
        'Collections' => 'Collections',
        'Payment Pending' => 'Payment Pending'
    ]

];