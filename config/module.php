<?php
return [
	"pages" => [
	"table" => "pages",
	],
	"emailtemplates" => [
	"table" => "email_templates",
	],
	"blog_tags" => [
	"table" => "blog_tags",
	],
	"blog_categories" => [
	"table" => "blog_categories",
	],
	"blogs" => [
	"table" => "blogs",
	],
	"faqs" => [
	"table" => "faqs",
	],
	"activities" => [
	"table" => "activity_log",
	],
	"paymentgatewaymanagers" => [
	"table" => "payment_gateway_name",
	],
	"payment_gateway_fields" => [
	"table" => "payment_gateway_fields",
	],
	"backuplogs" => [
	"table" => "backup_logs",
	],
	"notes" => [
	"table" => "notes",
	],
	"invoices" => [
	"table" => "invoices",
	],
];