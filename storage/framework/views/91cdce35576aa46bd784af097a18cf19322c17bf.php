<div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
    <div class="c-sidebar-brand d-lg-down-none">
        <svg class="c-sidebar-brand-full" width="118" height="46" alt="CoreUI Logo">
            <use xlink:href="<?php echo e(asset('img/brand/coreui.svg#full')); ?>"></use>
        </svg>
        <svg class="c-sidebar-brand-minimized" width="46" height="46" alt="CoreUI Logo">
            <use xlink:href="<?php echo e(asset('img/brand/coreui.svg#signet')); ?>"></use>
        </svg>
    </div><!--c-sidebar-brand-->

    <ul class="c-sidebar-nav">
        <li class="c-sidebar-nav-item">
             <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.utils.link','data' => ['class' => 'c-sidebar-nav-link','href' => route('admin.dashboard'),'active' => activeClass(Route::is('admin.dashboard'), 'c-active'),'icon' => 'c-sidebar-nav-icon cil-speedometer','text' => __('Dashboard')]]); ?>
<?php $component->withName('utils.link'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['class' => 'c-sidebar-nav-link','href' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(route('admin.dashboard')),'active' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(activeClass(Route::is('admin.dashboard'), 'c-active')),'icon' => 'c-sidebar-nav-icon cil-speedometer','text' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(__('Dashboard'))]); ?>
<?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 
        </li>

        <?php if(
            $logged_in_user->hasAllAccess() ||
            (
                $logged_in_user->can('admin.access.user.list') ||
                $logged_in_user->can('admin.access.user.deactivate') ||
                $logged_in_user->can('admin.access.user.reactivate') ||
                $logged_in_user->can('admin.access.user.clear-session') ||
                $logged_in_user->can('admin.access.user.impersonate') ||
                $logged_in_user->can('admin.access.user.change-password')
            )
        ): ?>
            <li class="c-sidebar-nav-title"><?php echo app('translator')->get('System'); ?></li>

            <li class="c-sidebar-nav-dropdown <?php echo e(activeClass(Route::is('admin.auth.user.*') || Route::is('admin.auth.role.*'), 'c-open c-show')); ?>">
                 <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.utils.link','data' => ['href' => '#','icon' => 'c-sidebar-nav-icon cil-user','class' => 'c-sidebar-nav-dropdown-toggle','text' => __('Access')]]); ?>
<?php $component->withName('utils.link'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['href' => '#','icon' => 'c-sidebar-nav-icon cil-user','class' => 'c-sidebar-nav-dropdown-toggle','text' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(__('Access'))]); ?>
<?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 

                <ul class="c-sidebar-nav-dropdown-items">
                    <?php if(
                        $logged_in_user->hasAllAccess() ||
                        (
                            $logged_in_user->can('admin.access.user.list') ||
                            $logged_in_user->can('admin.access.user.deactivate') ||
                            $logged_in_user->can('admin.access.user.reactivate') ||
                            $logged_in_user->can('admin.access.user.clear-session') ||
                            $logged_in_user->can('admin.access.user.impersonate') ||
                            $logged_in_user->can('admin.access.user.change-password')
                        )
                    ): ?>
                        <li class="c-sidebar-nav-item">
                             <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.utils.link','data' => ['href' => route('admin.auth.user.index'),'class' => 'c-sidebar-nav-link','text' => __('User Management'),'active' => activeClass(Route::is('admin.auth.user.*'), 'c-active')]]); ?>
<?php $component->withName('utils.link'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['href' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(route('admin.auth.user.index')),'class' => 'c-sidebar-nav-link','text' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(__('User Management')),'active' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(activeClass(Route::is('admin.auth.user.*'), 'c-active'))]); ?>
<?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 
                        </li>
                    <?php endif; ?>

                    <?php if($logged_in_user->hasAllAccess()): ?>
                        <li class="c-sidebar-nav-item">
                             <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.utils.link','data' => ['href' => route('admin.auth.role.index'),'class' => 'c-sidebar-nav-link','text' => __('Role Management'),'active' => activeClass(Route::is('admin.auth.role.*'), 'c-active')]]); ?>
<?php $component->withName('utils.link'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['href' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(route('admin.auth.role.index')),'class' => 'c-sidebar-nav-link','text' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(__('Role Management')),'active' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(activeClass(Route::is('admin.auth.role.*'), 'c-active'))]); ?>
<?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 
                        </li>
                     <?php endif; ?>
                </ul>
            </li>
        <?php endif; ?>

        <?php if($logged_in_user->hasAllAccess()): ?>
            <li class="c-sidebar-nav-item">
                 <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.utils.link','data' => ['class' => 'c-sidebar-nav-link','href' => '/admin/log-viewer','icon' => 'c-sidebar-nav-icon cil-list','text' => __('Logs')]]); ?>
<?php $component->withName('utils.link'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['class' => 'c-sidebar-nav-link','href' => '/admin/log-viewer','icon' => 'c-sidebar-nav-icon cil-list','text' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(__('Logs'))]); ?>
<?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 
            </li>
        <?php endif; ?>

		<li class="c-sidebar-nav-item">
				   <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.utils.link','data' => ['class' => 'c-sidebar-nav-link','href' => '/admin/paymenttypes','icon' => 'c-sidebar-nav-icon cil-list','text' => __('Payment Types')]]); ?>
<?php $component->withName('utils.link'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['class' => 'c-sidebar-nav-link','href' => '/admin/paymenttypes','icon' => 'c-sidebar-nav-icon cil-list','text' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(__('Payment Types'))]); ?>
<?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 
            </li>
			 <li >
               
				  <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.utils.link','data' => ['class' => 'c-sidebar-nav-link','href' => '/admin/orderformtemplates','icon' => 'c-sidebar-nav-icon cil-list','text' => __('Order Form Templates')]]); ?>
<?php $component->withName('utils.link'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['class' => 'c-sidebar-nav-link','href' => '/admin/orderformtemplates','icon' => 'c-sidebar-nav-icon cil-list','text' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(__('Order Form Templates'))]); ?>
<?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 
            </li>
			<li >
                
				 <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.utils.link','data' => ['class' => 'c-sidebar-nav-link','href' => '/admin/productgroups','icon' => 'c-sidebar-nav-icon cil-list','text' => __('Product Groups')]]); ?>
<?php $component->withName('utils.link'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['class' => 'c-sidebar-nav-link','href' => '/admin/productgroups','icon' => 'c-sidebar-nav-icon cil-list','text' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(__('Product Groups'))]); ?>
<?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 
            </li>
            
        <?php if($logged_in_user->hasAllAccess()): ?>
            <li class="c-sidebar-nav-item">
                 <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.utils.link','data' => ['class' => 'c-sidebar-nav-link','href' => route('admin.securityquestions.index'),'icon' => 'c-sidebar-nav-icon cil-list','text' => __('labels.backend.securityquestions.title'),'active' => activeClass(Route::is('admin.securityquestions.*'), 'c-active')]]); ?>
<?php $component->withName('utils.link'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['class' => 'c-sidebar-nav-link','href' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(route('admin.securityquestions.index')),'icon' => 'c-sidebar-nav-icon cil-list','text' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(__('labels.backend.securityquestions.title')),'active' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(activeClass(Route::is('admin.securityquestions.*'), 'c-active'))]); ?>
<?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 
            </li>
        <?php endif; ?>

    <?php if($logged_in_user->hasAllAccess()): ?>
    <li class="c-sidebar-nav-item">
         <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.utils.link','data' => ['class' => 'c-sidebar-nav-link','href' => route('admin.languages.index'),'icon' => 'c-sidebar-nav-icon cil-list','text' => __('labels.backend.languages.title'),'active' => activeClass(Route::is('admin.languages.*'), 'c-active')]]); ?>
<?php $component->withName('utils.link'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['class' => 'c-sidebar-nav-link','href' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(route('admin.languages.index')),'icon' => 'c-sidebar-nav-icon cil-list','text' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(__('labels.backend.languages.title')),'active' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(activeClass(Route::is('admin.languages.*'), 'c-active'))]); ?>
<?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 
    </li>
    <?php endif; ?>
    <?php if($logged_in_user->hasAllAccess()): ?>
    <!-- <li class="c-sidebar-nav-item">
         <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.utils.link','data' => ['class' => 'c-sidebar-nav-link','href' => route('admin.activities.index'),'icon' => 'c-sidebar-nav-icon cil-list','text' => __('labels.backend.activities.title'),'active' => activeClass(Route::is('admin.activities.*'), 'c-active')]]); ?>
<?php $component->withName('utils.link'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['class' => 'c-sidebar-nav-link','href' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(route('admin.activities.index')),'icon' => 'c-sidebar-nav-icon cil-list','text' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(__('labels.backend.activities.title')),'active' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(activeClass(Route::is('admin.activities.*'), 'c-active'))]); ?>
<?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 
    </li> -->
    <?php endif; ?>
        <?php if($logged_in_user->hasAllAccess()): ?>
        <li class="c-sidebar-nav-dropdown <?php echo e(activeClass(Route::is('admin.master-data.*'), 'c-open c-show')); ?>">
             <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.utils.link','data' => ['href' => '#','icon' => 'c-sidebar-nav-icon cil-settings','class' => 'c-sidebar-nav-dropdown-toggle','text' => __('menus.backend.master-data.title')]]); ?>
<?php $component->withName('utils.link'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['href' => '#','icon' => 'c-sidebar-nav-icon cil-settings','class' => 'c-sidebar-nav-dropdown-toggle','text' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(__('menus.backend.master-data.title'))]); ?>
<?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 

                <ul class="c-sidebar-nav-dropdown-items">
                    <li class="c-sidebar-nav-item">
                         <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.utils.link','data' => ['href' => route('admin.master-data.countries.index'),'class' => 'c-sidebar-nav-link','text' => __('menus.backend.master-data.country'),'active' => activeClass(Route::is('admin.master-data.countries.*'), 'c-active')]]); ?>
<?php $component->withName('utils.link'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['href' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(route('admin.master-data.countries.index')),'class' => 'c-sidebar-nav-link','text' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(__('menus.backend.master-data.country')),'active' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(activeClass(Route::is('admin.master-data.countries.*'), 'c-active'))]); ?>
<?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 
                    </li>
                    <li class="c-sidebar-nav-item">
                         <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.utils.link','data' => ['href' => route('admin.master-data.states.index'),'class' => 'c-sidebar-nav-link','text' => __('menus.backend.master-data.state'),'active' => activeClass(Route::is('admin.master-data.states.*'), 'c-active')]]); ?>
<?php $component->withName('utils.link'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['href' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(route('admin.master-data.states.index')),'class' => 'c-sidebar-nav-link','text' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(__('menus.backend.master-data.state')),'active' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(activeClass(Route::is('admin.master-data.states.*'), 'c-active'))]); ?>
<?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 
                    </li>
                </ul>
            </ul>
        </li>
        <?php endif; ?>
		
    </ul>

    <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent" data-class="c-sidebar-minimized"></button>
</div><!--sidebar-->
<?php /**PATH D:\xampp\htdocs\mth-exchange\resources\views/backend/includes/sidebar.blade.php ENDPATH**/ ?>