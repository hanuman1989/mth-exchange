<div class="box-body">
    <div class="form-group">
        <?php echo e(Form::label('name', trans('labels.backend.languages.table.name'), ['class' => 'col-lg-2 control-label required'])); ?>


        <div class="col-lg-10">
            <?php echo e(Form::text('name', null, ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.languages.table.name'), 'required', 'maxlength' => 255])); ?>

        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        <?php echo e(Form::label('iso_639-1', trans('labels.backend.languages.table.iso_639-1'), ['class' => 'col-lg-2 control-label required'])); ?>


        <div class="col-lg-10">
            <?php echo e(Form::text('iso_639-1', null, ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.languages.table.iso_639-1'), 'required', 'maxlength' => 2])); ?>

        </div><!--col-lg-10-->
    </div><!--form control-->

    
    <div class="form-group">
        <?php echo e(Form::label('status', trans('labels.backend.languages.table.status'), ['class' => 'col-lg-2 control-label'])); ?>


        <div class="col-lg-1">
                    <div class="control-group">
                        <label class="control control--checkbox">
                        <?php if(isset($language->status) && !empty ($language->status)): ?>
                            <?php echo e(Form::checkbox('status', 1, true)); ?>

                        <?php else: ?>
                            <?php echo e(Form::checkbox('status', 1, false)); ?>

                        <?php endif; ?>
                        <div class="control__indicator"></div>
                        </label>
                    </div>
        </div><!--col-lg-1-->
    </div><!--form control-->
</div><?php /**PATH D:\xampp_new_7_3\htdocs\mth-exchange\resources\views/backend/languages/form.blade.php ENDPATH**/ ?>