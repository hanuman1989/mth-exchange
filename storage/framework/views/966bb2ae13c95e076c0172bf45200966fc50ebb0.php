<?php if(config('boilerplate.google_analytics') && config('boilerplate.google_analytics') !== 'UA-XXXXX-X'): ?>
    
    <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','<?php echo e(config('boilerplate.google_analytics')); ?>','auto');ga('send','pageview');
    </script>
<?php endif; ?>
<?php /**PATH D:\xampp_new_7_3\htdocs\mth-exchange\resources\views/includes/partials/ga.blade.php ENDPATH**/ ?>