

<?php $__env->startSection('breadcrumb'); ?>
<div class="content-top-sec">
    <nav aria-label="breadcrumb">
        <?php echo e(Breadcrumbs::generate('common',['append' => [['label'=> $getController,'action' => $getAction,'route'=> 'admin.activities']]])); ?>

    </nav> 
    <h1>Client Logs Manager</h1>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('title', trans('labels.backend.activities.management')); ?>

<?php $__env->startSection('page-header'); ?>
    <h1><?php echo e(trans('labels.backend.activities.management')); ?></h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo e(trans('labels.backend.activities.management')); ?></h3>

            <div class="box-tools pull-right">
                <div class="btn-group">
                    <button type="button" class="btn btn-warning btn-flat dropdown-toggle" data-toggle="dropdown">Export
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li id="copyButton"><a href="#"><i class="fa fa-clone"></i> Copy</a></li>
                        <li id="csvButton"><a href="#"><i class="fa fa-file-text-o"></i> CSV</a></li>
                        <li id="excelButton"><a href="#"><i class="fa fa-file-excel-o"></i> Excel</a></li>
                        <li id="pdfButton"><a href="#"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                        <li id="printButton"><a href="#"><i class="fa fa-print"></i> Print</a></li>
                    </ul>
                </div>
            </div>
        </div><!--box-header with-border-->

        <div class="box-body">
            <div class="table-responsive data-table-wrapper">
                <table id="activities-table" class="table table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th><?php echo e(trans('labels.backend.activities.table.causer_id')); ?></th>
                            <th><?php echo e(trans('labels.backend.activities.table.log_name')); ?></th>
                            <th><?php echo e(trans('labels.backend.activities.table.description')); ?></th>
                            <th><?php echo e(trans('labels.backend.activities.table.createdat')); ?></th>
                            <th><?php echo e(trans('labels.general.actions')); ?></th>
                        </tr>
                    </thead>
                    <thead class="transparent-bg">
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
<?php $__env->stopSection(); ?>

<?php $__env->startPush('after-scripts'); ?>
    
    <?php echo e(Html::script('js/dataTable.js')); ?>


    <script>
        //Below written line is short form of writing $(document).ready(function() { })
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            var dataTable = $('#activities-table').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '<?php echo e(route("admin.activities.get")); ?>',
                    type: 'post'
                },
                columns: [
                    {data: 'causer_id', name: '<?php echo e(config('module.activities.table')); ?>.causer_id'},
                    {data: 'log_name', name: '<?php echo e(config('module.activities.table')); ?>.log_name'},
                    {data: 'description', name: '<?php echo e(config('module.activities.table')); ?>.description'},
                    {data: 'created_at', name: '<?php echo e(config('module.activities.table')); ?>.created_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                dom: 'lBfrtip',
                buttons: {
                    buttons: [
                        { extend: 'copy', className: 'copyButton',  exportOptions: {columns: [ 0, 1, 2, 3 ]  }},
                        { extend: 'csv', className: 'csvButton',  exportOptions: {columns: [ 0, 1, 2, 3 ]  }},
                        { extend: 'excel', className: 'excelButton',  exportOptions: {columns: [ 0, 1, 2, 3 ]  }},
                        { extend: 'pdf', className: 'pdfButton',  exportOptions: {columns: [ 0, 1, 2, 3 ]  }},
                        { extend: 'print', className: 'printButton',  exportOptions: {columns: [ 0, 1, 2, 3 ]  }}
                    ]
                }
            });

            Backend.DataTableSearch.init(dataTable);
        });
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp_new_7_3\htdocs\mth-exchange\resources\views/backend/clientlogs/index.blade.php ENDPATH**/ ?>