

<?php $__env->startSection('breadcrumb'); ?>
<div class="content-top-sec">
    <nav aria-label="breadcrumb">
        <?php echo e(Breadcrumbs::generate('common',['append' => [['label'=> $getController,'action' => $getAction,'route'=> 'admin.languages']]])); ?>

    </nav> 
    <h1>Language Manager</h1>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('title', trans('labels.backend.languages.management') . ' | ' . trans('labels.backend.languages.create')); ?>

<?php $__env->startSection('page-header'); ?>
    <h1>
        <?php echo e(trans('labels.backend.languages.management')); ?>

        <small><?php echo e(trans('labels.backend.languages.create')); ?></small>
    </h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <?php echo e(Form::open(['route' => 'admin.languages.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-language'])); ?>


        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo e(trans('labels.backend.languages.create')); ?></h3>

                <div class="box-tools pull-right">
                    <?php echo $__env->make('backend.languages.partials.languages-header-buttons', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </div><!--box-tools pull-right-->
            </div><!--box-header with-border-->

            <div class="box-body">
                <div class="form-group">
                    
                    <?php echo $__env->make("backend.languages.form", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <div class="edit-form-btn">
                        <?php echo e(link_to_route('admin.languages.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-md'])); ?>

                        <?php echo e(Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-primary btn-md'])); ?>

                        <div class="clearfix"></div>
                    </div><!--edit-form-btn-->
                </div><!-- form-group -->
            </div><!--box-body-->
        </div><!--box box-success-->
    <?php echo e(Form::close()); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp_new_7_3\htdocs\mth-exchange\resources\views/backend/languages/create.blade.php ENDPATH**/ ?>