

<?php $__env->startSection('title', __('Two Factor Recovery Codes')); ?>

<?php $__env->startSection('content'); ?>
    <div class="container py-4">
        <div class="row justify-content-center">
            <div class="col-md-8">
                 <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.frontend.card','data' => []]); ?>
<?php $component->withName('frontend.card'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes([]); ?>
                     <?php $__env->slot('header'); ?> 
                        <?php echo app('translator')->get('Two Factor Recovery Codes'); ?>
                     <?php $__env->endSlot(); ?>

                     <?php $__env->slot('body'); ?> 
                        <h5><?php echo app('translator')->get('Save your two factor recovery codes:'); ?></h5>

                        <p><?php echo app('translator')->get('Recovery codes are used to access your account in the event you no longer have access to your authenticator app.'); ?></p>

                        <p class="text-danger"><strong><?php echo app('translator')->get('Save these codes! If you lose your device and don\'t have the recovery codes you will lose access to your account forever!'); ?></strong></p>

                         <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.forms.patch','data' => ['action' => route('frontend.auth.account.2fa.update'),'name' => 'confirm-item']]); ?>
<?php $component->withName('forms.patch'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['action' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(route('frontend.auth.account.2fa.update')),'name' => 'confirm-item']); ?>
                            <button class="btn btn-sm btn-block btn-danger mb-3" type="submit"><?php echo app('translator')->get('Generate New Backup Codes'); ?></button>
                         <?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 

                        <p><strong><?php echo app('translator')->get('Each code can only be used once!'); ?></strong></p>

                        <div class="row">
                            <?php $__currentLoopData = collect($recoveryCodes)->chunk(5); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $codes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="col-6">
                                    <ul>
                                        <?php $__currentLoopData = $codes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $code): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li>
                                                <?php echo e($code['code']); ?> -

                                                <?php if($code['used_at']): ?>
                                                    <strong class="text-danger">
                                                        <?php echo app('translator')->get('Used'); ?>: <?php echo e(Timezone::convertToLocal(carbon($code['used_at']))); ?>
                                                    </strong>
                                                <?php else: ?>
                                                    <em><?php echo app('translator')->get('Not Used'); ?></em>
                                                <?php endif; ?>
                                            </li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div><!--col-->
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div><!--row-->

                        <a href="<?php echo e(route('frontend.user.account', ['#two-factor-authentication'])); ?>" class="btn btn-sm btn-block btn-success"><?php echo app('translator')->get('I have stored these codes in a safe place'); ?></a>
                     <?php $__env->endSlot(); ?>
                 <?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 
            </div><!--col-md-8-->
        </div><!--row-->
    </div><!--container-->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\mth-exchange\resources\views/frontend/user/account/tabs/two-factor-authentication/recovery.blade.php ENDPATH**/ ?>