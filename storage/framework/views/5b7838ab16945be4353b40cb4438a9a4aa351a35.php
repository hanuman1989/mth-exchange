<!--Action Button-->

<!--Action Button-->
<div class="btn-group">
    <button type="button" class="btn btn-primary btn-flat dropdown-toggle" data-toggle="dropdown">Action
        <span class="sr-only">Toggle Dropdown</span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li>
            <a href="<?php echo e(route( 'admin.languages.index' )); ?>">
                <i class="fa fa-list-ul"></i> <?php echo e(trans( 'menus.backend.languages.all' )); ?>

            </a>
        </li>
        <?php if($logged_in_user->hasAllAccess( 'create-language' )): ?>
            <li>
                <a href="<?php echo e(route( 'admin.languages.create' )); ?>">
                    <i class="fa fa-plus"></i> <?php echo e(trans( 'menus.backend.languages.create' )); ?>

                </a>
            </li>
        <?php endif; ?>
    </ul>
</div>
<div class="clearfix"></div>
<?php /**PATH D:\xampp_new_7_3\htdocs\mth-exchange\resources\views/backend/languages/partials/languages-header-buttons.blade.php ENDPATH**/ ?>