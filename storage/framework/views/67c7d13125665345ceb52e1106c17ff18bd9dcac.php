<div>
    <?php $__errorArgs = ['code'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
         <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.utils.alert','data' => ['type' => 'danger']]); ?>
<?php $component->withName('utils.alert'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['type' => 'danger']); ?>
            <?php echo e($message); ?>

         <?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 
    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

    <form wire:submit.prevent="validateCode" class="form-horizontal">
        <div class="form-group row">
            <label for="code" class="col-md-4 col-form-label text-md-right"><?php echo app('translator')->get('Authorization Code'); ?></label>

            <div class="col-md-6">
                <input
                    type="text"
                    id="code"
                    wire:model.lazy="code"
                    minlength="6"
                    class="form-control"
                    placeholder="<?php echo e(__('Authorization Code')); ?>"
                    required
                    autofocus />
            </div>
        </div><!--form-group-->

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button class="btn btn-primary" type="submit"><?php echo app('translator')->get('Enable Two Factor Authentication'); ?></button>
            </div>
        </div><!--form-group-->
    </form>
</div>
<?php /**PATH D:\xampp\htdocs\mth-exchange\resources\views/components/frontend/two-factor-authentication.blade.php ENDPATH**/ ?>