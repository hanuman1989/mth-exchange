<!--Action Button-->

<!--Action Button-->
<div class="btn-group">
    <button type="button" class="btn btn-primary btn-flat dropdown-toggle" data-toggle="dropdown">Action
        <span class="caret"></span>
        <span class="sr-only">Toggle Dropdown</span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li>
            <a href="<?php echo e(route( 'admin.securityquestions.index' )); ?>">
                <i class="fa fa-list-ul"></i> <?php echo e(trans( 'menus.backend.securityquestions.all' )); ?>

            </a>
        </li>
        <?php if($logged_in_user->hasAllAccess( 'create-securityquestion' )): ?>
            <li>
                <a href="<?php echo e(route( 'admin.securityquestions.create' )); ?>">
                    <i class="fa fa-plus"></i> <?php echo e(trans( 'menus.backend.securityquestions.create' )); ?>

                </a>
            </li>
        <?php endif; ?>
    </ul>
</div>
<div class="clearfix"></div>
<?php /**PATH D:\xampp_new_7_3\htdocs\mth-exchange\resources\views/backend/securityquestions/partials/securityquestions-header-buttons.blade.php ENDPATH**/ ?>