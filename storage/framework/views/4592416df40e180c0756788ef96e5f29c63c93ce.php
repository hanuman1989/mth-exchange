

<?php $__env->startSection('title', trans('labels.backend.master-data.states.management') . ' | ' . trans('labels.backend.master-data.states.edit')); ?>

<?php $__env->startSection('page-header'); ?>
    <h1>
        <?php echo e(trans('labels.backend.master-data.states.management')); ?>

        <small><?php echo e(trans('labels.backend.master-data.states.edit')); ?></small>
    </h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <?php echo e(Form::model($state, ['route' => ['admin.master-data.states.update', $state], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH'])); ?>

        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo e(trans('labels.backend.master-data.states.edit')); ?></h3>

                <div class="box-tools pull-right">
                <?php if (access()->allow('create-state')): ?>
                <a class="btn btn-primary btn-flat" style='color:#fff;' href="<?php echo e(route( 'admin.master-data.states.index' )); ?>">
                    <i class="fa fa-list"></i> <?php echo e(trans('List')); ?>

                </a>
                <div class="clearfix"></div>
                <?php endif; ?>
                </div><!--box-tools pull-right-->
            </div><!-- /.box-header -->

            
            <div class="box-body">
                <div class="form-group">
                    <?php echo $__env->make("backend.master-data.states.form", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <div class="edit-form-btn">
                    <?php echo e(link_to_route('admin.master-data.states.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-md'])); ?>

                    <?php echo e(Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-primary btn-md'])); ?>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div><!--box-->
    </div>
    <?php echo e(Form::close()); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp_new_7_3\htdocs\mth-exchange\resources\views/backend/master-data/states/edit.blade.php ENDPATH**/ ?>