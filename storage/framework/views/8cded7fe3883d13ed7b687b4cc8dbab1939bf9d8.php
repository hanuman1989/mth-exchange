

<?php $__env->startSection('title', __('Enable Two Factor Authentication')); ?>

<?php $__env->startSection('content'); ?>
    <div class="container py-4">
        <div class="row justify-content-center">
            <div class="col-md-8">
                 <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.frontend.card','data' => []]); ?>
<?php $component->withName('frontend.card'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes([]); ?>
                     <?php $__env->slot('header'); ?> 
                        <?php echo app('translator')->get('Enable Two Factor Authentication'); ?>
                     <?php $__env->endSlot(); ?>

                     <?php $__env->slot('body'); ?> 
                        <div class="row">
                            <div class="col-xl-4">
                                <h5><strong><?php echo app('translator')->get('Step 1: Configure your 2FA app'); ?></strong></h5>

                                <p><?php echo app('translator')->get('To enable 2FA, you\'ll need a 2FA authenticator app on your phone. Examples include: Google Authenticator, FreeOTP, Authy, andOTP, and Microsoft Authenticator (Just to name a few).'); ?></p>

                                <p><?php echo app('translator')->get('Most applications will let you set up by scanning the QR code from within the app. If you prefer, you may type the key below the QR code in manually.'); ?></p>
                            </div><!--col-->

                            <div class="col-xl-8">
                                <div class="text-center">
                                    <?php echo $qrCode; ?>


                                    <p><i class="fa fa-key"> <?php echo e($secret); ?></i></p>
                                </div>
                            </div><!--col-->
                        </div><!--row-->

                        <hr/>

                        <h5><strong><?php echo app('translator')->get('Step 2: Enter a 2FA code'); ?></strong></h5>

                        <p><?php echo app('translator')->get('Generate a code from your 2FA app and enter it below:'); ?></p>

                        <?php
if (! isset($_instance)) {
    $dom = \Livewire\Livewire::mount('two-factor-authentication', [])->dom;
} elseif ($_instance->childHasBeenRendered('5Xnkm4l')) {
    $componentId = $_instance->getRenderedChildComponentId('5Xnkm4l');
    $componentTag = $_instance->getRenderedChildComponentTagName('5Xnkm4l');
    $dom = \Livewire\Livewire::dummyMount($componentId, $componentTag);
    $_instance->preserveRenderedChild('5Xnkm4l');
} else {
    $response = \Livewire\Livewire::mount('two-factor-authentication', []);
    $dom = $response->dom;
    $_instance->logRenderedChild('5Xnkm4l', $response->id, \Livewire\Livewire::getRootElementTagName($dom));
}
echo $dom;
?></livewire:two-factor-authentication>
                     <?php $__env->endSlot(); ?>
                 <?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 
            </div><!--col-md-8-->
        </div><!--row-->
    </div><!--container-->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\mth-exchange\resources\views/frontend/user/account/tabs/two-factor-authentication/enable.blade.php ENDPATH**/ ?>