<div class="box-body">
    <div class="form-group">
        <?php echo e(Form::label('name', trans('labels.backend.master-data.states.table.name'), ['class' => 'col-lg-2 control-label required'])); ?>


        <div class="col-lg-10">
            <?php echo e(Form::text('name', null, ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.master-data.states.table.name'), 'required', 'maxlength' => 100])); ?>

        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        <?php echo e(Form::label('code', trans('labels.backend.master-data.states.table.code'), ['class' => 'col-lg-2 control-label required'])); ?>


        <div class="col-lg-10">
            <?php echo e(Form::text('code', null, ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.master-data.states.table.code'), 'required', 'maxlength' => 2])); ?>

        </div><!--col-lg-10-->
    </div><!--form control-->

    
    <div class="form-group">
        <?php echo e(Form::label('status', trans('labels.backend.master-data.states.table.status'), ['class' => 'col-lg-2 control-label'])); ?>


        <div class="col-lg-1">
                    <div class="control-group">
                        <label class="control control--checkbox">
                        <?php if(isset($state->status) && !empty ($state->status)): ?>
                            <?php echo e(Form::checkbox('status', 1, true)); ?>

                        <?php else: ?>
                            <?php echo e(Form::checkbox('status', 1, false)); ?>

                        <?php endif; ?>
                        <div class="control__indicator"></div>
                        </label>
                    </div>
        </div><!--col-lg-1-->
    </div><!--form control-->
</div><?php /**PATH D:\xampp_new_7_3\htdocs\mth-exchange\resources\views/backend/master-data/states/form.blade.php ENDPATH**/ ?>