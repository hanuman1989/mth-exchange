

<?php $__env->startSection('title', __('Two Factor Authentication is required')); ?>

<?php $__env->startSection('content'); ?>
    <div class="container py-4">
        <div class="row justify-content-center">
            <div class="col-md-8">
                 <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.frontend.card','data' => []]); ?>
<?php $component->withName('frontend.card'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes([]); ?>
                     <?php $__env->slot('header'); ?> 
                        <?php echo app('translator')->get('Two Factor Authentication is required'); ?>
                     <?php $__env->endSlot(); ?>

                     <?php $__env->slot('body'); ?> 
                        <p><?php echo app('translator')->get('To continue, open up your Authenticator app and issue your 2FA code.'); ?></p>

                         <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.forms.post','data' => ['action' => $action]]); ?>
<?php $component->withName('forms.post'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['action' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute($action)]); ?>
                            <?php $__currentLoopData = (array)$credentials; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $name => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <input type="hidden" name="<?php echo e($name); ?>" value="<?php echo e($value); ?>">
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            <?php if($remember): ?>
                                <input type="hidden" name="remember" value="on">
                            <?php endif; ?>

                            <div class="form-group row">
                                <label for="<?php echo e($input); ?>" class="col-md-4 col-form-label text-md-right"><?php echo app('translator')->get('Authentication Code'); ?></label>

                                <div class="col-md-6">
                                    <input type="text"
                                           name="<?php echo e($input); ?>"
                                           id="<?php echo e($input); ?>"
                                           class="form-control <?php echo e($error ? 'is-invalid' : ''); ?>"
                                           placeholder="123456"
                                           minlength="6"
                                           required />

                                    <?php if($error): ?>
                                        <div class="invalid-feedback">
                                            <?php echo e(trans('laraguard::validation.totp_code')); ?>

                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div><!--form-group-->

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button class="btn btn-primary" type="submit"><?php echo app('translator')->get('Confirm Code'); ?></button>
                                </div>
                            </div><!--form-group-->
                         <?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 
                     <?php $__env->endSlot(); ?>
                 <?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?> 
            </div><!--col-md-8-->
        </div><!--row-->
    </div><!--container-->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp_new_7_3\htdocs\mth-exchange\resources\views/vendor/laraguard/auth.blade.php ENDPATH**/ ?>